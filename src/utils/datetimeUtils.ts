/**
 * A class for datetime utility functions
 */

import type { MapTracksConfig } from '@/types/config';
import { DateTime } from 'luxon';

/**
 * Gets a formatted timestamp for display
 *
 * @param datetime - the datetime to format
 * @param should_append_date - whether to display date or not
 * @param locale - a valid locale
 * @param timezone - a valid IANA timezone
 *
 * @returns a formatted timestamp for display
 */
export function formatTimestamp(
  datetime: Date | string | null | undefined,
  should_append_date = true,
  locale = 'en-GB',
  timezone = 'UTC'
): string {
  if (!datetime) {
    return 'no time';
  }
  datetime = new Date(datetime);
  const time_string = datetime.toLocaleTimeString(locale, {
    hour: 'numeric',
    minute: 'numeric',
    timeZoneName: 'short',
    timeZone: timezone,
  });
  const date_string = datetime.toLocaleDateString(locale, {
    weekday: 'short',
    day: '2-digit',
    month: 'short',
    year: 'numeric',
    timeZone: timezone,
  });
  return should_append_date ? `${time_string} (${date_string})` : time_string;
}

/**
 * Checks if a string is valid ISO datetimestamp
 */
export function isValidIsoDatetime(datetime: Date): boolean {
  return DateTime.fromJSDate(datetime).isValid;
}

/**
 * Format timestamp from 2022-03-10T09:58:645371Z to 2022-03-10 09:58
 * Cuts seconds and replaces the T for easier reading
 */
export function cutTimestamp(timestring: string | undefined): string {
  if (!timestring) return '';
  return timestring.slice(0, -8).replace('T', ' ');
}

/**
 * Remove the offset between UTC and browser locale's timezone from datetime and preserve UTC as timezone.
 *
 * Some UI libraries do not allow setting a timezone for a datetime component,
 * instead inferring the timezone from the browser locale, followed by converting it to UTC timezone by adding an offset.
 * This function serves to remove the resulting offset from the datetime and store it in UTC correctly.
 *
 * @example
 * // returns datetime equivalent to 2022-04-21T12:36:04.000Z when locale's timezone is 2 hours ahead
 * overrideTimezoneWithUtc(new Date(Date.parse('2022-04-21T10:36:04.000Z')));
 *
 * @param datetime - The datetime with a wrong UTC offset.
 * @returns The datetime in UTC with the offset removed.
 */
export function overrideTimezoneWithUtc(datetime: Date): Date {
  const zone = 'utc';
  return DateTime.fromJSDate(datetime)
    .setZone(zone, { keepLocalTime: true })
    .toJSDate();
}

/**
 * Add offset in minutes to datetime
 *
 * @param datetime - The datetime to add an offset to.
 * @param offset - The offset in minutes.
 * @returns The datetime with added offset.
 */
export function addOffsetToDatetime(datetime: Date, offset: number): Date {
  return DateTime.fromJSDate(datetime).plus({ minutes: offset }).toJSDate();
}

/**
 * Computes the oldest date using a given newestDateIso and a number of days that should be subtracted.
 * @param newestDateIso An ISO-formatted date string. Or a Date object. Or undefined|null for "now".
 * @param daysToSubtract The number of days to subtract from the newestDateIso, creating an oldestDateIso that lies daysToSubtract days before newestDateIso.
 * @returns An oldestDateIso that lies daysToSubtract days before newestDateIso.
 */
export function computeOldestDateIso(
  daysToSubtract: number,
  newestDateIso?: string | Date | null
): string {
  const shift_date = newestDateIso ? new Date(newestDateIso) : new Date();
  shift_date.setDate(shift_date.getDate() - daysToSubtract);
  const oldestDateIso = shift_date.toISOString();
  return oldestDateIso;
}

/** Returns a textual representation of how much time has passed between two dates */
export function timeSince(
  date_past: string | number | Date,
  date_now: string | number | Date | null | undefined
): string {
  //   date = new Date(date);
  const lastDate = new Date(date_past);
  const now = date_now ? new Date(date_now) : new Date();
  const seconds = Math.floor((now.getTime() - lastDate.getTime()) / 1000);

  let interval = Math.floor(seconds / 31536000);
  if (interval > 1) {
    return interval + ' years';
  }
  interval = Math.floor(seconds / 2592000);
  if (interval > 1) {
    return interval + ' months';
  }
  interval = Math.floor(seconds / 86400);
  if (interval > 1) {
    return interval + ' days';
  }
  interval = Math.floor(seconds / 3600);
  if (interval > 1) {
    return interval + ' hours';
  }
  interval = Math.floor(seconds / 60);
  if (interval > 1) {
    return interval + ' minutes';
  }
  return Math.floor(seconds) + ' seconds';
}
/** Returns a status light color that depends on how much time has passed */
export function colorSince(
  date_past?: string | number | Date | null | undefined,
  date_now?: string | number | Date | null | undefined
): string {
  if (
    date_now &&
    Math.abs(new Date(date_now).getTime() - new Date().getTime()) > 60 * 1000 // more than a minute different from now
  )
    // we're looking into the past or future, so just show blue:
    return 'blue';
  else {
    if (date_past) {
      const now = date_now ? new Date(date_now) : new Date();
      const seconds = Math.floor(
        (now.getTime() - new Date(date_past).getTime()) / 1000
      );
      // less than 30 minutes:
      if (seconds > 0 && seconds <= 1800) return 'green';
      // less than 24 hours:
      else if (seconds <= 86400) return 'yellow';
      // more than 24 hours:
      else return 'red';
    } else {
      // no old date given / does not apply:
      return 'grey';
    }
  }
}

/**
 * Returns a descriptive string of the selected timespan of positions being shown on the map.
 * This uses the given MapTracksConfig instance to describe the overall trace timespan, while
 * any item-specific timespans are not reflected in the output string and should be described separately.
 */
export function describeTrackConfigTimespan(
  map_tracks_config: MapTracksConfig
): string {
  const oldest_string = map_tracks_config.oldest_date_iso
    ? formatTimestamp(new Date(map_tracks_config.oldest_date_iso))
    : undefined;
  const newest_string = map_tracks_config.newest_date_iso
    ? formatTimestamp(new Date(map_tracks_config.newest_date_iso))
    : undefined;

  if (map_tracks_config.type == 'number_of_days') {
    const before_string = newest_string ? ' before ' + newest_string : '';
    return ` in the last ${map_tracks_config.show_past_days} days${before_string}`;
  } else {
    if (oldest_string && newest_string) {
      return ` between ${oldest_string} and ${newest_string}`;
    } else if (oldest_string) {
      return ` since ${oldest_string}`;
    } else if (newest_string) {
      return ` before ${newest_string}`;
    } else {
      return ``;
    }
  }
}
