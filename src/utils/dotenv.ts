/**
 * Get a .env variable value and throw an error if it is undefined.
 *
 * @param key - The .env variable key.
 * @param type - The .env variable type.
 * @param fallback - The fallback value for the .env variable.
 * @returns The .env variable value.
 *
 * @throws {@link Error}
 * Throws if the variable value is undefined.
 */
export function getDotenvVariable(key: string): string;
export function getDotenvVariable(key: string, type: 'string'): string;
export function getDotenvVariable(key: string, type: 'number'): number;
export function getDotenvVariable(
  key: string,
  type: 'string',
  fallback: string
): string;
export function getDotenvVariable(
  key: string,
  type: 'number',
  fallback: number
): number;
export function getDotenvVariable(
  key: string,
  type: 'string' | 'number' = 'string',
  fallback?: string | number
): string | number {
  const value = import.meta.env[key];
  if (value === undefined || value === '') {
    if (fallback) {
      return fallback;
    } else {
      throw new Error(`.env variable ${key} is undefined.`);
    }
  }
  return type === 'number' ? parseInt(value, 10) : value;
}
