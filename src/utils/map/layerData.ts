import * as L from 'leaflet';
import all_filters from '@/constants/filters';
import type { ItemFilterGroup } from '@/types/item-filter-and-sorting';
import storage from '@/utils/storageWrapper';
import { useMapStore } from '@/stores/MapStore';

/**
 * The following code includes the definitions for all layers (incl. geojson layers)
 * that can be displayed on the map and appear in the Leaflet Layers Control.
 * For the layers control, the plugin Leaflet.Control.Layers.Tree is used.
 */

// Prepare Leaflet geoJSON layers
export const layerLabels = new L.LayerGroup();
export const itemLabels = new L.LayerGroup();

const ne10mTimeZones = L.geoJSON();
// SAR Zones
// West Mediterranean Sea SAR Zones
// Portugal SAR Zones
const portugalSarDelgada = L.geoJSON();
const portugalSarFunchal = L.geoJSON();
const portugalSarLisboa = L.geoJSON();
// Morocco SAR Zone
const moroccoSarZone = L.geoJSON();
/** Spain SAR Zones */
const spainSarAlgeciras = L.geoJSON();
const spainSarAlmeria = L.geoJSON();
const SpainSarBarcelona = L.geoJSON();
const spainSarCadiz = L.geoJSON();
const spainSarCartagena = L.geoJSON();
const spainSarCastellon = L.geoJSON();
const spainSarHuelva = L.geoJSON();
const spainSarMadrid = L.geoJSON();
const spainSarPalma = L.geoJSON();
const spainSarTarifa = L.geoJSON();
const spainSarTarragona = L.geoJSON();
const spainSarValencia = L.geoJSON();
/** France SAR Zones */
const franceSarAjaccio = L.geoJSON();
const franceSarLaGarde = L.geoJSON();
// Algeria SAR Zones
const algerianSarAlger = L.geoJSON();
const algerianSarJijel = L.geoJSON();
const algerianSarOran = L.geoJSON();
// Central Mediterranean Sea SAR Zones
// Italy SAR Zones
const italianSarAncona = L.geoJSON();
const italianSarBari = L.geoJSON();
const italianSarCagliari = L.geoJSON();
const italianSarCatania = L.geoJSON();
const italianSarCivitavecchia = L.geoJSON();
const italianSarGenova = L.geoJSON();
const italianSarlivorno = L.geoJSON();
const italianSarMessina = L.geoJSON();
const italianSarNapoli = L.geoJSON();
const italianSarOlbia = L.geoJSON();
const italianSarPalermo = L.geoJSON();
const italianSarPescara = L.geoJSON();
const italianSarRavenna = L.geoJSON();
const italianSarReggioCalabria = L.geoJSON();
const italianSarRome = L.geoJSON();
const italianSarTrieste = L.geoJSON();
const italianSarVenezia = L.geoJSON();
// Maltese SAR Zone
const malteseSarZones = L.geoJSON();
// Tunisian SAR Zones
const tunisianSarZones = L.geoJSON();
// Libyan SAR Zones
const libyanSarZones = L.geoJSON();
// East Mediterranean Sea SAR Zones
// Israel SAR Zone
const israelSarRccHaifa = L.geoJSON();
// Cyprus SAR Zone
const cyprusSarLarnaca = L.geoJSON();
// Egypt SAR Zones
const egyptSarBarnes = L.geoJSON();
const egyptSarBorgElArab = L.geoJSON();
const egyptSarCairo = L.geoJSON();
const egyptSarElArish = L.geoJSON();
const egyptSarHurghada = L.geoJSON();
const egyptSarLuxor = L.geoJSON();
const egyptSarMatrouh = L.geoJSON();
// Greece SAR Zones
const greeceSarChania = L.geoJSON();
const greeceSarMytilini = L.geoJSON();
const greeceSarPatra = L.geoJSON();
const greeceSarPiraeus = L.geoJSON();
const greeceSarRodos = L.geoJSON();
const greeceSarThessaloniki = L.geoJSON();
// Turkey SAR Zones
const turkiyeSarAmasra = L.geoJSON();
const turkiyeSarAntalaya = L.geoJSON();
const turkiyeSarCanakkale = L.geoJSON();
const turkiyeSarIskenderun = L.geoJSON();
const turkiyeSarIstanbul = L.geoJSON();
const turkiyeSarIzmir = L.geoJSON();
const turkiyeSarMainRccAnkara = L.geoJSON();
const turkiyeSarMarmaris = L.geoJSON();
const turkiyeSarMersin = L.geoJSON();
const turkiyeSarMudanya = L.geoJSON();
const turkiyeSarSamsun = L.geoJSON();
const turkiyeSarTrabzon = L.geoJSON();
const turkiyeSarTurkish = L.geoJSON();

const malteseNoflyZones = L.geoJSON();
const tunisianNoflyZone1 = L.geoJSON();
const tunisianNoflyZone2 = L.geoJSON();
const tunisianNoflyZone3 = L.geoJSON();
const libyanNoflyZones = L.geoJSON();
const twelveNauticalMileZonesLayer = L.geoJSON();
const twentyfourNauticalMileZonesLayer = L.geoJSON();

const worldPortIndex2019 = L.geoJSON();
const ifrWaypoints = L.geoJSON();
const ne10mGraticules1Cr = L.geoJSON();
const ne10mGraticules5Cr = L.geoJSON();
const ne10mGraticules10Cr = L.geoJSON();
const ne10mGraticules15Cr = L.geoJSON();
const ne10mGraticules20Cr = L.geoJSON();
const ne10mGraticules30Cr = L.geoJSON();
const ne10mFerriesCr = L.geoJSON();
const ne10mAdmin1StatesProvinces = L.geoJSON();
const ne10mAdmin0Countries = L.geoJSON();
const ne10mAdmin0BoundaryLinesMaritimeIndicator = L.geoJSON();

const allShipsLayer = L.geoJSON();
const allAircraftLayer = L.geoJSON();

export const static_geojson = {
  'geojson/eez_12nm_v3_cr.geojson': twelveNauticalMileZonesLayer,
  'geojson/eez_24nm_v3_cr.geojson': twentyfourNauticalMileZonesLayer,
  'geojson/ne_10m_time_zones_r.geojson': ne10mTimeZones,
  'geojson/ne_10m_admin_1_states_provinces_cr.geojson':
    ne10mAdmin1StatesProvinces,
  'geojson/ne_10m_admin_0_boundary_lines_maritime_indicators_cr.geojson':
    ne10mAdmin0BoundaryLinesMaritimeIndicator,
  // SAR Zones
  // West Mediterranean Sea SAR Zones
  /** Portugal SAR Zones */
  'geojson/portugal_sar_delgada.geojson': portugalSarDelgada,
  'geojson/portugal_sar_funchal.geojson': portugalSarFunchal,
  'geojson/portugal_sar_lisboa.geojson': portugalSarLisboa,
  // Morocco SAR Zone
  'geojson/morocco_sar_rabat.geojson': moroccoSarZone,
  /** Spain SAR Zones */
  'geojson/spain_sar_algeciras.geojson': spainSarAlgeciras,
  'geojson/spain_sar_almeria.geojson': spainSarAlmeria,
  'geojson/spain_sar_barcelona.geojson': SpainSarBarcelona,
  'geojson/spain_sar_cadiz.geojson': spainSarCadiz,
  'geojson/spain_sar_cartagena.geojson': spainSarCartagena,
  'geojson/spain_sar_castellon.geojson': spainSarCastellon,
  'geojson/spain_sar_huelva.geojson': spainSarHuelva,
  'geojson/spain_sar_madrid.geojson': spainSarMadrid,
  'geojson/spain_sar_palma.geojson': spainSarPalma,
  'geojson/spain_sar_tarifa.geojson': spainSarTarifa,
  'geojson/spain_sar_tarragona.geojson': spainSarTarragona,
  'geojson/spain_sar_valencia.geojson': spainSarValencia,
  /** France SAR Zones */
  'geojson/france_sar_ajaccio.geojson': franceSarAjaccio,
  'geojson/france_sar_la_garde.geojson': franceSarLaGarde,
  /** Algerian SAR Zones */
  'geojson/algeria_sar_alger.geojson': algerianSarAlger,
  'geojson/algeria_sar_jijel.geojson': algerianSarJijel,
  'geojson/algeria_sar_oran.geojson': algerianSarOran,
  // Central Mediterranean Sea SAR Zones
  /** Italian SAR Zones */
  'geojson/italian_sar_ancona.geojson': italianSarAncona,
  'geojson/italian_sar_bari.geojson': italianSarBari,
  'geojson/italian_sar_cagliari.geojson': italianSarCagliari,
  'geojson/italian_sar_catania.geojson': italianSarCatania,
  'geojson/italian_sar_civitavecchia.geojson': italianSarCivitavecchia,
  'geojson/italian_sar_genova.geojson': italianSarGenova,
  'geojson/italian_sar_livorno.geojson': italianSarlivorno,
  'geojson/italian_sar_messina.geojson': italianSarMessina,
  'geojson/italian_sar_napoli.geojson': italianSarNapoli,
  'geojson/italian_sar_olbia.geojson': italianSarOlbia,
  'geojson/italian_sar_palermo.geojson': italianSarPalermo,
  'geojson/italian_sar_pescara.geojson': italianSarPescara,
  'geojson/italian_sar_ravenna.geojson': italianSarRavenna,
  'geojson/italian_sar_reggio_calabria.geojson': italianSarReggioCalabria,
  'geojson/italian_sar_rome.geojson': italianSarRome,
  'geojson/italian_sar_trieste.geojson': italianSarTrieste,
  'geojson/italian_sar_venezia.geojson': italianSarVenezia,
  /** Malta SAR Zone */
  'geojson/maltese_sar_zone.geojson': malteseSarZones,
  /** Tunisian SAR Zone */
  'geojson/tunisian_sar_zone.geojson': tunisianSarZones,
  /** Libyan SAR Zone */
  'geojson/libyan_sar_zone.geojson': libyanSarZones,
  // East Mediterranean Sea SAR Zones
  /** Greece SAR Zones */
  'geojson/greece_sar_chania.geojson': greeceSarChania,
  'geojson/greece_sar_mytilini.geojson': greeceSarMytilini,
  'geojson/greece_sar_patra.geojson': greeceSarPatra,
  'geojson/greece_sar_piraeus.geojson': greeceSarPiraeus,
  'geojson/greece_sar_rodos.geojson': greeceSarRodos,
  'geojson/greece_sar_thessaloniki.geojson': greeceSarThessaloniki,
  'geojson/maltese_nofly_zone.geojson': malteseNoflyZones,
  'geojson/tunisian_nofly_zone1.geojson': tunisianNoflyZone1,
  'geojson/tunisian_nofly_zone2.geojson': tunisianNoflyZone2,
  'geojson/tunisian_nofly_zone3.geojson': tunisianNoflyZone3,
  'geojson/libyan_nofly_zone.geojson': libyanNoflyZones,
  'geojson/wpi_2019.geojson': worldPortIndex2019,
  'geojson/ifr_waypoints.geojson': ifrWaypoints,
  'geojson/ne_10m_graticules_1_cr.geojson': ne10mGraticules1Cr,
  'geojson/ne_10m_graticules_5_cr.geojson': ne10mGraticules5Cr,
  'geojson/ne_10m_graticules_10_cr.geojson': ne10mGraticules10Cr,
  'geojson/ne_10m_graticules_15_cr.geojson': ne10mGraticules15Cr,
  'geojson/ne_10m_graticules_20_cr.geojson': ne10mGraticules20Cr,
  'geojson/ne_10m_graticules_30_cr.geojson': ne10mGraticules30Cr,
  'geojson/ne_10m_roads_ferries_cr.geojson': ne10mFerriesCr,
  // Israel SAR Zone
  'geojson/israel_sar_rcc_haifa.geojson': israelSarRccHaifa,
  /** Egypt SAR Zones */
  'geojson/egypt_sar_barnes.geojson': egyptSarBarnes,
  'geojson/egypt_sar_borg_el_arab.geojson': egyptSarBorgElArab,
  'geojson/egypt_sar_cairo.geojson': egyptSarCairo,
  'geojson/egypt_sar_el_arish.geojson': egyptSarElArish,
  'geojson/egypt_sar_hurghada.geojson': egyptSarHurghada,
  'geojson/egypt_sar_luxor.geojson': egyptSarLuxor,
  'geojson/egypt_sar_matrouh.geojson': egyptSarMatrouh,
  // Cyprus SAR Zone
  'geojson/cyprus_sar_larnaca.geojson': cyprusSarLarnaca,
  /** Turkiye SAR Zones */
  'geojson/turkiye_sar_amasra.geojson': turkiyeSarAmasra,
  'geojson/turkiye_sar_antalya.geojson': turkiyeSarAntalaya,
  'geojson/turkiye_sar_canakkale.geojson': turkiyeSarCanakkale,
  'geojson/turkiye_sar_iskenderun.geojson': turkiyeSarIskenderun,
  'geojson/turkiye_sar_izmir.geojson': turkiyeSarIzmir,
  'geojson/turkiye_sar_main_rcc_ankara.geojson': turkiyeSarMainRccAnkara,
  'geojson/turkiye_sar_marmaris.geojson': turkiyeSarMarmaris,
  'geojson/turkiye_sar_mersin.geojson': turkiyeSarMersin,
  'geojson/turkiye_sar_mudanya.geojson': turkiyeSarMudanya,
  'geojson/turkiye_sar_trabzon.geojson': turkiyeSarTrabzon,
  'geojson/turkiye_sar_samsun.geojson': turkiyeSarSamsun,
  'geojson/turkiye_sar_istanbul.geojson': turkiyeSarIstanbul,
  'geojson/turkiye_sar_turkish.geojson': turkiyeSarTurkish,
};

export const dynamicGeojson = [
  {
    path: 'geojson/all_ships.geojson',
    layer: allShipsLayer,
    layer_info: 'ship_count',
  },
  {
    path: 'geojson/all_aircraft.geojson',
    layer: allAircraftLayer,
    layer_info: 'aircraft_count',
  },
];

// Get loaded database layers
export const loadedDatabaseLayers: Record<string, L.LayerGroup> =
  _makeDatabaseLayergroups();

function _makeDatabaseLayergroups(): Record<string, L.LayerGroup> {
  const all_filter_groups: Array<ItemFilterGroup> =
    all_filters.get_filter_groups.filter((fg) => fg.selectable_on_map ?? false);

  const database_layers: Record<string, L.LayerGroup> = {};
  // fire events when layers are added/removed to update checkboxes in leftnavigation
  for (const filtergroup of all_filter_groups) {
    const my_layergroup = new L.LayerGroup();
    my_layergroup.on('add', () => {
      useMapStore().setItemLayerVisibility({
        title: filtergroup.title,
        isShowing: true,
      });
    });
    my_layergroup.on('remove', () => {
      useMapStore().setItemLayerVisibility({
        title: filtergroup.title,
        isShowing: false,
      });
    });
    database_layers[filtergroup.title] = my_layergroup;
  }

  console.table(database_layers);
  return database_layers;
}

/**
 * Some database layers might not be supposed to be shown on the map initially.
 * Specifically, if a layer's filterGroup states that it is not to be
 * initially selected on the map, then don't include it in this array.
 *
 * In any case, an ItemFilterGroup needs to be selectable_on_map in
 * order to be considered as a starting layer.
 */
const startingDatabaseLayers: L.LayerGroup<any>[] =
  all_filters.get_filter_groups
    .filter(
      (fg: ItemFilterGroup) =>
        (fg.selectable_on_map ?? false) &&
        (fg.initially_selected_on_map ?? true)
    )
    .map((fg: ItemFilterGroup) => loadedDatabaseLayers[fg.title]);

// Prepare other Layer Groups
export const classicItemsLayer = new L.LayerGroup();

// Prepare FeatureGroups which combine multiple Layers
export const drawnAreasAndMarkers = new L.FeatureGroup();

const graticulesFeatureGroup = new L.FeatureGroup();
graticulesFeatureGroup.addLayer(ne10mGraticules10Cr);
graticulesFeatureGroup.addLayer(ne10mGraticules1Cr);
graticulesFeatureGroup.addLayer(ne10mGraticules5Cr);
graticulesFeatureGroup.addLayer(ne10mGraticules15Cr);
graticulesFeatureGroup.addLayer(ne10mGraticules20Cr);
graticulesFeatureGroup.addLayer(ne10mGraticules30Cr);

const nmZonesFeatureGroup = new L.FeatureGroup();
nmZonesFeatureGroup.addLayer(twelveNauticalMileZonesLayer);
nmZonesFeatureGroup.addLayer(twentyfourNauticalMileZonesLayer);

/** SAR Zones */
// West Mediterranean SAR Zones
const westMediterraneanSarZonesFeatureGroup = new L.FeatureGroup();
/** Portugal SAR Zones */
westMediterraneanSarZonesFeatureGroup.addLayer(portugalSarDelgada);
westMediterraneanSarZonesFeatureGroup.addLayer(portugalSarFunchal);
westMediterraneanSarZonesFeatureGroup.addLayer(portugalSarLisboa);
// Morocco SAR ZOne
westMediterraneanSarZonesFeatureGroup.addLayer(moroccoSarZone);
// Spain SAR Zones
westMediterraneanSarZonesFeatureGroup.addLayer(spainSarAlgeciras);
westMediterraneanSarZonesFeatureGroup.addLayer(spainSarAlmeria);
westMediterraneanSarZonesFeatureGroup.addLayer(spainSarCadiz);
westMediterraneanSarZonesFeatureGroup.addLayer(spainSarCartagena);
westMediterraneanSarZonesFeatureGroup.addLayer(spainSarCastellon);
westMediterraneanSarZonesFeatureGroup.addLayer(spainSarHuelva);
westMediterraneanSarZonesFeatureGroup.addLayer(spainSarMadrid);
westMediterraneanSarZonesFeatureGroup.addLayer(spainSarPalma);
westMediterraneanSarZonesFeatureGroup.addLayer(spainSarTarifa);
westMediterraneanSarZonesFeatureGroup.addLayer(spainSarTarragona);
westMediterraneanSarZonesFeatureGroup.addLayer(spainSarValencia);
westMediterraneanSarZonesFeatureGroup.addLayer(spainSarValencia);
// France SAR Zones
westMediterraneanSarZonesFeatureGroup.addLayer(franceSarAjaccio);
westMediterraneanSarZonesFeatureGroup.addLayer(franceSarLaGarde);
// Algerian SAR Zones
westMediterraneanSarZonesFeatureGroup.addLayer(algerianSarAlger);
westMediterraneanSarZonesFeatureGroup.addLayer(algerianSarJijel);
westMediterraneanSarZonesFeatureGroup.addLayer(algerianSarOran);
// Central Mediterranean SAR Zones
const centralMediterraneanSarZonesFeatureGroup = new L.FeatureGroup();
// Maltese SAR Zone
centralMediterraneanSarZonesFeatureGroup.addLayer(malteseSarZones);
// Tunisian SAR Zone
centralMediterraneanSarZonesFeatureGroup.addLayer(tunisianSarZones);
// Libyan SAR Zone
centralMediterraneanSarZonesFeatureGroup.addLayer(libyanSarZones);
/** Italian SAR Zones */
centralMediterraneanSarZonesFeatureGroup.addLayer(italianSarAncona);
centralMediterraneanSarZonesFeatureGroup.addLayer(italianSarBari);
centralMediterraneanSarZonesFeatureGroup.addLayer(italianSarCagliari);
centralMediterraneanSarZonesFeatureGroup.addLayer(italianSarCatania);
centralMediterraneanSarZonesFeatureGroup.addLayer(italianSarCivitavecchia);
centralMediterraneanSarZonesFeatureGroup.addLayer(italianSarGenova);
centralMediterraneanSarZonesFeatureGroup.addLayer(italianSarlivorno);
centralMediterraneanSarZonesFeatureGroup.addLayer(italianSarMessina);
centralMediterraneanSarZonesFeatureGroup.addLayer(italianSarNapoli);
centralMediterraneanSarZonesFeatureGroup.addLayer(italianSarOlbia);
centralMediterraneanSarZonesFeatureGroup.addLayer(italianSarPalermo);
centralMediterraneanSarZonesFeatureGroup.addLayer(italianSarPescara);
centralMediterraneanSarZonesFeatureGroup.addLayer(italianSarRavenna);
centralMediterraneanSarZonesFeatureGroup.addLayer(italianSarReggioCalabria);
centralMediterraneanSarZonesFeatureGroup.addLayer(italianSarRome);
centralMediterraneanSarZonesFeatureGroup.addLayer(italianSarTrieste);
centralMediterraneanSarZonesFeatureGroup.addLayer(italianSarVenezia);
// East Mediterranean SAR Zones
const eastMediterraneanSarZonesFeatureGroup = new L.FeatureGroup();
// Cyprus SAR Zone
eastMediterraneanSarZonesFeatureGroup.addLayer(cyprusSarLarnaca);
// Israel SAR Zone
eastMediterraneanSarZonesFeatureGroup.addLayer(israelSarRccHaifa);
// Egypt SAR Zones
eastMediterraneanSarZonesFeatureGroup.addLayer(egyptSarBarnes);
eastMediterraneanSarZonesFeatureGroup.addLayer(egyptSarBorgElArab);
eastMediterraneanSarZonesFeatureGroup.addLayer(egyptSarCairo);
eastMediterraneanSarZonesFeatureGroup.addLayer(egyptSarElArish);
eastMediterraneanSarZonesFeatureGroup.addLayer(egyptSarHurghada);
eastMediterraneanSarZonesFeatureGroup.addLayer(egyptSarLuxor);
eastMediterraneanSarZonesFeatureGroup.addLayer(egyptSarMatrouh);
/** Greece SAR Zones */
eastMediterraneanSarZonesFeatureGroup.addLayer(greeceSarChania);
eastMediterraneanSarZonesFeatureGroup.addLayer(greeceSarMytilini);
eastMediterraneanSarZonesFeatureGroup.addLayer(greeceSarPatra);
eastMediterraneanSarZonesFeatureGroup.addLayer(greeceSarPiraeus);
eastMediterraneanSarZonesFeatureGroup.addLayer(greeceSarRodos);
eastMediterraneanSarZonesFeatureGroup.addLayer(greeceSarThessaloniki);
/** Turkiye SAR Zones */
eastMediterraneanSarZonesFeatureGroup.addLayer(turkiyeSarAmasra);
eastMediterraneanSarZonesFeatureGroup.addLayer(turkiyeSarAntalaya);
eastMediterraneanSarZonesFeatureGroup.addLayer(turkiyeSarCanakkale);
eastMediterraneanSarZonesFeatureGroup.addLayer(turkiyeSarIskenderun);
eastMediterraneanSarZonesFeatureGroup.addLayer(turkiyeSarIstanbul);
eastMediterraneanSarZonesFeatureGroup.addLayer(turkiyeSarIzmir);
eastMediterraneanSarZonesFeatureGroup.addLayer(turkiyeSarMainRccAnkara);
eastMediterraneanSarZonesFeatureGroup.addLayer(turkiyeSarMarmaris);
eastMediterraneanSarZonesFeatureGroup.addLayer(turkiyeSarMersin);
eastMediterraneanSarZonesFeatureGroup.addLayer(turkiyeSarMudanya);
eastMediterraneanSarZonesFeatureGroup.addLayer(turkiyeSarSamsun);
eastMediterraneanSarZonesFeatureGroup.addLayer(turkiyeSarTrabzon);
eastMediterraneanSarZonesFeatureGroup.addLayer(turkiyeSarTurkish);

const noflyZonesFeatureGroup = new L.FeatureGroup();
noflyZonesFeatureGroup.addLayer(malteseNoflyZones);
noflyZonesFeatureGroup.addLayer(tunisianNoflyZone1);
noflyZonesFeatureGroup.addLayer(tunisianNoflyZone2);
noflyZonesFeatureGroup.addLayer(tunisianNoflyZone3);
noflyZonesFeatureGroup.addLayer(libyanNoflyZones);

// Prepare layer directory to access all layers
export const allLayers = {
  ...loadedDatabaseLayers, // include all the database layers
  items: classicItemsLayer,
  ships: allShipsLayer,
  aircrafts: allAircraftLayer,
  Countries: ne10mAdmin0Countries,
  Indicators: ne10mAdmin0BoundaryLinesMaritimeIndicator,
  States: ne10mAdmin1StatesProvinces,
  Timeszones: ne10mTimeZones,
  WestMedSarzones: westMediterraneanSarZonesFeatureGroup,
  CentralMedSarzones: centralMediterraneanSarZonesFeatureGroup,
  EastMedSarzones: eastMediterraneanSarZonesFeatureGroup,
  Noflyzones: noflyZonesFeatureGroup,
  Nmzones: nmZonesFeatureGroup,
  Graticules: graticulesFeatureGroup,
  Ports: worldPortIndex2019,
  'IFR waypoints': ifrWaypoints,
  'Layer labels': layerLabels,
  'Item labels': itemLabels,
};

export const allLayersWithLabels = [worldPortIndex2019, ifrWaypoints];

const localTiles: any[] = [
  {
    label: 'Light',
    layer: L.tileLayer('/MapTiles/light/{z}/{x}/{y}.png', {
      attribution: '',
      maxZoom: 12,
      minZoom: 2,
      noWrap: true,
      id: 'groundtile',
    }),
  },
  {
    label: 'Dark',
    layer: L.tileLayer('/MapTiles/dark/{z}/{x}/{y}.png', {
      attribution: '',
      maxZoom: 12,
      minZoom: 2,
      noWrap: true,
      id: 'groundtile',
    }),
  },
  {
    label: 'Extra',
    layer: L.tileLayer('/MapTiles/extra/{z}/{x}/{y}.png', {
      attribution: '',
      maxZoom: 12,
      minZoom: 2,
      noWrap: true,
      id: 'groundtile',
    }),
  },
];

const openstreetmapTiles = L.tileLayer(
  'https://{s}.tile.osm.org/{z}/{x}/{y}.png',
  {
    attribution:
      'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    minZoom: 3,
    noWrap: true,
    id: 'groundtile',
  }
);

const lightTiles = L.tileLayer(
  'https://s.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png',
  {
    attribution:
      'Map tiles by Carto, under CC BY 3.0. Data by <a href="https://www.openstreetmap.org/">OpenStreetMap</a>, under ODbL.',
    maxZoom: 18,
    minZoom: 3,
    noWrap: true,
    id: 'groundtile',
  }
);

// Prepare additional info layers
const seamapTiles = L.tileLayer(
  'https://tiles.openseamap.org/seamark/{z}/{x}/{y}.png',
  {
    attribution: '',
    maxZoom: 18,
    minZoom: 9, // seamap tiles don't exists below 9
    noWrap: true,
    id: 'openseamap',
    accessToken: '',
  }
);

const openportguideWeatherWindstream0 = L.tileLayer(
  'https://weather.openportguide.org/tiles/actual/wind_stream/0h/{z}/{x}/{y}.png',
  {
    attribution:
      'Map tiles by <a href="https://weather.openportguide.org/">openportguide.org</a> under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0.</a>',
    maxZoom: 18,
    minZoom: 3,
    noWrap: true,
    id: '',
    accessToken: '',
  }
);

const openportguideWeatherWindstream6 = L.tileLayer(
  'https://weather.openportguide.org/tiles/actual/wind_stream/6h/{z}/{x}/{y}.png',
  {
    attribution:
      'Map tiles by <a href="https://weather.openportguide.org/">openportguide.org</a> under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0.</a>',
    maxZoom: 18,
    minZoom: 3,
    noWrap: true,
    id: '',
    accessToken: '',
  }
);

const openportguideWeatherWindstream12 = L.tileLayer(
  'https://weather.openportguide.org/tiles/actual/wind_stream/12h/{z}/{x}/{y}.png',
  {
    attribution:
      'Map tiles by <a href="https://weather.openportguide.org/">openportguide.org</a> under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0.</a>',
    maxZoom: 18,
    minZoom: 3,
    noWrap: true,
    id: '',
    accessToken: '',
  }
);

const openportguideWeatherWindstream24 = L.tileLayer(
  'https://weather.openportguide.org/tiles/actual/wind_stream/24h/{z}/{x}/{y}.png',
  {
    attribution:
      'Map tiles by <a href="https://weather.openportguide.org/">openportguide.org</a> under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0.</a>',
    maxZoom: 18,
    minZoom: 3,
    noWrap: true,
    id: '',
    accessToken: '',
  }
);

const openportguideWeatherWindGust0 = L.tileLayer(
  'https://weather.openportguide.org/tiles/actual/gust/0h/{z}/{x}/{y}.png',
  {
    attribution:
      'Map tiles by <a href="https://weather.openportguide.org/">openportguide.org</a> under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0.</a>',
    maxZoom: 18,
    minZoom: 3,
    noWrap: true,
    id: '',
    accessToken: '',
  }
);
openportguideWeatherWindGust0.setOpacity(0.5);

const openportguideWeatherWindGust6 = L.tileLayer(
  'https://weather.openportguide.org/tiles/actual/gust/6h/{z}/{x}/{y}.png',
  {
    attribution:
      'Map tiles by <a href="https://weather.openportguide.org/">openportguide.org</a> under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0.</a>',
    maxZoom: 18,
    minZoom: 3,
    noWrap: true,
    id: '',
    accessToken: '',
  }
);
openportguideWeatherWindGust6.setOpacity(0.5);

const openportguideWeatherWindGust12 = L.tileLayer(
  'https://weather.openportguide.org/tiles/actual/gust/12h/{z}/{x}/{y}.png',
  {
    attribution:
      'Map tiles by <a href="https://weather.openportguide.org/">openportguide.org</a> under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0.</a>',
    maxZoom: 18,
    minZoom: 3,
    noWrap: true,
    id: '',
    accessToken: '',
  }
);
openportguideWeatherWindGust12.setOpacity(0.5);

const openportguideWeatherWindGust24 = L.tileLayer(
  'https://weather.openportguide.org/tiles/actual/gust/24h/{z}/{x}/{y}.png',
  {
    attribution:
      'Map tiles by <a href="https://weather.openportguide.org/">openportguide.org</a> under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0.</a>',
    maxZoom: 18,
    minZoom: 3,
    noWrap: true,
    id: '',
    accessToken: '',
  }
);
openportguideWeatherWindGust24.setOpacity(0.5);

const openportguideWeatherWaveheights0 = L.tileLayer(
  'https://weather.openportguide.org/tiles/actual/significant_wave_height/0h/{z}/{x}/{y}.png',
  {
    attribution:
      'Map tiles by <a href="https://weather.openportguide.org/">openportguide.org</a> under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0.</a>',
    maxZoom: 18,
    minZoom: 3,
    noWrap: true,
    id: 'openportguide_weather_waves',
    accessToken: '',
  }
);

const openportguideWeatherWaveheights6 = L.tileLayer(
  'https://weather.openportguide.org/tiles/actual/significant_wave_height/6h/{z}/{x}/{y}.png',
  {
    attribution:
      'Map tiles by <a href="https://weather.openportguide.org/">openportguide.org</a> under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0.</a>',
    maxZoom: 18,
    minZoom: 3,
    noWrap: true,
    id: 'openportguide_weather_waves',
    accessToken: '',
  }
);

const openportguideWeatherWaveheights12 = L.tileLayer(
  'https://weather.openportguide.org/tiles/actual/significant_wave_height/12h/{z}/{x}/{y}.png',
  {
    attribution:
      'Map tiles by <a href="https://weather.openportguide.org/">openportguide.org</a> under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0.</a>',
    maxZoom: 18,
    minZoom: 3,
    noWrap: true,
    id: 'openportguide_weather_waves',
    accessToken: '',
  }
);

const openportguideWeatherWaveheights24 = L.tileLayer(
  'https://weather.openportguide.org/tiles/actual/significant_wave_height/24h/{z}/{x}/{y}.png',
  {
    attribution:
      'Map tiles by <a href="https://weather.openportguide.org/">openportguide.org</a> under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0.</a>',
    maxZoom: 18,
    minZoom: 3,
    noWrap: true,
    id: 'openportguide_weather_waves',
    accessToken: '',
  }
);

const openportguideWeatherTemperature0 = L.tileLayer(
  'https://weather.openportguide.org/tiles/actual/air_temperature/0h/{z}/{x}/{y}.png',
  {
    attribution:
      'Map tiles by <a href="https://weather.openportguide.org/">openportguide.org</a> under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0.</a>',
    maxZoom: 18,
    minZoom: 3,
    noWrap: true,
    id: 'openportguide_weather_temperature',
    accessToken: '',
  }
);

const openportguideWeatherTemperature6 = L.tileLayer(
  'https://weather.openportguide.org/tiles/actual/air_temperature/6h/{z}/{x}/{y}.png',
  {
    attribution:
      'Map tiles by <a href="https://weather.openportguide.org/">openportguide.org</a> under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0.</a>',
    maxZoom: 18,
    minZoom: 3,
    noWrap: true,
    id: 'openportguide_weather_temperature',
    accessToken: '',
  }
);

const openportguideWeatherTemperature12 = L.tileLayer(
  'https://weather.openportguide.org/tiles/actual/air_temperature/12h/{z}/{x}/{y}.png',
  {
    attribution:
      'Map tiles by <a href="https://weather.openportguide.org/">openportguide.org</a> under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0.</a>',
    maxZoom: 18,
    minZoom: 3,
    noWrap: true,
    id: 'openportguide_weather_temperature',
    accessToken: '',
  }
);

const openportguideWeatherTemperature24 = L.tileLayer(
  'https://weather.openportguide.org/tiles/actual/air_temperature/24h/{z}/{x}/{y}.png',
  {
    attribution:
      'Map tiles by <a href="https://weather.openportguide.org/">openportguide.org</a> under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0.</a>',
    maxZoom: 18,
    minZoom: 3,
    noWrap: true,
    id: 'openportguide_weather_temperature',
    accessToken: '',
  }
);

const openportguideWeatherPrecipitation0 = L.tileLayer(
  'https://weather.openportguide.org/tiles/actual/precipitation_shaded/0h/{z}/{x}/{y}.png',
  {
    attribution:
      'Map tiles by <a href="https://weather.openportguide.org/">openportguide.org</a> under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0.</a>',
    maxZoom: 18,
    minZoom: 3,
    noWrap: true,
    id: 'openportguide_weather_temperature',
    accessToken: '',
  }
);

const openportguideWeatherPrecipitation6 = L.tileLayer(
  'https://weather.openportguide.org/tiles/actual/precipitation_shaded/6h/{z}/{x}/{y}.png',
  {
    attribution:
      'Map tiles by <a href="https://weather.openportguide.org/">openportguide.org</a> under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0.</a>',
    maxZoom: 18,
    minZoom: 3,
    noWrap: true,
    id: 'openportguide_weather_temperature',
    accessToken: '',
  }
);

const openportguideWeatherPrecipitation12 = L.tileLayer(
  'https://weather.openportguide.org/tiles/actual/precipitation_shaded/12h/{z}/{x}/{y}.png',
  {
    attribution:
      'Map tiles by <a href="https://weather.openportguide.org/">openportguide.org</a> under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0.</a>',
    maxZoom: 18,
    minZoom: 3,
    noWrap: true,
    id: 'openportguide_weather_temperature',
    accessToken: '',
  }
);

const openportguideWeatherPrecipitation24 = L.tileLayer(
  'https://weather.openportguide.org/tiles/actual/precipitation_shaded/24h/{z}/{x}/{y}.png',
  {
    attribution:
      'Map tiles by <a href="https://weather.openportguide.org/">openportguide.org</a> under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0.</a>',
    maxZoom: 18,
    minZoom: 3,
    noWrap: true,
    id: 'openportguide_weather_temperature',
    accessToken: '',
  }
);

// Prepare layer trees for layers control
export const baseTree: L.Control.Layers.TreeObject = {
  label: '<b>Map Tiles</b>',
  children: [
    ...localTiles,
    {
      label: 'OSM Server',
      layer: lightTiles,
    },
  ],
};

export const overlaysTree: L.Control.Layers.TreeObject = {
  label: '<b>Further Information</b>',
  children: [
    {
      label: 'Layer labels',
      layer: layerLabels,
    },
    {
      label: 'Item labels',
      layer: itemLabels,
    },

    {
      label: 'Drawn Areas & Markers',
      layer: drawnAreasAndMarkers,
    },
    {
      label: 'All Ships in Area <span id="ship_count"></span>',
      layer: allShipsLayer,
    },
    {
      label: 'All Aircrafts in Area <span id="aircraft_count"></span>',
      layer: allAircraftLayer,
    },
    {
      label: 'West Med SAR Zones',
      layer: westMediterraneanSarZonesFeatureGroup,
    },
    {
      label: 'Central Med SAR Zones',
      layer: centralMediterraneanSarZonesFeatureGroup,
    },
    {
      label: 'East Med SAR Zones',
      layer: eastMediterraneanSarZonesFeatureGroup,
    },
    {
      label: 'NoFly Zones',
      layer: noflyZonesFeatureGroup,
    },
    {
      label: '12&hairsp;&&hairsp;24NM zones',
      layer: nmZonesFeatureGroup,
    },
    {
      label: 'IFR waypoints',
      layer: ifrWaypoints,
    },
    {
      label: 'Time zones',
      layer: ne10mTimeZones,
    },
    {
      label: 'Graticules',
      layer: graticulesFeatureGroup,
    },
    {
      label: 'Open Sea Map',
      layer: seamapTiles,
    },
    {
      label: 'Ferry Lines',
      layer: ne10mFerriesCr,
    },
    { label: 'Ports', layer: worldPortIndex2019 },
  ],
};

export const currentWeatherTree: L.Control.Layers.TreeObject = {
  label: '<b>Current Weather</b>',
  children: [
    {
      label: 'Wind intensity in 10m height (Bft)',
      layer: openportguideWeatherWindstream0,
    },
    {
      label: 'Wind intensity near ground (Bft)',
      layer: openportguideWeatherWindGust0,
    },
    {
      label: 'Air temperature (°C)',
      layer: openportguideWeatherTemperature0,
    },
    {
      label: 'Wave heights (m)',
      layer: openportguideWeatherWaveheights0,
    },
    {
      label: 'Precipitation (mm/m²h)',
      layer: openportguideWeatherPrecipitation0,
    },
  ],
};

export const WeatherForecast6hTree: L.Control.Layers.TreeObject = {
  label: '<b>6h forecasts</b>',
  collapsed: true,
  children: [
    {
      label: 'Wind intensity in 10m height (Bft)',
      layer: openportguideWeatherWindstream6,
    },
    {
      label: 'Wind intensity near ground (Bft)',
      layer: openportguideWeatherWindGust6,
    },
    {
      label: 'Air temperature (°C)',
      layer: openportguideWeatherTemperature6,
    },
    {
      label: 'Wave heights (m)',
      layer: openportguideWeatherWaveheights6,
    },
    {
      label: 'Precipitation (mm/m²h)',
      layer: openportguideWeatherPrecipitation6,
    },
  ],
};

export const WeatherForecast12hTree: L.Control.Layers.TreeObject = {
  label: '<b>12h forecasts</b>',
  collapsed: true,
  children: [
    {
      label: 'Wind intensity in 10m height (Bft)',
      layer: openportguideWeatherWindstream12,
    },
    {
      label: 'Wind intensity near ground (Bft)',
      layer: openportguideWeatherWindGust12,
    },
    {
      label: 'Air temperature (°C)',
      layer: openportguideWeatherTemperature12,
    },
    {
      label: 'Wave heights (m)',
      layer: openportguideWeatherWaveheights12,
    },
    {
      label: 'Precipitation (mm/m²h)',
      layer: openportguideWeatherPrecipitation12,
    },
  ],
};

export const WeatherForecast24hTree: L.Control.Layers.TreeObject = {
  label: '<b>24h forecasts</b>',
  collapsed: true,
  children: [
    {
      label: 'Wind intensity in 10m height (Bft)',
      layer: openportguideWeatherWindstream24,
    },
    {
      label: 'Wind intensity near ground (Bft)',
      layer: openportguideWeatherWindGust24,
    },
    {
      label: 'Air temperature (°C)',
      layer: openportguideWeatherTemperature24,
    },
    {
      label: 'Wave heights (m)',
      layer: openportguideWeatherWaveheights24,
    },
    {
      label: 'Precipitation (mm/m²h)',
      layer: openportguideWeatherPrecipitation24,
    },
  ],
};

/**
 * These are the layers that show up at the start, when the map is first loaded.
 * (Layers are shown and hidden by adding and removing them from the map)
 */
export const startingLayers: (L.TileLayer | L.LayerGroup<any>)[] = [
  {
    greyscale: lightTiles,
    light: localTiles[0].layer,
    dark: localTiles[1].layer,
    extra: localTiles[2].layer,
  }[storage.get_map_layers_config().background_tiles] ?? lightTiles,
  drawnAreasAndMarkers,
  allLayers.CentralMedSarzones,
  allLayers.Nmzones,
  // all_layers.ships,
  // all_layers.aircrafts,
  ...startingDatabaseLayers,
];
