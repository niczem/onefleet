import * as L from 'leaflet';
import 'leaflet.control.layers.tree';
import 'leaflet.control.layers.tree/L.Control.Layers.Tree.css';

import {
  baseTree,
  overlaysTree,
  currentWeatherTree,
  WeatherForecast6hTree,
  WeatherForecast12hTree,
  WeatherForecast24hTree,
} from './layerData';

/**
 * Initialise the three layer-control buttons in the top right corner of the map.
 * @param map The map that the buttons will be added to.
 * @returns An array of length 3, which contains the (currently) 3 layerControls.
 */
export function initLayerControls(map: L.Map): L.Control.Layers.Tree[] {
  /* Setting collapsed to false disables the default opening/closing on hover but requires to close manually */
  const customLayerControlOptions: L.Control.Layers.TreeOptions = {
    collapsed: false,
  };

  const layerControlConfigs = {
    infoLayers: {
      baseTree: baseTree,
      overlayTree: overlaysTree,
    },
    weatherLayers: {
      baseTree: undefined,
      overlayTree: [
        currentWeatherTree,
        WeatherForecast6hTree,
        WeatherForecast12hTree,
        WeatherForecast24hTree,
      ],
    },
  };

  return Object.values(layerControlConfigs).map((controlConfig, index) =>
    _setUpLayerTreeControl(
      map,
      index,
      controlConfig.baseTree,
      controlConfig.overlayTree,
      customLayerControlOptions
    )
  );
}

function _setUpLayerTreeControl(
  map: L.Map,
  index: number,
  baseTree?: L.Control.Layers.TreeObject,
  overlayTree?: L.Control.Layers.TreeObject | L.Control.Layers.TreeObject[],
  options?: L.Control.Layers.TreeOptions
): L.Control.Layers.Tree {
  //@ts-expect-error
  const control = L.control.layers.tree(baseTree, overlayTree, options);
  control.addTo(map);
  control.collapse();
  //add a close button to the control
  const btn = L.DomUtil.create('div', 'layers_control_btn_class');
  control.getContainer()!.insertBefore(btn, control.getContainer()!.firstChild);
  //when close button is clicked close the control and hide the button
  L.DomEvent.on(btn, 'click', (e) => {
    btn.innerHTML = '<el-button class="layers-control-btn-closed"></el-button>';
    control.collapse();
    e.stopPropagation();
  });
  //when the layer control is opened show the close button
  const controlIcon = document.getElementsByClassName(
    'leaflet-control-layers-toggle'
  )[index];
  L.DomEvent.on(controlIcon as HTMLElement, 'mousedown', () => {
    control.expand();
    btn.innerHTML = '<el-button class="layers-control-btn-open"></el-button>';
  });
  return control;
}
