import { Icon } from 'leaflet';
export class IconHandler {
  private svgNamespaceURI = 'http://www.w3.org/2000/svg';
  private iconPointRadius = 2.5; // radius of the circle marker of each icon which indicates its exact position
  private speedfactor = 0.8; // determines how many pt represent 1NM/h in the speed indicator of vehicles
  private iconWidthHeight = 28; // default width and height of the square icons

  // determine at which distance from the icon center (0,0) the speed indicator should start:
  private vehicleIconOffset = 14;
  private aircraftIconOffset = 10;
  private caseIconOffset = 11;

  /**
   * This function can be used across the app to create svg icons for the different item types
   *
   * @param type The type of icon to be created (options: case, vehicle, landmark, aircraft)
   * @param heading Direction the vehicle is headed in degree. 0 = North, 90 = East etc.
   * @param colour of the icon
   * @param caption to be displayed next to the icon
   * @param speed Speed of the vehicle in NM/h
   * @param precision Precision of the position in plusminus meters --not yet implemented!
   *
   * @memberof IconHandler
   */
  public createIcon(
    type: string,
    heading: number,
    colour: string,
    speed?: number
  ): string {
    // Create a svg element with the appropriate dimensions.
    const iconSvg = document.createElementNS(this.svgNamespaceURI, 'svg');
    const iconDimensions: number = Object.values(
      this.determineIconSize(type, speed)
    )[2];
    // Make the center of the svg element the center of the local coordinate system:
    const svgViewPortPosition = -iconDimensions / 2;
    iconSvg.setAttribute('width', `${iconDimensions}`);
    iconSvg.setAttribute('height', `${iconDimensions}`);
    iconSvg.setAttribute(
      'viewBox',
      `${svgViewPortPosition} ${svgViewPortPosition} ${iconDimensions} ${iconDimensions}`
    ); // coordinate (0,0) is the center of the viewBox and the resulting icon

    // Attach a svg icon depending on the icon type
    if (type === 'vehicle' || type === 'ship') {
      iconSvg.appendChild(this.createVehicleIcon(colour, heading, speed));
    } else if (type === 'aircraft') {
      iconSvg.appendChild(this.createAircraftIcon(colour, heading, speed));
    } else if (type === 'case') {
      iconSvg.appendChild(this.createCaseIcon(colour, heading, speed));
    } else if (type === 'landmark') {
      iconSvg.appendChild(this.createLandmarkIcon(colour));
    } else if (type === 'sighting') {
      iconSvg.appendChild(this.createSightingIcon(colour));
    } else if (type === 'droppoint') {
      iconSvg.appendChild(this.createDroppointIcon(colour));
    } else {
      iconSvg.appendChild(this.createUnknownIcon(colour));
    }

    // Create the circle marker that all icons share to mark their exact position
    const iconPoint = document.createElementNS(this.svgNamespaceURI, 'circle');
    iconPoint.setAttribute('r', this.iconPointRadius.toString());
    iconPoint.setAttribute('fill', 'white');
    iconPoint.setAttribute('stroke-width', '1');
    iconPoint.setAttribute('stroke', colour);
    iconSvg.appendChild(iconPoint);
    return iconSvg.outerHTML;
  }

  /**
   * Creates a speed indicator for vehicle icons, similar to the one used in ECDIS.
   *
   * @remarks
   * The speed indicator consists of a dashed line. Each dash represents 1NM/h.
   * The red dash represents the remaining decimals.
   *
   * @param speed The speed of the vehicle in NM/h (knots)
   * @param heading Direction the vehicle is headed in degree. 0 = North, 90 = East etc.
   * @param colour of the icon
   * @param offset x-offset of the indicator position
   *
   * @memberof IconHandler
   */
  public createSpeedIndicator(
    speed: number,
    heading: number,
    colour: string,
    offset = 0
  ): Element {
    // create a svg line that changes depending on speed and heading
    const speedIndicator = document.createElementNS(
      this.svgNamespaceURI,
      'line'
    );
    // the line starts at point 1 with the coordinates (x1,y1).
    // it is located at the outer edge of each item type's circle marker + an individual offset)
    speedIndicator.setAttribute('x1', '0');
    speedIndicator.setAttribute(
      'y1',
      (-(offset + this.iconPointRadius)).toString()
    );
    // the line ends at point 2 with the coordinates (x2,y2).
    // the length of the line is determined by the speed of the vehicle
    speedIndicator.setAttribute('x2', '0');
    speedIndicator.setAttribute(
      'y2',
      (-(offset + this.iconPointRadius + this.speedfactor * speed)).toString()
    );
    // the line is coloured according to the colour of the overall icon
    speedIndicator.setAttribute('stroke', colour);
    // the line is rotated according to the heading of the vehicle
    speedIndicator.setAttribute('transform', 'rotate(' + heading + ')');
    return speedIndicator;
  }

  /**
   * Returns a svg element for the item type 'vehicle'
   *
   * @remarks
   * The svg element consists of a polyline (outline of a ship) and, if the speed parameter is given, a speed indicator.
   *
   * @param colour The colour of the svg element
   * @param heading Rotation of the svg element in degree. 0 = North, 90 = East etc.
   * @param speed The speed of the vehicle.
   *
   * @memberof IconHandler
   */
  public createVehicleIcon(
    colour: string,
    heading: number,
    speed?: number
  ): Element {
    // create svg container
    const vehicleIcon = document.createElementNS(this.svgNamespaceURI, 'g');
    // create speed indicator and attach it to svg container
    if (speed) {
      const speedIndicator = this.createSpeedIndicator(
        speed,
        heading,
        colour,
        this.vehicleIconOffset
      );
      vehicleIcon.appendChild(speedIndicator);
    }

    // create svg ship outline and attach it to svg container
    const shipOutline = document.createElementNS(
      this.svgNamespaceURI,
      'polyline'
    );
    shipOutline.setAttribute(
      'points',
      '-5,6 5,6 7,-7 6,-10 0,-14 -6,-10 -7,-7 -5,6'
    );
    shipOutline.setAttribute('fill', 'none');
    shipOutline.setAttribute('stroke', colour);
    shipOutline.setAttribute('transform', 'rotate(' + heading + ')');
    vehicleIcon.appendChild(shipOutline);

    return vehicleIcon;
  }

  /**
   * Returns a svg element for the item type 'aircraft'
   *
   * @remarks
   * The svg element consists of a path (outline of a plane) and, if the speed parameter is given, a speed indicator.
   *
   * @param colour The colour of the svg element
   * @param heading Rotation of the svg element in degree. 0 = North, 90 = East etc.
   * @param speed The speed of the aircraft.
   *
   * @memberof IconHandler
   */
  public createAircraftIcon(
    colour: string,
    heading: number,
    speed?: number
  ): Element {
    // create svg container
    const aircraftIcon = document.createElementNS(this.svgNamespaceURI, 'g');
    // create speed indicator and attach it to svg container
    if (speed) {
      const speedIndicator = this.createSpeedIndicator(
        speed,
        heading,
        colour,
        this.aircraftIconOffset
      );
      aircraftIcon.appendChild(speedIndicator);
    }

    const aircraftOutline = document.createElementNS(
      this.svgNamespaceURI,
      'path'
    );
    aircraftOutline.setAttribute(
      'd',
      'M 78.09 20.881 C 78.09 20.881 81.44200000000001 6.4719999999999995 69.602 16.469 L 58.488 36.071 L 19.339 35.68 L 13.755999999999998 44.559 L 48.884 51.205999999999996 L 40.003 67.848 L 30.622 68.576 L 26.32 74.496 L 39.669 77.455 L 47.208 87.06099999999999 L 50.727 80.806 L 47.207 71.2 L 57.762 55.620000000000005 L 80.884 82.25800000000001 L 86.244 73.043 L 67.368 39.757 L 78.09 20.881'
    );
    aircraftOutline.setAttribute('stroke-width', '0');
    aircraftOutline.setAttribute(
      'transform',
      'scale(0.25), rotate(' + (-30 + heading) + '), translate(-56, -50)'
    );
    aircraftOutline.setAttribute('fill', colour);
    aircraftIcon.appendChild(aircraftOutline);

    return aircraftIcon;
  }

  /**
   * Returns a svg element for the item type 'case'
   *
   * @remarks
   * The svg element consists of a polyline (case symbol) and, if the speed parameter is given, a speed indicator.
   *
   * @param colour The colour of the svg element
   * @param heading Rotation of the svg element in degree. 0 = North, 90 = East etc.
   * @param speed The speed of the boat.
   *
   * @memberof IconHandler
   */
  public createCaseIcon(
    colour: string,
    heading: number,
    speed?: number
  ): Element {
    // create svg container
    const caseIcon = document.createElementNS(this.svgNamespaceURI, 'g');

    // create speed indicator and attach it to svg container
    if (speed) {
      const speedIndicator = this.createSpeedIndicator(
        speed,
        heading,
        colour,
        this.caseIconOffset
      );
      caseIcon.appendChild(speedIndicator);
    }

    // create boat icon and attach it to svg container
    const boatIcon = document.createElementNS(this.svgNamespaceURI, 'polyline');
    boatIcon.setAttribute('points', '0,-12 10,10 0,0 -10,10 0,-12');
    boatIcon.setAttribute('fill', 'none');
    boatIcon.setAttribute('stroke', colour);
    boatIcon.setAttribute('transform', 'scale(0.9), rotate(' + heading + ')');
    caseIcon.appendChild(boatIcon);

    return caseIcon;
  }

  /**
   * Returns a svg element for the item type 'landmark'
   *
   * @remarks
   * The svg element consists of a polyline (landmark symbol)
   *
   * @param colour The colour of the svg element
   *
   * @memberof IconHandler
   */
  public createLandmarkIcon(colour: string): Element {
    const iconLandmark = document.createElementNS(
      this.svgNamespaceURI,
      'polyline'
    );
    iconLandmark.setAttribute('points', '0,0 0,-10 4,-10');
    iconLandmark.setAttribute('fill', 'none');
    iconLandmark.setAttribute('stroke', colour);

    return iconLandmark;
  }

  /**
   * Returns a svg element for the item type 'sighting'
   *
   * @remarks
   * The svg element consists of a polyline (star symbol)
   *
   * @param colour The colour of the svg element
   *
   * @memberof IconHandler
   */
  public createSightingIcon(colour: string): Element {
    const starOutline = document.createElementNS(
      this.svgNamespaceURI,
      'polyline'
    );
    starOutline.setAttribute(
      'points',
      '6.5,0 9,5 14,5.5 10.5,9 12,14 6.5,11.5 2,14 3.5,9 0,5.5 5,5 6.5,0'
    );
    starOutline.setAttribute('fill', 'none');
    starOutline.setAttribute('transform', 'scale(1.4), translate(-7,-8)');
    starOutline.setAttribute('stroke', colour);

    return starOutline;
  }

  /**
   * Returns a svg element for the item type 'droppoint'
   *
   * @remarks
   * The svg element consists of a path (warning sign).
   *
   * @param colour The colour of the svg element
   * @memberof IconHandler
   */
  public createDroppointIcon(colour: string): Element {
    const dropIcon = document.createElementNS(this.svgNamespaceURI, 'path');
    dropIcon.setAttribute(
      'd',
      'M 0 0 L -5 -15 L 5 -15 L 0 0 M 0 -13 L 0 -9 M 0 -8 L 0 -7 Z'
    );
    dropIcon.setAttribute('fill', 'none');
    dropIcon.setAttribute('stroke', colour);

    return dropIcon;
  }

  /**
   * Returns a default svg element for unknown item types
   *
   * @remarks
   * The svg element consists of a path (rect box) containing a question mark.
   *
   * @param colour The colour of the svg element   *
   * @memberof IconHandler
   */
  public createUnknownIcon(colour: string): Element {
    const unknownIcon = document.createElementNS(this.svgNamespaceURI, 'path');
    unknownIcon.setAttribute(
      'd',
      'M -5 5 L -5 -12 L 5 -12 L 5 5 L -5 5 M -1 -9 L 0 -10 L 1 -9.7 L 1 -8 L 0 -7 L 0 -6 M 0 -5 L 0 -4 Z'
    );
    unknownIcon.setAttribute('fill', 'none');
    unknownIcon.setAttribute('stroke', colour);

    return unknownIcon;
  }

  /**
   * Returns the default Marker icon
   *
   * @remarks
   * This is a fix to circumvent a bug where leaflet marker image does not get bundled by Rollup and thus
   * the marker displays an "image not found icon". This bug does not exist on the dev server. Another possible solution could be
   * to change Vite's options on static asset handling.
   */
  public createMarkerIcon(): Icon {
    return new Icon({
      iconUrl: 'css/images/marker-icon.png',
      iconSize: [25, 41],
      iconAnchor: [12, 41],
      popupAnchor: [1, -34],
      shadowUrl: 'css/images/marker-shadow.png',
      shadowSize: [41, 41],
      shadowAnchor: [12, 41],
      iconRetinaUrl: 'css/images/marker-icon-2x.png',
    });
  }

  /**
   * This function calculates the minimum size an icon should have
   *
   * @remarks
   * Minimizing the icon size improves the usability of the map because icons take up less space and thus interfere less with other map items.
   * This function returns the L.DivIcon properties iconSize and iconAnchor as well as the minimum required icon width and height
   *
   * @param icontype The type of the icon to be created (case, vehicle, aircraft etc)
   * @param speed The speed of the icon to be created
   *
   * @memberof IconHandler
   */
  public determineIconSize(icontype: string, speed?: number): Object {
    if (speed) {
      // the icon size depends on the length of the speed indicator and item type
      if (icontype === 'vehicle') {
        this.iconWidthHeight =
          2 * (speed * this.speedfactor + this.vehicleIconOffset);
      } else if (icontype === 'case') {
        this.iconWidthHeight =
          2 * (speed * this.speedfactor + this.caseIconOffset);
      } else if (icontype === 'aircraft') {
        this.iconWidthHeight =
          2 * (speed * this.speedfactor + this.aircraftIconOffset);
      }
    }
    // returns the iconSize and iconAnchor for L.DivIcon creation and the minimum icon width and height that is required for icon creation
    return {
      iconSize: [this.iconWidthHeight, this.iconWidthHeight],
      iconAnchor: [this.iconWidthHeight / 2, this.iconWidthHeight / 2],
      iconWidthHeight: this.iconWidthHeight,
    };
  }
}

export default new IconHandler();
