import * as L from 'leaflet';

import 'leaflet-draw';
import 'leaflet-mouse-position';
import 'leaflet.polylinemeasure';
import 'leaflet.polylinemeasure/Leaflet.PolylineMeasure.css';
import 'leaflet-draw/dist/leaflet.draw.css';
import 'leaflet-mouse-position/src/L.Control.MousePosition.css';

import { drawnAreasAndMarkers } from './layerData';
import type {
  DrawnAreaPopupData,
  DrawnMarkerPopupData,
  DrawnTrackPopupData,
  PopupData,
} from '@/types/popup';
import {
  asDD,
  asDMS,
  convertPositionsLeafletToOneFleet,
} from './positionUtils';
import iconHandler from './iconHandler';

/* Measurement Tools for measuring distances and angles */
export function initMeasurementTool(): L.Control.PolylineMeasure {
  return L.control.polylineMeasure({
    // config follows https://developer.aliyun.com/mirror/npm/package/leaflet.polylinemeasure
    position: 'topleft',
    unit: 'nauticalmiles',
    measureControlTitleOn: 'Measure Distances and Angles', // Title for the control going to be switched on
    measureControlTitleOff: 'Clear Measurements', // Title for the control going to be switched off
    measureControlLabel: '&#8614;', // HTML to place inside the control. Default: &#8614;
    backgroundColor: '#8f8', // Background color for control when selected. Default: #8f8
    showBearings: true,
    // clearMeasurementsOnStop: false,
    // showClearControl: true,
    // showUnitControl: false,
  } as L.Control.PolylineMeasureOptions);
}

/* Show the zoom scale in bottom left corner */
export function initScaleDisplay() {
  return L.control.scale({ imperial: false });
}

export function initMouseCoordinatesDisplay() {
  return L.control.mousePosition({
    position: 'bottomright',
    emptyString: '',
    formatter: (lng: number, lat: number) => {
      return (
        asDMS({ lat: lat, lon: lng }) + ' | ' + asDD({ lat: lat, lon: lng })
      );
    },
  });
}

/** Add a toggle to create a new custom marker */
export function initMarkerPlacementControl(
  vuePopupContentsGetter: (
    popupRef: string,
    popupData: PopupData
  ) => string | HTMLElement
) {
  const toggleCustomMarkerControl = L.Control.extend({
    options: {
      position: 'topleft',
    },

    onAdd: () => {
      const container = L.DomUtil.create(
        'div',
        'leaflet-bar leaflet-control leaflet-control-custom leaflet-bar'
      );

      const aTag = L.DomUtil.create('a', 'text-xl', container);
      aTag.innerText = '✛'; // icon
      aTag.title = 'Enter coordinates of a specific location';

      // We must prevent the double click of the button, otherwise the map zoomes in if button is double clicked.
      container.ondblclick = (e) => e.stopImmediatePropagation();

      container.onclick = () => vuePopupContentsGetter('addmarker', {});

      return container;
    },
  });
  return new toggleCustomMarkerControl();
}

/**
 * Sets up the ability to draw shapes on the map, including dropping markers and what to do when clicked.
 */
export function initShapeDrawingControl(
  map: L.Map,
  vuePopupContentsGetter: (
    popupRef: string,
    popupData: PopupData
  ) => string | HTMLElement
): void {
  /* Add a layer for drawing shapes in. These can then be added to items as positions, areas, etc */

  //** Add the standard Map actions. */
  const drawing_toolbar = new L.Control.Draw({
    draw: {
      polyline: false,
      polygon: false,
      rectangle: false,
      circle: false,
      circlemarker: false,
      marker: {
        icon: iconHandler.createMarkerIcon(),
      },
    },
  });
  map.addControl(drawing_toolbar);

  // Object created - bind popup to layer, add to feature group
  map.on(L.Draw.Event.CREATED, (event: L.LayerEvent) => {
    const drawnMarkerAreaOrTrack: L.Layer = event.layer;
    addPopupContentsToMarkerAndPlaceOnMap(
      drawnMarkerAreaOrTrack,
      vuePopupContentsGetter,
      map
    );
  });
}

/**
 * This function uses the vuePopupContentsGetter to retrieve the correct vue component instance
 * and sets it up to be displayed as the popup whenever the given marker/area/track is clicked.
 * @param drawnMarkerAreaOrTrack The leaflet layer (marker or track or area) that will be given a popup.
 * @param vuePopupContentsGetter The getter function that returns the correct vue component instance upon request.
 * @param map The leaflet map instance.
 */
export function addPopupContentsToMarkerAndPlaceOnMap(
  drawnMarkerAreaOrTrack: L.Layer,
  vuePopupContentsGetter: (
    popupRef: string,
    popupData: PopupData
  ) => string | HTMLElement,
  map: L.Map
) {
  const popupContent = _getDrawnShapePopupContent(
    drawnMarkerAreaOrTrack,
    vuePopupContentsGetter
  );
  if (popupContent !== null) {
    drawnMarkerAreaOrTrack.bindPopup(popupContent);
  }
  drawnAreasAndMarkers.addLayer(drawnMarkerAreaOrTrack);
  drawnAreasAndMarkers.addTo(map); // ensure the draw-layer is always shown whenever something is drawn
}

/**
 * Generate popup content based on layer type
 * @param layer A vue marker, track (L.Polyline) or area (L.Polygon) for which we need some content for a popup.
 * @param vuePopupContentsGetter The getter function that returns the correct vue component instance upon request.
 * @returns Anything that is accepted by Layer.bindPopup(). May even be a Vue Component or simply just a string or null.
 */
function _getDrawnShapePopupContent(
  layer: L.Layer,
  vuePopupContentsGetter: (
    popupRef: string,
    popupData: PopupData
  ) => string | HTMLElement
): string | null | any {
  let latlngs, distance: number;
  if (
    layer instanceof L.Marker ||
    layer instanceof L.CircleMarker ||
    layer instanceof L.Circle
  ) {
    // Marker - add lat/long
    return (map_object) => {
      const popup_data: DrawnMarkerPopupData = {
        position: {
          lat: map_object.getLatLng().lat,
          lon: map_object.getLatLng().lng,
        },
        marker: map_object,
        // item_title: 'Unknown Item',
        // radius: map_object.getRadius ? map_object.getRadius() : 0,
      };
      return vuePopupContentsGetter('drawn_marker_popup', popup_data);
    };
  } else if (layer instanceof L.Polygon) {
    // Rectangle/Polygon - area
    return (map_object) => {
      // let latlngs = layer.getLatLngs();
      const area_positions = map_object._defaultShape
        ? map_object._defaultShape()
        : map_object.getLatLngs();
      const area_geodesic = L.GeometryUtil.geodesicArea(area_positions);
      const popup_data: DrawnAreaPopupData = {
        area_readable: L.GeometryUtil.readableArea(area_geodesic, true),
        positions: convertPositionsLeafletToOneFleet(area_positions),
      };
      return vuePopupContentsGetter('drawn_area_popup', popup_data);
    };
  } else if (layer instanceof L.Polyline)
    // Polyline - distance
    return () => {
      let popup_data: DrawnTrackPopupData;
      // layer = layer as any;
      // latlngs = layer._defaultShape
      //   ? layer._defaultShape()
      //   : layer.getLatLngs();
      latlngs = layer.getLatLngs();
      distance = 0;
      if (latlngs.length > 1) {
        for (let i = 0; i < latlngs.length - 1; i++) {
          distance += latlngs[i].distanceTo(latlngs[i + 1]);
        }
        popup_data = {
          distance_in_meters: distance,
          positions: convertPositionsLeafletToOneFleet(latlngs),
        };
      } else {
        popup_data = {
          distance_in_meters: -1,
          positions: [],
        };
      }
      return vuePopupContentsGetter('drawn_track_popup', popup_data);
    };
  return null;
}
