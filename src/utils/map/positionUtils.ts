import * as L from 'leaflet';
import templates from '@/constants/templates';
import type { MapTracksConfig } from '@/types/config';
import type { DbItem, DbItemMapSettings } from '@/types/db-item';
import type { MapCoordinates, NewPosition } from '@/types/db-position';
import { formatCoordinates, COORDINATE_FORMATS } from 'coordinate-formats';
import storageWrapper from '../storageWrapper';
import { computeOldestDateIso } from '../datetimeUtils';

/**
 * Check whether a given position is valid.
 * Valid positions have valid coordinates and a valid timestamp.
 */
export function positionIsValid(position: NewPosition): boolean {
  return timestampIsValid(position.timestamp) && coordinatesAreValid(position);
}
export function coordinatesAreValid(position: MapCoordinates): boolean {
  try {
    return (
      parseFloat(`${position.lat}`) == position.lat &&
      parseFloat(`${position.lon}`) == position.lon
    );
  } catch {
    return false;
  }
}
export function timestampIsValid(timestamp_iso: string): boolean {
  try {
    return new Date(timestamp_iso).toISOString() == timestamp_iso;
  } catch {
    return false;
  }
}

/**
 * Creates a new map_settings object from a given DbItem.
 * This function is needed during the transition from property-based tracking info
 * to having all tracking info contained in a map_settings section of each item
 * and can be deleted after all existing items in the DB have been migrated.
 *
 * By definition, this function is NOT template-agnostic. But it is needed for us to
 * become template-agnostic without just throwing away all existing data.
 *
 * @param item The existing DbItem from which the tracking info should be extracted.
 * @param marker_icon_id The marker icon is *usually* just the template name.
 * @returns A populated map_settings object that can be directly assigned to a DbItem.
 */
export function convertTrackingPropertiesToMapSettings(
  item: DbItem,
  marker_icon_id: string
): DbItemMapSettings {
  return {
    marker_color:
      item.properties['color'] ?? item.properties['boat_color'] ?? 'black',

    marker_icon: marker_icon_id,

    get_historical_data_since: parseInt(
      item.properties['get_historical_data_since'] ?? '-1',
      10
    ),

    tracking_type:
      item.properties['tracking_type'] == 'AIS'
        ? 'AIS'
        : item.properties['tracking_type'] == 'IRIDIUMGO'
        ? 'IRIDIUMGO'
        : item.properties['tracking_type'] == 'ADSB'
        ? 'ADSB'
        : 'MANUAL',

    tracking_id:
      item.properties['MMSI'] ??
      item.properties['iridium_sender_mail'] ??
      item.properties['ADSB'],
    tracking_token: item.properties['fleetmon_vessel_id'],
  };
}

/**
 * Converts an array of leaflet LatLng objects to OneFleet objects.
 * Note: `lng` gets changed to `lon` because the consuming code expects that.
 * @param latlngs An array of leaflet positions
 * @returns An array of OneFleet-positions (MapCoordinates)
 */
export function convertPositionsLeafletToOneFleet(
  latlngs: L.LatLng[]
): MapCoordinates[] {
  return latlngs.map((pos: L.LatLng) => ({ lat: pos.lat, lon: pos.lng }));
}
/**
 * Converts an array of OneFleet position objects to leaflet positions
 * Note: `lon` gets changed to `lng` because the consuming code expects that.
 * @param latlons An array of OneFleet-positions (MapCoordinates)
 * @returns An array of leaflet positions
 */
export function convertPositionsOneFleetToLeaflet(
  latlons: MapCoordinates[]
): L.LatLng[] {
  return latlons.map((pos: MapCoordinates) => new L.LatLng(pos.lat, pos.lon));
}

/**
 * Compute the item-specific track config. Used by getPositionsForDbItemPromise() and some UI info feedback.
 *
 * This can differ from the common tracks config due to item-specific map-settings.
 * Also, this function resolves things like null-values for `newest_date_iso`,
 * which should always be filled-in with the current time, and restricts the maximum
 * number of polled positions per item to 9999, as anything above that value is probably a bug.
 */
export function computeItemSpecificTracksConfig(
  base_item: DbItem,
  common_tracks_config: MapTracksConfig
): MapTracksConfig {
  // Use "now" if newest_date_iso is null
  const newestDate = common_tracks_config.newest_date_iso
    ? new Date(common_tracks_config.newest_date_iso)
    : new Date(); // now

  // use shorter names for the two past-days settings:
  /** item-specific days should always take effect whenever they are set. */
  let pastDaysItem = base_item.map_settings?.get_historical_data_since;
  /** global days should only be used if type is 'number_of_days'. */
  let pastDaysGlobal =
    common_tracks_config.type == 'number_of_days'
      ? common_tracks_config.show_past_days
      : undefined;

  // treat '-1' as the undefined value it should be:
  if (!pastDaysItem || (pastDaysItem ?? -1) < 0) {
    pastDaysItem = undefined;
  }
  if (!pastDaysGlobal || (pastDaysGlobal ?? -1) < 0) {
    pastDaysGlobal = undefined;
  }

  let oldest_date_iso = common_tracks_config.oldest_date_iso;
  // only change the oldest_date_iso if either pastDaysItem or pastDaysGlobal is defined:
  if (pastDaysItem || pastDaysGlobal) {
    // item-specific settings take precedence over global days settings (the 8 is just for typing):
    const daysToSubtract = pastDaysItem ?? pastDaysGlobal ?? 8;
    // shift our newest_date backwards in time by daysToSubtract to get a new oldest_date:
    oldest_date_iso = computeOldestDateIso(daysToSubtract, newestDate);
  }

  return {
    type: common_tracks_config.type,
    newest_date_iso: newestDate.toISOString(),
    oldest_date_iso: oldest_date_iso,
    length_limit: Math.min(common_tracks_config.length_limit, 9999),
    show_past_days: common_tracks_config.show_past_days,
  };
}

/**
 * Decide whether a given timestamp fits into the currently set MapTracksConfig or item-specific MapTracksConfig.
 * This function can be used for filtering which positions should actually be displayed on the map, even if more than those were loaded from the database.
 *
 * @param timestamp ISO-string of a position's timestamp
 * @param item_tracks_config item-specific MapTracksConfig - Use with computeItemSpecificTracksConfig() above
 * @returns A decision whether the given timestamp is within the time range defined by the current maptracksconfig
 */
export function isWithinTrackRange(
  timestamp: string,
  item_tracks_config: MapTracksConfig
): boolean {
  let withinTrackRange = true;
  if (item_tracks_config.newest_date_iso) {
    withinTrackRange =
      withinTrackRange && timestamp <= item_tracks_config.newest_date_iso;
  }
  if (item_tracks_config.oldest_date_iso) {
    withinTrackRange =
      withinTrackRange && timestamp >= item_tracks_config.oldest_date_iso;
  }
  return withinTrackRange;
}
/**
 * Determines the number of days to sync for the second initial
 * replication based on the type of tracks config
 *
 * @param overrideDaysToSubtract Allows us to override the number of days to be replicated, e.g. during initial onetime replication.
 */
export function whatDaysShouldBeReplicated(
  overrideDaysToSubtract?: number
): [startDateIso: string, endDateIso?: string] {
  const mapTracksConfig: MapTracksConfig =
    storageWrapper.get_map_tracks_config();
  const newestDateIso = mapTracksConfig.newest_date_iso ?? undefined;

  if (overrideDaysToSubtract) {
    return [
      computeOldestDateIso(overrideDaysToSubtract, newestDateIso),
      newestDateIso,
    ];
  }
  switch (mapTracksConfig.type) {
    case 'number_of_days':
      return [
        computeOldestDateIso(mapTracksConfig.show_past_days, newestDateIso),
        newestDateIso,
      ];
    case 'date_range':
      return [mapTracksConfig.oldest_date_iso, newestDateIso];
    default:
      throw new Error('Unexpected MapTracksConfig.type!');
  }
}

/**
 * Decide whether to override the response of isWithinTrackRange for a given item.
 * This is because some items should always be shown on the map, even if their position
 * timestamp is not within the currently selected track daterange.
 * @param base_item The item that decides whether its marker will override track range settings.
 *   No item-specific settings are currently supported and only the
 *   template setting `always_show_markers` is used to decide.
 */
export function isAlwaysShowMarker(base_item: DbItem): boolean {
  return templates.get(base_item.template).always_show_markers ?? false;
}

/** Public helper function to format UI position objects as Degrees, Minutes, Seconds */
export function asDMS(position: MapCoordinates | null): string {
  if (!position) return 'No Position';
  const coords = { latitude: position.lat, longitude: position.lon };
  return formatCoordinates(coords, { format: COORDINATE_FORMATS.DMS });
}

/** Public helper function to format UI position objects as Decimal Digits */
export function asDD(position: MapCoordinates | null): string {
  if (!position) return 'No Position';
  const coords = { latitude: position.lat, longitude: position.lon };
  return formatCoordinates(coords, { format: COORDINATE_FORMATS.DD });
}
