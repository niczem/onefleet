import * as L from 'leaflet';
import 'leaflet/dist/leaflet.css';

import storage from '@/utils/storageWrapper';
import * as map_utils from '@/utils/map/positionUtils';

import type { MapItem, ItemLeafletObjects } from '@/types/map-item';
import type { DbItem, DbItemMapSettings } from '@/types/db-item';
import type { DbPosition, DbPositionsPerItem } from '@/types/db-position';
import type { FilteredItemSection } from '@/types/item-filter-and-sorting';
import type { MapConfig, MapViewConfig } from '@/types/config';
import {
  layerLabels,
  allLayersWithLabels,
  loadedDatabaseLayers,
  classicItemsLayer,
  allLayers,
  itemLabels,
  startingLayers,
} from './map/layerData';

import type { ItemPopupData, PopupData } from '@/types/popup';
import iconHandler from './map/iconHandler';
import { initLayerControls } from './map/layerControls';
import {
  addPopupContentsToMarkerAndPlaceOnMap,
  initMarkerPlacementControl,
  initMeasurementTool,
  initMouseCoordinatesDisplay,
  initScaleDisplay,
  initShapeDrawingControl,
} from './map/otherControls';
import {
  loadDynamicGeoJSONLayers,
  loadStaticGeoJSONLayers,
  setupLabelListeners,
} from './map/layerGeojsonHelpers';

/**
 * The mapWrapper is an abstraction layer from the underlying mapping backend. (Currently leaflet.js)
 * It also provides some convenience methods for recurring map-related tasks.
 */
export class MapWrapper {
  public map!: L.Map;
  public map_config!: MapConfig;
  /**
   * The vuePopupContentsGetter is the mapWrapper's link to Vue.
   * It is used to retrieve Vue components to be used as contents for popups, tooltips, etc.
   * It is created as a headless function in Vue (MapArea.vue) and passed into mapWrapper at initialisation time.
   */
  private vuePopupContentsGetter!: (
    popupRef: string,
    popupData: PopupData
  ) => string | HTMLElement;
  /**
   * Keeps references to leaflet objects for modifying them later.
   * It is currently in question whether keeping references to Leaflet objects after passing
   * them into Leaflet might be a cause for slowdowns we have observed in OneFleet.
   */
  private loaded_items: Record<string, ItemLeafletObjects> = {};

  /**
   * Initialises the map backend component.
   * @param mapId The id of the div element that this map will be placed in
   * @param map_config The map configuration to initialise the map with.
   * @param vuePopupContentsGetter A callable that provides a div element for popups when requested.
   * @memberof mapWrapper
   */
  public init(
    mapId: string,
    map_config: MapConfig,
    vuePopupContentsGetter: (
      popupRef: string,
      popupData: PopupData
    ) => string | HTMLElement
  ): void {
    this.map_config = map_config;
    this.vuePopupContentsGetter = vuePopupContentsGetter;

    /** Map Zoom and Center */
    let map_view: MapViewConfig;
    try {
      map_view = JSON.parse(storage.get('map_view') ?? '');
    } catch (err) {
      map_view = {
        zoom: 6,
        center: [36.2, 17.5],
      };
    }

    /** instantiate Map object start */
    this.map = L.map(mapId, {
      center: map_view.center,
      zoom: map_view.zoom,
      zoomSnap: 1.0,
      layers: startingLayers,
    });
    /** instantiate Map object end */

    /** Set up layer control buttons */
    initLayerControls(this.map);

    /* Show the zoom scale in bottom left corner */
    initScaleDisplay().addTo(this.map);

    this.map.zoomControl.setPosition('bottomleft');

    /* Toggle to create extra control to add a custom marker by coordinates */
    initMarkerPlacementControl(this.vuePopupContentsGetter).addTo(this.map);

    /* Measurement Tools for measuring distances and angles */
    initMeasurementTool().addTo(this.map);

    /* Allows us to draw shapes on the map, including dropping markers and what to do when clicked. */
    initShapeDrawingControl(this.map, this.vuePopupContentsGetter);

    /* Add mouse coordinates */
    initMouseCoordinatesDisplay().addTo(this.map);

    /* Set up leaflet Event listeners that it uses for reacting to user-originated clicks on the map. */
    this.setupEventListeners();

    // load all GeoJSON layers once at startup...
    setTimeout(() => loadStaticGeoJSONLayers(), 1000); // execute 5 sec after loading page
    setTimeout(() => loadDynamicGeoJSONLayers(), 1000); // execute 5 sec after loading page
    // ...also schedule some of the GeoJSON layers for regular reloading:
    setInterval(() => loadDynamicGeoJSONLayers(), 300000); // every 5 minutes

    console.log('map initiated');
  }

  private setupEventListeners() {
    this.map.on('move', () => {
      const map_view = {
        zoom: (this.map as any)._zoom,
        center: [this.map.getCenter().lat, this.map.getCenter().lng],
      };
      storage.set('map_view', JSON.stringify(map_view));
    });

    // map label handling
    setupLabelListeners(layerLabels, allLayersWithLabels);
    setupLabelListeners(itemLabels, Object.values(loadedDatabaseLayers));
  }

  /**
   * Updates the map configuration, e.g. after settings have been changed in the Settings dialog.
   * @param new_map_config The new map configuration
   */
  public updateMapConfig(new_map_config: MapConfig): void {
    this.map_config = new_map_config;
  }

  /** Method calls the flyTo method from leaflet.js
   * See also: https://leafletjs.com/reference-1.0.0.html
   * Sets the view of the map (geographical center and zoom) performing a smooth pan-zoom animation.
   * @param {[number, number]} positions The latitude/longitude coordinates.
   * @param { zoom } zoom in if the current zoom level is smaller.
   */
  public flyTo(positions: [number, number], zoom?: number): void {
    this.map.flyTo(positions, Math.max(this.map.getZoom(), zoom || 0));
  }
  //remove/add layers when they are de-/selected in the LeftNavigation via checkbox
  public itemLayerVisibilityChangedByLeftNav(
    layerTitle: string,
    isSelected: boolean
  ): void {
    if (isSelected) {
      this.map.addLayer(loadedDatabaseLayers[layerTitle]);
    } else {
      this.map.removeLayer(loadedDatabaseLayers[layerTitle]);
    }
  }

  /**
   * Removes all layers from the map, thereby clearing it.
   * @deprecated this should no longer be needed
   */
  public clearMap(): void {
    let i = 0;
    this.map.eachLayer((layer) => {
      if (i > 0) this.map.removeLayer(layer);
      i++;
    });
  }

  /**
   * Updates a given item's line captions on the map
   */
  public updateLineCaptions(item: MapItem): void {
    for (
      let i = this.loaded_items[item.id].lineCaptions.length;
      i < item.positions.length - 1;
      i++
    ) {
      const v = item.positions[i];
      if (v.doc.lat && v.doc.lon) {
        const date = new Date(v.doc.timestamp);
        const caption =
          date.toISOString().slice(0, 10) +
          ' - ' +
          String(date.getHours()).padStart(2, '0') +
          ':' +
          String(date.getMinutes()).padStart(2, '0');
        const icon = L.divIcon({
          className: 'lineCaption',
          html: '<div>' + caption + '</div>',
        });
        this.loaded_items[item.id].lineCaptions.push(
          L.marker([v.doc.lat, v.doc.lon], { icon: icon })
        );
      }
    }
  }

  /**
   * Creates a new map_settings object from a given DbItem.
   * This function is needed during the transition from property-based tracking info
   * to having all tracking info contained in a map_settings section of each item
   * and can be deleted after all existing items in the DB have been migrated.
   *
   * By definition, this function is NOT template-agnostic. But it is needed for us to
   * become template-agnostic without just throwing away all existing data.
   *
   * @param item The existing DbItem from which the tracking info should be extracted.
   * @param marker_icon_id The marker icon is *usually* just the template name.
   * @returns A populated map_settings object that can be directly assigned to a DbItem.
   * @deprecated Moved to map_utils. Please use that function directly.
   */
  public convertTrackingPropertiesToMapSettings(
    item: DbItem,
    marker_icon_id: string
  ): DbItemMapSettings {
    return map_utils.convertTrackingPropertiesToMapSettings(
      item,
      marker_icon_id
    );
  }

  /**
   * Return a new leaflet.Polyline consisting of the positions of the item.
   */
  public generateLine(item: MapItem): L.Polyline {
    // use only those positions which actually have the fields "lat" and "lon" defined:
    const pointList: L.LatLngExpression[] = item.positions
      // .filter(v => v.doc.lat && v.doc.lon)
      .map((v) => [v.doc.lat, v.doc.lon]);

    if (pointList.length < 1) {
      throw new Error('An item track needs at least one defined position.');
    } else {
      const lastKnownPosition = item.positions[item.positions.length - 1];

      const track = new L.Polyline(pointList, {
        color: item.doc.map_settings.marker_color,
        weight: 3,
        opacity: 1,
        smoothFactor: 1,
      });

      // Use the item's identifier (and its name if applicable) as Pop-up
      let popuptitle = (item.doc.identifier || '').toString();
      if (item.doc.properties['name']) {
        popuptitle = item.doc.properties['name'];
      } else {
        popuptitle = item.id;
      }
      // bind the popup content to the marker. Used by openPopup() below
      track.bindPopup(() => {
        const popup_data: ItemPopupData = {
          item_id: item.id,
          item_title: popuptitle,
          latest_position: lastKnownPosition.doc,
        };
        return this.vuePopupContentsGetter('item_track_popup', popup_data);
      });

      return track;
    }
  }

  /**
   * Return a new leaflet.Marker at the last position of the item.
   * The marker uses a "cursor" icon and is rotated to reflect the heading
   * of the item at its last known position.
   */
  public generateMarker(item: MapItem): L.Marker {
    if (!item.positions || item.positions.length < 1) {
      throw new Error('An item marker needs at least one position.');
    }
    // use the last known position for the marker:
    const lastKnownPosition = item.positions[item.positions.length - 1];
    // if (!lastKnownPosition.doc.lat || !lastKnownPosition.doc.lon) {
    //   throw new Error('An item marker needs at least one defined position.');
    // }

    const icon = L.divIcon({
      className: item.doc.template,
      html: iconHandler.createIcon(
        item.doc.template,
        lastKnownPosition.doc.heading || 0,
        item.doc.map_settings.marker_color,
        item.positions[item.positions.length - 1].doc.speed || 0
      ),
      iconSize: Object.values(
        iconHandler.determineIconSize(
          item.doc.template,
          item.positions[item.positions.length - 1].doc.speed || 0
        )
      )[0],
      iconAnchor: Object.values(
        iconHandler.determineIconSize(
          item.doc.template,
          item.positions[item.positions.length - 1].doc.speed || 0
        )
      )[1],
    });

    // create a new marker with the given lat/lon position and icon
    const marker = L.marker(
      [lastKnownPosition.doc.lat, lastKnownPosition.doc.lon],
      { icon: icon, zIndexOffset: -1000 }
    );

    // add item captions (tooltips) to each marker but don't display by default (permanent=false)
    let caption;
    if (item.doc.properties['name']) {
      caption = item.doc.properties['name'];
    } else {
      caption = '';
    }
    marker
      .bindTooltip(caption, {
        permanent: false,
        direction: 'center',
        className: 'map-icon-labels',
      })
      .openTooltip();

    // Use the item's identifier (and its name if applicable) as Pop-up
    let popuptitle = (item.doc.identifier || '').toString();
    if (item.doc.properties['name']) {
      popuptitle = item.doc.properties['name'];
    } else {
      popuptitle = item.id;
    }
    // bind the popup content to the marker. Used by openPopup() below
    marker.bindPopup(() => {
      const popup_data: ItemPopupData = {
        item_id: item.id,
        item_title: popuptitle,
        latest_position: lastKnownPosition.doc,
      };
      return this.vuePopupContentsGetter('item_marker_popup', popup_data);
    });
    return marker;
  }

  /**
   * Adds the given item onto the map.
   * The given item will be prepared and replaces any previous
   * instances of this item on the map.
   *
   * @see this.generateLine() Used to generate a polyline for the item.
   * @see this.generateMarker() Used to generate a marker for the item
   */
  private _createItemMarkersTracksAreas(
    item: MapItem,
    target_layer_title?: string
  ): ItemLeafletObjects {
    if (item.positions.length < 1) {
      throw new Error(
        '_createItemMarkersTracksAreas() was called but no positions present!'
      );
    }

    return {
      marker: this.generateMarker(item),
      line: this.generateLine(item),
      lineCaptions: [],
      show_track: false, // see this.initialTrackVisibility()
      showing_on_layer: target_layer_title,
    };
  }

  /**
   * Updates a loaded item's position on the map and ensures it is visible.
   *
   * @see this.addItemToMap() Used to add an item to the map if not present.
   * @returns {boolean} False if the item has no positions; undefined otherwise.
   */
  public updateItemPosition(item: MapItem, target_layer_title?: string): void {
    try {
      // Only create marker and line objects if they have never been created before:
      if (!this.loaded_items[item.id]) {
        this.loaded_items[item.id] = this._createItemMarkersTracksAreas(
          item,
          target_layer_title
        );
      }

      // Convert all given positions of MapItem into an array of leaflet-compatible tuples:
      const leaflet_positions: L.LatLngTuple[] = item.positions.map((pos) => [
        pos.doc.lat,
        pos.doc.lon,
      ]);

      // Update coordinates of the item's marker and track:
      this.loaded_items[item.id].marker.setLatLng(
        leaflet_positions[leaflet_positions.length - 1]
      );
      this.loaded_items[item.id].line.setLatLngs(leaflet_positions);

      //Update the item's line captions
      if (this.map_config.layers.positiontimestamps) {
        this.updateLineCaptions(item);
      }

      // Ensure that the item is actually shown on the map:
      this._showItemMarkerOnMap(item.id, target_layer_title);
      this._showItemTrackOnMap(item.id, target_layer_title);
    } catch (e) {
      console.error(e, item);
    }
  }
  /**
   * Unbinds the previous popups of both the track and the marker of a given item,
   * and binds a new one with updated data.
   */
  public updatePopupContent(item: MapItem): void {
    const lastKnownPosition = item.positions[item.positions.length - 1];
    let popupTitle: string;
    if (item.doc.properties['name']) {
      popupTitle = item.doc.properties['name'];
    } else {
      popupTitle = item.id;
    }
    const popupData: ItemPopupData = {
      item_id: item.id,
      item_title: popupTitle,
      latest_position: lastKnownPosition.doc,
    };
    this.loaded_items[item.id].marker.unbindPopup();
    this.loaded_items[item.id].marker.bindPopup(() => {
      return this.vuePopupContentsGetter('item_marker_popup', popupData);
    });

    this.loaded_items[item.id].line.unbindPopup();
    this.loaded_items[item.id].line.bindPopup(() => {
      return this.vuePopupContentsGetter('item_track_popup', popupData);
    });
  }

  private _buildMapItemFromDbItem(
    base_item: DbItem,
    item_positions: Array<DbPosition>
  ): MapItem {
    return {
      id: base_item._id,
      doc: {
        template: base_item.template,
        identifier: base_item._id,
        properties: {
          name: base_item.properties.name as string,
          icon:
            base_item.properties['air'] === 'true'
              ? 'plane'
              : base_item.properties['icon'] ?? '',
        },
        map_settings:
          base_item.map_settings ??
          map_utils.convertTrackingPropertiesToMapSettings(
            base_item,
            base_item.template
          ),
      },
      positions:
        item_positions != undefined
          ? item_positions.map((item) => ({ doc: item })).reverse() // the old implementation assumes that the newest position is at the back of the array
          : [],
    };
  }

  /**
   * Sets a map item's visibility and adds any new positions it might have gained
   * If the item is not on the map yet, add it to the map.
   */
  public updateItemOnMap(
    base_item: DbItem,
    item_positions: Array<DbPosition>,
    show_item: boolean,
    target_layer_title?: string
  ): void {
    if (show_item) {
      item_positions = (item_positions ?? [])
        .filter((pos) => !pos.soft_deleted)
        .filter(
          (pos) =>
            map_utils.isAlwaysShowMarker(base_item) ||
            map_utils.isWithinTrackRange(
              pos.timestamp,
              map_utils.computeItemSpecificTracksConfig(
                base_item,
                storage.get_map_tracks_config()
              )
            )
        );
      // todo: only build this when really necessary
      const map_item = this._buildMapItemFromDbItem(base_item, item_positions);
      if (map_item.positions.length > 0) {
        this.updateItemPosition(map_item, target_layer_title);
        this.updatePopupContent(map_item);
      }
    } else {
      this.hideItem(base_item._id, target_layer_title);
    }
  }

  /**
   * Creates a marker at specific coordinates.
   */
  public addMarkerByCoordinates(lat: number, lon: number): void {
    this.flyTo([lat, lon]);
    const coords = new L.LatLng(lat, lon);
    const placedMarker = new L.Marker(coords, {
      icon: iconHandler.createMarkerIcon(),
    });
    addPopupContentsToMarkerAndPlaceOnMap(
      placedMarker,
      this.vuePopupContentsGetter,
      this.map
    );
  }

  /**
   * Hides the given item's marker from the map.
   */
  public hideItem(item_id: string, target_layer_title?: string): void {
    this.hideItemMarker(item_id, target_layer_title);
    this.hideItemTrack(item_id, target_layer_title);
  }

  /**
   * Hides the given item's marker from the map.
   */
  public hideItemMarker(item_id: string, target_layer_title?: string): void {
    const targetLayerGroup: L.LayerGroup = target_layer_title
      ? allLayers[target_layer_title]
      : classicItemsLayer;

    const leaflet_objects: ItemLeafletObjects = this.loaded_items[item_id];
    if (leaflet_objects) {
      targetLayerGroup.removeLayer(leaflet_objects.marker);
    }
  }

  /**
   * Hides the given item's track from the map.
   */
  public hideItemTrack(item_id: string, target_layer_title?: string): void {
    const targetLayerGroup: L.LayerGroup = target_layer_title
      ? allLayers[target_layer_title]
      : classicItemsLayer;

    const leaflet_objects: ItemLeafletObjects = this.loaded_items[item_id];
    if (leaflet_objects) {
      targetLayerGroup.removeLayer(leaflet_objects.line);
      for (const i in leaflet_objects.lineCaptions) {
        targetLayerGroup.removeLayer(leaflet_objects.lineCaptions[i]);
      }
    }
  }

  private _showItemMarkerOnMap(item_id: string, target_layer_title?: string) {
    const targetLayerGroup = target_layer_title
      ? allLayers[target_layer_title]
      : classicItemsLayer;

    if (this.loaded_items[item_id].marker) {
      this.loaded_items[item_id].marker.addTo(targetLayerGroup);
    }
  }

  private _showItemTrackOnMap(item_id: string, target_layer_title?: string) {
    const targetLayerGroup = target_layer_title
      ? allLayers[target_layer_title]
      : classicItemsLayer;

    const leaflet_objects: ItemLeafletObjects = this.loaded_items[item_id];
    if (leaflet_objects?.show_track) {
      leaflet_objects.line.addTo(targetLayerGroup);
      if (this.map_config.layers.positiontimestamps) {
        for (const i in leaflet_objects.lineCaptions) {
          leaflet_objects.lineCaptions[i].addTo(targetLayerGroup);
        }
      } else {
        for (const i in leaflet_objects.lineCaptions) {
          leaflet_objects.lineCaptions[i].removeFrom(targetLayerGroup);
        }
      }
    }
  }

  /**
   * Calculates the geographical distance between two points on the map, in meters.
   * @returns {number} The distance in meters (CI unit)
   */
  public getDistance(
    point1: { lat: number; lon: number },
    point2: { lat: number; lon: number }
  ): number {
    const latlng1 = L.latLng(point1.lat, point1.lon);
    const latlng2 = L.latLng(point2.lat, point2.lon);
    return latlng1.distanceTo(latlng2);
  }

  /**
   * Tell the map to hide or show the track for a given item.
   * @param item_id
   * @param visibility_value
   */
  public setItemTrackShowing(item_id: string, visibility_value: boolean) {
    if (this.loaded_items[item_id]) {
      this.loaded_items[item_id].show_track = visibility_value;
      if (visibility_value) {
        this._showItemTrackOnMap(
          item_id,
          this.loaded_items[item_id].showing_on_layer
        );
      } else {
        this.hideItemTrack(
          item_id,
          this.loaded_items[item_id].showing_on_layer
        );
      }
    }
  }

  /**
   * Ask the map if a given ite's track is currently being displayed.
   * @param item_id
   */
  public isItemTrackShowing(item_id: string): boolean {
    return this.loaded_items[item_id]?.show_track || false;
  }

  /**
   * Ask the map which layer a given item is being shown on.
   * This is useful for item popups to that users can be told which filter
   * they need to deactivate if they want to hide a given item.
   * @param item_id The id of the item in question.
   */
  public getShowingOnLayer(item_id: string): string | undefined {
    return this.loaded_items[item_id]?.showing_on_layer;
  }

  /**
   * This creates a map layer per FilteredItemSection, and fills the new layer with
   * markers and polylines each base_items. For each base_item, the positions_per_item
   * are used for placing the markers on the ap and for drawing any required tracks.
   * @param filtered_base_items An array of FilteredItemSections where each section
   *   contains the array of base_items (i.e. items without positions as in the database)
   *   that fit the current filter sections for that section.
   * @param positions_per_item A mapping from item ID to an array of database positions.
   */
  public setItemSections(
    filtered_base_items: FilteredItemSection[],
    positions_per_item: DbPositionsPerItem
  ): void {
    for (const filter_section of filtered_base_items) {
      // update items within this layer / section of filtered items;
      if (filter_section.filter_group_settings?.selectable_on_map) {
        const section_group: L.LayerGroup = allLayers[filter_section.title];
        section_group.clearLayers();
        for (const base_item of filter_section.base_items) {
          const item_positions = positions_per_item[base_item._id];
          const first_appearance_on_map = !this.loaded_items[base_item._id];
          this.updateItemOnMap(
            base_item,
            item_positions,
            true,
            filter_section.title
          );
          if (first_appearance_on_map) {
            this.setItemTrackShowing(
              base_item._id,
              this.initialTrackVisibility(base_item)
            );
          }
        }
      }
    }
  }

  /**
   * Heuristic to decide which item tracks should initially be visible on the map.
   * This may be overridden by user preferences in the future.
   * @param base_item The item to look into for deciding initial track visibility.
   */
  public initialTrackVisibility(base_item: DbItem): boolean {
    if (base_item.properties['status'])
      return base_item.properties['status'] != 'closed';
    else return false;
    // else return (
    //     base_item.properties['category'] == 'civilfleet' &&
    //     base_item.properties['active'] == 'true'
    //   );
  }
}

export default new MapWrapper();
