import storage from './storageWrapper';
import * as map_utils from './map/positionUtils';
import type { DbItem, NewDbItem } from '../types/db-item';
import type { DbPosition, NewPosition } from '../types/db-position';
import type { MapTracksConfig } from '@/types/config.js';
import type { ItemFilter } from '@/types/item-filter-and-sorting.js';
import type { LogEntry, LogEntryChange } from '@/types/db-log.js';

import { monotonicFactory } from 'ulid';
import { databasesApi } from './databasesApi';
import type { databaseNames } from '@/types/databases';
import { getDotenvVariable } from './dotenv';

import PouchDB from 'pouchdb';
import type { AllDocsResponse, Response } from '@types/pouchdb-core';
import find from 'pouchdb-find';
PouchDB.plugin(find);

import { useAuthStore } from '../stores/AuthStore';
import { useShowStore } from '../stores/ShowStore';

export class DbWrapper {
  public getDB(db_name: string, type = 'local'): PouchDB.Database {
    const database = databasesApi.getDatabase(db_name);
    if (!database?.[type]) {
      useAuthStore().logout();
      useShowStore().setLogin({
        show: true,
        errorMessage: 'Database not found.',
      });
      throw new Error(`No database with name ${db_name}`);
    } else {
      return database[type];
    }
  }

  /**
   * Get positions for a given DbItem, and sort from newest to oldest.
   * Use the DbItem's map_settings section to restrict the returned positions, if it exists.
   *
   * The first returned position (array index 0) is the newest position.
   * The last returned position (last index in array) is the oldest position.
   *
   * @param base_item The base item without positions. This was probably previously retrieved from the DB.
   * @param common_tracks_config The common tracks configuration. Usually found via the storageWrapper.
   * @returns A Promise to an array of DbPositions.
   * @memberof DbWrapper
   */
  public async getPositionsForDbItem(
    base_item: DbItem,
    common_tracks_config: MapTracksConfig
  ): Promise<DbPosition[]> {
    const item_tracks_config = map_utils.computeItemSpecificTracksConfig(
      base_item,
      common_tracks_config
    );

    let positions: DbPosition[] =
      (await this._getPositionsForItem(
        base_item._id,
        item_tracks_config.length_limit,
        item_tracks_config.newest_date_iso,
        item_tracks_config.oldest_date_iso
      )) ?? [];

    // try to get at least one position for this DbItem if none are in range:
    if (positions.filter((pos) => !(pos.soft_deleted ?? false)).length == 0) {
      const latestPosition = await this.findLatestPositionForItemId(
        base_item,
        positions
      );
      positions = latestPosition ? [latestPosition] : [];
    }

    // return either the item's positions within the given time range, or the single last known position for that item
    return positions;
  }

  private async findLatestPositionForItemId(
    base_item: DbItem,
    positions: DbPosition[]
  ): Promise<DbPosition | null> {
    // TODO: Use .find(..) instead of .allDocs(..), and find only with soft_deleted != true
    // TODO: only retrieve the newest position that isn't soft_deleted.
    const oldest_iso = new Date('2000-01-01').toISOString();
    const single_id_position: DbPosition[] = await this._getPositionsForItem(
      base_item._id,
      3,
      null,
      oldest_iso
    );
    positions = [...(single_id_position ?? [])].filter(
      (pos) => !(pos.soft_deleted ?? false)
    );
    return positions.length > 0 ? positions[0] : null;
  }

  /**
   * Load all positions for a given item, but in pages!
   * @param base_item The item to load positions for. This may be changed to just an itemId string in the future.
   * @param page Which page to load. Start at page 0.
   * @param pageLength How many positions should be contained in each page. Suggestion: Ask for e.g. 50 positions.
   * @returns An array of DbPositions with length between 0 and pageLength, depending on how many positions were found.
   */
  public async getAllPositionsPaged(
    base_item: DbItem,
    page: number,
    pageLength: number
  ): Promise<DbPosition[]> {
    const positions: DbPosition[] =
      (await this._getPositionsForItem(
        base_item._id,
        pageLength,
        null /* do not restrict newest date */,
        '1970-01-01T00:00:00.000Z' /* use oldest unix date */,
        page * pageLength
      )) ?? [];

    return positions;
  }

  /**
   * Get positions for a given item _id, and sort from newest to oldest (!).
   * The first returned position (array index 0) is the newest position.
   * The last returned position (last index in array) is the oldest position.
   *
   * @param _id The _id of the vessel that these positions are for.
   * @param count_limit Return at most this many positions. Cut away older positions if necessary.
   * @param {string | null} newest_date_iso The ISO string of the newest date
   * @param {string} oldest_date_iso The ISO string of the oldest date
   * @param {number} skip Offset of how many documents to skip from its normal starting point
   * @param {boolean} [ignoreSoftDeleted=false] Whether soft-deleted positions should be included
   * @returns
   * @memberof DbWrapper
   */
  private _getPositionsForItem(
    _id: string,
    count_limit = 9999,
    newest_date_iso: string | null,
    oldest_date_iso: string,
    skip: number = 0
  ): Promise<DbPosition[]> {
    return this.getDB('positions')
      .allDocs({
        include_docs: true,
        attachments: true,
        startkey: _id + '_' + (newest_date_iso || '') + '\ufff0', // start with the newest date, so that the newest position is always returned
        endkey: _id + '_' + oldest_date_iso + '\ufff0', // end with oldest date, which is ok to cut off if limit is reached
        limit: Math.min(9999, count_limit), // fetch at most the XX most recent positions per item
        skip: skip,
        descending: true, // return latest positions first, so that the limit only cuts off older positions
      })
      .then((result) => {
        return result.rows.map((position) => position.doc) as DbPosition[];
      })
      .catch((err) => {
        console.log(err);
        return [];
      });
  }

  /**
   * Creates all given positions in the database concurrently via Promises.
   * Use a ULID for time-sortable position IDs, and have it monotonically
   * increase if multiple positions exist for the same timestamp.
   * @param item_id The ID of the item that these positions are for.
   * @param positions An array of `NewPosition`s to be created.
   * @returns An array of promises that can be waited for via a `Promise.allSettled(..)` statement.
   */
  public createPositions(
    item_id: string,
    positions: NewPosition[]
  ): Promise<Response>[] {
    const ulid_mono = monotonicFactory();
    return positions.map((new_position: NewPosition) =>
      this.createPosition(
        item_id,
        ulid_mono(new Date(new_position.timestamp).getTime()),
        new_position
      )
    );
  }

  /**
   * Update (or soft-delete) all given positions in the database concurrently via Promises.
   * Every position is assumed to already exist in the database, and hence already have its `_id` field filled-in.
   * @param positions An array of `DbPosition`s to be updated.
   * @returns An array of promises that can be waited for via a `Promise.allSettled(..)` statement.
   */
  public updatePositions(positions: DbPosition[]): Promise<Response>[] {
    return positions.map((current_pos: DbPosition) =>
      this.updatePosition(current_pos)
    );
  }

  /**
   * Creates a new position from a `NewPosition` object and fills-in its `_id` and `item_id` field using the provided data.
   * @param item_id The ID of the item that this position is for.
   * @param ulid_string The unique ID that is also lexigraphically sortable by time.
   * @param new_position The new position that is to be created in the database.
   * @returns A promise to the database operation.
   */
  public createPosition(
    item_id: string,
    ulid_string: string,
    new_position: NewPosition
  ): Promise<Response> {
    if (new_position['_id']) {
      throw new Error(
        'The "new" position already has an _id assigned to it!' +
          `Maybe it is not that new after all? id=${new_position['_id']}`
      );
    }
    if (!map_utils.positionIsValid(new_position)) {
      throw new Error('New Position is invalid!');
    }

    const db_position: DbPosition = {
      ...new_position,
      ...{
        // TODO: remove the redundant middle part in a future MR:
        _id: item_id + '_' + new_position.timestamp + '_' + ulid_string,
        item_id: item_id,
        source: new_position.source || 'onefleet',
        time_created: new Date().toISOString(),
      },
    };
    return this.updatePosition(db_position);
  }

  /**
   * Updates an existing position in the database and adds/updates the position's `time_modified` field.
   * @param db_position A `DbPosition` that already includes the correct _id field.
   * @returns A promise to the database operation.
   */
  public updatePosition(db_position: DbPosition): Promise<Response> {
    if (!map_utils.positionIsValid(db_position)) {
      throw new Error(`Position ${db_position['_id']} is invalid!`);
    }
    db_position.time_modified = new Date();
    return this.getDB('positions').put(db_position);
  }

  /**
   * Adds a change entry for an item to the *log* DB
   * @param item_id ID of the item to add log entry for
   * @param change The change object
   * @param comment Optional comment by user regarding change
   */
  public addItemLog(
    item_id: string,
    change: LogEntryChange,
    comment: string
  ): void {
    const logDB = this.getDB('logs');
    const log_entry: LogEntry = {
      _id: item_id + '_' + new Date().toISOString(),
      user: localStorage.username,
      change: change,
      comment: comment,
    };
    logDB
      .put(log_entry)
      .then(() => {})
      .catch(() => {});
  }

  /**
   * Creates an item in the database and sets its `time_created` + `prefix_created` fields.
   * PouchDB does not differentiate between update and create operations,
   * therefore *updateItem* is called internally
   *
   * @param freshDbItem DbItem to update
   * @returns Promise to DB Response or an Error
   */
  public async createItem(
    freshDbItem: NewDbItem,
    newItemId: string
  ): Promise<Response | PouchDB.Core.Error> {
    //PouchDB does not differentiate between update and create operations.
    //Therefore we set time_created here and proceed with an update
    const obj: Partial<DbItem> = freshDbItem;
    obj._id = newItemId;
    obj.time_created = new Date().toISOString();
    obj.prefix_created = getDotenvVariable('VITE_DB_PREFIX');
    return await this.updateItem(obj as DbItem);
  }

  /**
   * Updates an item in the database and it's `time_modified` + `prefix_modified` fields.
   *
   * @param obj DbItem to update
   * @returns Promise to DB Response or an Error
   */
  public async updateItem(obj: DbItem): Promise<Response | PouchDB.Core.Error> {
    const itemDB = this.getDB('items');
    obj.time_modified = new Date().toISOString();
    obj.prefix_modified = getDotenvVariable('VITE_DB_PREFIX');
    return itemDB.put(obj);
  }

  /**
   * Get a DbItem and its positions from the database, as a Promise
   *
   * @param itemId The database ID of the item to retrieve
   * @returns A Promise to a 2-element tuple of the found DbItem and its array of DbPositions
   * @memberof DbWrapper
   */
  public async getItemAndPositions(
    itemId: string
  ): Promise<[DbItem | null, DbPosition[]]> {
    if (itemId === '') {
      return [null, []];
    }

    try {
      const base_item: DbItem = await this.getItem(itemId);
      const db_positions: DbPosition[] = await this.getPositionsForDbItem(
        base_item,
        storage.get_map_tracks_config()
      );
      return [base_item, db_positions];
    } catch (err) {
      console.error(err);
      return [null, []];
    }
  }

  public async getItem(itemId: string): Promise<DbItem> {
    const base_item: DbItem = await this.getDB('items').get(itemId);
    if (base_item) {
      // There are still items in the DB that have no map_settings section.
      // If so, create it here ad-hoc from properties:
      if (!base_item.map_settings) {
        base_item.map_settings =
          map_utils.convertTrackingPropertiesToMapSettings(
            base_item,
            base_item.template
          );
      }
      return base_item;
    } else {
      throw new Error(`The item '${itemId}' could not be found.`);
    }
  }

  /**
   * Get items using Mango Query.
   * Used to get all items that match the initially active filter set.
   *
   * @param filters - Set of currently active filters
   * @returns Promise of the items matching the filters
   */
  public getFilteredItems(filters: ItemFilter[]): Promise<void | DbItem[]> {
    const clauses = {};

    filters.forEach((filter) => {
      clauses[filter.field] = {
        $in: filter.values,
      };
    });

    const query: FindRequest<{}> = {
      selector: {
        $and: [clauses],
      },
    };
    return this.getDB('items')
      .find(query)
      .then((result: FindResponse<{}>) => result.docs as DbItem[])
      .catch((err) => {
        console.log(`Error while retrieving filtered items: ${err}`);
      });
  }

  /**
   * Get items using Mango Query.
   * Used to get all items that match the given item IDs.
   *
   * @param ids - Array of ids
   * @returns Promise of the items matching the filters
   */
  public async getItemsById(ids: string[]): Promise<void | DbItem[]> {
    return (await this.getItemsOrPositionsById(ids, 'items')) as DbItem[];
  }

  /**
   * Get positions using Mango Query.
   * Used to get all positions that match the given item IDs.
   *
   * @param ids - Array of ids
   * @returns Promise of the positions matching the filters
   */
  public async getPositionsById(ids: string[]): Promise<void | DbPosition[]> {
    return (await this.getItemsOrPositionsById(
      ids,
      'positions'
    )) as DbPosition[];
  }

  /**
   * Get items or positions using Mango Query.
   * Please use getItemsById() and getPositionsById()
   *
   * @param ids - Array of ids
   * @param databaseName - Name of database to query from
   * @returns Promise of the items or positions matching the filters
   */
  public getItemsOrPositionsById(
    ids: string[],
    databaseName: databaseNames
  ): Promise<void | DbItem[] | DbPosition[]> {
    const idsStringified = JSON.parse(JSON.stringify(ids));
    const query: FindRequest<{}> = {
      selector: {
        _id: {
          $in: idsStringified,
        },
      },
    };
    return this.getDB(databaseName)
      .find(query)
      .then(
        (result: FindResponse<{}>) => result.docs as DbItem[] | DbPosition[]
      )
      .catch((err) => {
        console.log(`Error while retrieving filtered ${databaseName}: ${err}`);
      });
  }

  /**
   * @returns Promise of all entries in *items* DB
   */
  public async getBaseItems(): Promise<DbItem[]> {
    return this.getDB('items')
      .allDocs({
        include_docs: true,
        attachments: true,
      })
      .then((result: AllDocsResponse<{}>) => {
        return result.rows.map((item) => item.doc) as DbItem[];
      })
      .catch((error) => {
        console.log(error);
        return [];
      });
  }

  /**
   * @returns Promise of all entries in *logs* DB
   */
  public async getLog(): Promise<LogEntry[]> {
    return this.getDB('logs')
      .allDocs({
        include_docs: true,
        attachments: true,
      })
      .then((result: AllDocsResponse<{}>) => {
        return result.rows.map((entry) => entry.doc) as LogEntry[];
      })
      .catch((error) => {
        console.log(error);
        return [];
      });
  }
}
