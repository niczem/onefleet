import type { UrlArgs } from '@/utils/url';
import { generateUrl } from '@/utils/url';

/**
 * Log into an existing CouchDB.
 *
 * @param args - The login arguments.
 * @returns The CouchDB POST _session response status code.
 *
 * @throws {@link Error}
 * Thrown if the login fails.
 */
export async function logIntoCouchDb({
  protocol,
  host,
  port,
  username,
  password,
}: UrlArgs & { username: string; password: string }): Promise<number> {
  const couchDbUrl = generateUrl({ protocol, host, port });
  try {
    // Request for a new cookie from CouchDB _session
    const response = await fetch(`${couchDbUrl}/_session`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      credentials: 'include',
      body: JSON.stringify({
        name: username,
        password: password,
      }),
    });
    return response.status;
  } catch (error) {
    throw new Error('Logging into remote CouchDB failed.');
  }
}

/**
 * Logs back in to the existing couchDB session
 *
 * @param args - The login arguments.
 * @returns The CouchDB GET _session response status code.
 */
export async function checkExistingCouchDbLogin({
  protocol,
  host,
  port,
}: UrlArgs): Promise<number> {
  const couchDbUrl = generateUrl({ protocol, host, port });
  try {
    const response = await fetch(`${couchDbUrl}/_session`, {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
      credentials: 'include',
    });
    return response.status;
  } catch (error) {
    throw new Error('Logging into remote CouchDB failed.');
  }
}

/**
 * Logout from existing CouchDB.
 * Instructs the browser to clear the cookie.
 *
 * @param args - The logout arguments.
 */
export async function logoutFromCouchDb({
  protocol,
  host,
  port,
}: UrlArgs): Promise<void> {
  const couchDbUrl = generateUrl({ protocol, host, port });
  try {
    // Closes user’s session by instructing the browser to clear the cookie.
    await fetch(`${couchDbUrl}/_session`, {
      method: 'DELETE',
    });
  } catch (err) {
    throw new Error('Logging out from remote CouchDB failed.');
  }
}
