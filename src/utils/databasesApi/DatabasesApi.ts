import { getDotenvVariable } from '../dotenv';
import { minutesToMilliseconds, secondsToMilliseconds } from '../units';
import {
  checkExistingCouchDbLogin,
  logIntoCouchDb,
  logoutFromCouchDb,
} from './authentication';
import { DatabaseApi } from './DatabaseApi';

import { useDataStore } from '../../stores/DataStore';
import { useAuthStore } from '@/stores/AuthStore';

type KnownDatabaseNames = 'items' | 'positions' | 'logs';

/**
 * API for the application's PouchDB and CouchDB databases.
 */
export class DatabasesApi {
  /**
   * The remote CouchDB URL protocol.
   */
  private protocol: string;

  /**
   * The remote CouchDB URL host.
   */
  private host: string;

  /**
   * The remote CouchDB URL protocol.
   */
  private port: number;

  /**
   * The database prefix.
   */
  private databasePrefix: string;

  /**
   * The list of initialized databases.
   */
  private databases: Map<string, DatabaseApi> = new Map();

  /**
   * The id of the refresh cookie interval function.
   */
  private refreshCookieIntervalId: number | undefined;

  /**
   * Create a new Databases API instance.
   *
   * @param args - The constructor arguments.
   */
  constructor({
    protocol,
    host,
    port,
    databasePrefix,
  }: {
    protocol: string;
    host: string;
    port: number;
    databasePrefix: string;
  }) {
    this.protocol = protocol;
    this.host = host;
    this.port = port;
    this.databasePrefix = databasePrefix;
  }

  /**
   * Log into the remote CouchDB and start periodically refreshing the session
   * cookie.
   *
   * @param args - The login credentials.
   * @returns The CouchDB POST _session response status code.
   */
  public async login({
    username,
    password,
  }: {
    username: string;
    password: string;
  }): Promise<number> {
    const login = () =>
      logIntoCouchDb({
        username,
        password,
        protocol: this.protocol,
        host: this.host,
        port: this.port,
      });
    const responseStatusCode = await login();
    if (
      this.refreshCookieIntervalId === undefined &&
      responseStatusCode === 200
    ) {
      const dbCookieValidationTimeout = minutesToMilliseconds(
        getDotenvVariable('VITE_DB_COOKIE_VALIDATION_TIMEOUT', 'number')
      );
      const refreshCookieOffset = secondsToMilliseconds(30);
      this.refreshCookieIntervalId = window.setInterval(
        login,
        // get the CouchDB cookie validation timeout from .env and subtract a
        // sensible offset to refresh the session in time
        dbCookieValidationTimeout - refreshCookieOffset
      );
    }
    return responseStatusCode;
  }

  /**
   * Log out from remote CouchDB.
   *
   */
  public async logout(): Promise<void> {
    this.databases.forEach((database) => {
      database.stopSynchronization();
    });
    window.clearInterval(this.refreshCookieIntervalId);
    await logoutFromCouchDb({
      protocol: this.protocol,
      host: this.host,
      port: this.port,
    });
  }

  /**
   * Tries to login to couchDB using an existing cookie.
   * cookie.
   *
   * @returns The CouchDB GET _session response status code.
   */
  public async checkExistingCookie(): Promise<number> {
    const checkCookie = () =>
      checkExistingCouchDbLogin({
        protocol: this.protocol,
        host: this.host,
        port: this.port,
      });
    const responseStatusCode = await checkCookie();
    return responseStatusCode;
  }

  /**
   * Initialize the databases (local and remote).
   *
   * @param databaseNames - The names of the databases to initialize.
   */
  public intializeDatabases(databaseNames: string[]): void {
    databaseNames.forEach((databaseName) => {
      if (this.databases.has(databaseName)) {
        throw new Error(
          `Database ${databaseName} has already been initialized`
        );
      } else {
        const database = DatabaseApi.create({
          databaseName,
          protocol: this.protocol,
          host: this.host,
          port: this.port,
          databasePrefix: this.databasePrefix,
        });
        this.databases.set(databaseName, database);
        this.initializeDatabase(databaseName);
      }
    });
  }

  private async initializeDatabase(
    databaseName: KnownDatabaseNames
  ): Promise<void> {
    const databaseInstance = this.getDatabase(databaseName);

    switch (databaseName) {
      case 'items':
        useDataStore().addEventListenersToItemsDb(databaseInstance);
        break;
      case 'positions':
        useDataStore().addEventListenersToPositionsDb(databaseInstance);
        break;
      case 'logs':
        // no listeners to set up, but the database is known.
        break;
      default:
        throw new Error('Unknown database name. Implementation error?');
    }
    // Start Synchronization of the database.
    return await databaseInstance!.startSynchronization();
  }

  /**
   * Get a database by it's name.
   *
   * @param name - The database name.
   * @returns The database.
   */
  public getDatabase(name: string): DatabaseApi | undefined {
    return this.databases.get(name);
  }
}
