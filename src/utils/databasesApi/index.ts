import { getDotenvVariable } from '../dotenv';
import { DatabasesApi } from './DatabasesApi';

/**
 * The global PouchDB API instance.
 */
export const databasesApi = new DatabasesApi({
  host: getDotenvVariable(
    'VITE_DB_REMOTE_HOST',
    'string',
    window.location.hostname
  ).replace('map', 'db'), //hotfix for issue 406 (https://gitlab.com/civilmrcc/onefleet/-/issues/406)
  protocol: getDotenvVariable(
    'VITE_DB_REMOTE_PROTOCOL',
    'string',
    window.location.protocol
  ),
  databasePrefix: getDotenvVariable('VITE_DB_PREFIX'),
  port: getDotenvVariable('VITE_DB_REMOTE_PORT', 'number', 5984),
});
