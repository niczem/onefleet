import type { MapLayersConfig, MapTracksConfig } from '@/types/config';

/**
 * Storage wrapper singleton class.
 */
class StorageWrapper {
  /**
   * Store an item in localStorage.
   * @param i - The storage item's key.
   * @param v - The value to store.
   */
  public set(i: string, v: string): void {
    return localStorage.setItem(i, v);
  }

  /**
   * Retrieve an item from localStorage.
   * @param i - The storage item's key.
   * @returns The storage item's value.
   */
  public get(i: string): string | null {
    return localStorage.getItem(i);
  }

  /**
   * Gets the Tracks configuration from the storage.
   * See also mapUtils.computeItemSpecificTracksConfig()
   */
  public get_map_tracks_config(): MapTracksConfig {
    try {
      return JSON.parse(this.get('map_tracks') ?? '');
    } catch (err) {
      // default tracks config:
      return {
        type: 'number_of_days',
        oldest_date_iso: new Date('2022-07-01').toISOString(),
        newest_date_iso: null,
        length_limit: 10000, // see also computeItemSpecificTracksConfig()
        show_past_days: 1, // may be overwritten by item's map_tracks_config
      };
    }
  }

  public get_map_layers_config(): MapLayersConfig {
    try {
      return JSON.parse(this.get('map_layers') ?? '');
    } catch (err) {
      // default layers config:
      return {
        background_tiles: 'greyscale',
        openseamap: false,
        showcaptions: true,
        positiontimestamps: false,
      };
    }
  }
}

export default new StorageWrapper();
