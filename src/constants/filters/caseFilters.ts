import type { ItemFilterGroup } from '@/types/item-filter-and-sorting';

export const caseFilters: ItemFilterGroup = {
  title: 'Cases',
  selectable_in_sidebar: true,
  selectable_on_map: true,
  initially_selected_on_map: true,
  filters: [
    {
      name: 'Filtergroup Template',
      field: 'template',
      values: ['case'],
      always_active: true,
    },
    {
      name: 'Only Open Cases',
      field: 'properties.status',
      values: [
        'open_confirmed',
        'open_lost',
        'open_unknown',
        'open_known',
        'approaching',
        'disembarked',
        'attending',
      ],
      initially_active: true,
    },
    {
      name: 'Only Closed Cases',
      field: 'properties.status',
      values: ['closed'],
    },
    {
      name: 'Only Ready for Documentation',
      field: 'properties.status',
      values: ['ready_for_documentation'],
    },
    {
      name: 'Only SAR1 Region',
      field: 'properties.sar_region',
      values: ['sar1'],
    },
    {
      name: 'Only SAR2 Region',
      field: 'properties.sar_region',
      values: ['sar2'],
    },
    {
      name: 'Only SAR3 Region',
      field: 'properties.sar_region',
      values: ['sar3'],
    },
    {
      name: 'Only Show Trash',
      field: 'soft_deleted',
      values: ['true'],
    },
    {
      name: 'Hide Trash',
      field: 'soft_deleted',
      values: ['false', 'undefined'],
      initially_active: true,
    },
    // Not yet implemented:
    // {
    //   name: 'First Seen in last 7 days',
    //   field: 'properties.first_seen',
    //   values: ['$gte(now - 7)'],
    // },
    // {
    //   name: 'Last Position < 2 days ago',
    //   field: 'position.timestamp',
    //   values: ['$gte(now - 2)'],
    // },
  ],
  sortings: [],
};
