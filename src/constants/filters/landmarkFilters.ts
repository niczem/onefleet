import type { ItemFilterGroup } from '@/types/item-filter-and-sorting';

export const landmarkFilters: ItemFilterGroup = {
  title: 'Landmarks',
  selectable_in_sidebar: true,
  selectable_on_map: true,
  initially_selected_on_map: true,
  filters: [
    {
      name: 'Filtergroup Templates',
      field: 'template',
      values: ['landmark'],
      always_active: true,
    },
    {
      name: 'Hide Oil Fields',
      field: 'properties.category',
      values: ['!oil_field'],
    },
    {
      name: 'Hide Sand Banks',
      field: 'properties.category',
      values: ['!sand_bank'],
    },
    {
      name: 'Hide Islands',
      field: 'properties.category',
      values: ['!island'],
      initially_active: true,
    },
    {
      name: 'Hide Beaches',
      field: 'properties.category',
      values: ['!beach'],
    },
    {
      name: 'Hide Cities',
      field: 'properties.category',
      values: ['!city'],
    },
    {
      name: 'Only Show Trash',
      field: 'soft_deleted',
      values: ['true'],
    },
    {
      name: 'Hide Trash',
      field: 'soft_deleted',
      values: ['false', 'undefined'],
      initially_active: true,
    },
  ],
  sortings: [],
};
