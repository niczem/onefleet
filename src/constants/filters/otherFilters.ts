import type { ItemFilterGroup } from '@/types/item-filter-and-sorting';

export const otherFilters: ItemFilterGroup = {
  title: 'Other Ships & Aircraft',
  selectable_in_sidebar: true,
  selectable_on_map: true,
  initially_selected_on_map: true,
  filters: [
    {
      name: 'Filtergroup Templates',
      field: 'template',
      values: ['ship', 'vehicle', 'aircraft'],
      always_active: true,
    },
    {
      name: 'Filtergroup Category: Other (Not Civilfleet, Commercial Or Military)',
      field: 'properties.category',
      values: ['import', 'unknown', 'other'],
      always_active: true,
    },
    {
      name: 'Active Vehicles',
      field: 'properties.active',
      values: ['true'],
      initially_active: true,
    },
    {
      name: 'Only Aircraft',
      field: 'template',
      values: ['aircraft'],
    },
    {
      name: 'Only Ships',
      field: 'template',
      values: ['ship', 'vehicle'],
    },
    {
      name: 'Only Show Trash',
      field: 'soft_deleted',
      values: ['true'],
    },
    {
      name: 'Hide Trash',
      field: 'soft_deleted',
      values: ['false', 'undefined'],
      initially_active: true,
    },
  ],
  sortings: [],
};
