import filters from './index';
import type {
  ItemTemplate,
  ItemTemplateField,
} from '@/types/item-template-schema';
import type {
  ItemFilterGroup,
  ItemFilterSpecification,
} from '@/types/item-filter-and-sorting';
import templates from '../templates';

describe('Filtergroups should use only existing select field options', () => {
  const all_filters: ItemFilterGroup[] = filters.get_filter_groups;

  for (const filterGroup of all_filters) {
    describe(`Checking FilterGroup '${filterGroup.title}'`, () => {
      it('Has a descriptive title', () => {
        expect(filterGroup.title.length >= 3).toBe(true);
      });

      it('Consists of a list of filters', () => {
        expect(
          filterGroup.filters.length >= 1 &&
            filterGroup.filters.every(
              (filter: ItemFilterSpecification) => typeof filter === 'object'
            )
        ).toBe(true);
      });

      /* Gather template names defining the filtergroup (i.e. must be always_active): */
      const namedFilterGroupTemplateKeys: string[] = filterGroup.filters
        .filter(
          (filter: ItemFilterSpecification) =>
            !!filter.always_active && filter.field == 'template'
        )
        .reduce((templateKeys: string[], filter: ItemFilterSpecification) => {
          return [...filter.values, ...templateKeys];
        }, []);
      /* Assume all templates have to match if no template filter was specified: */
      const actualTemplateKeys: typeof templates.keys =
        namedFilterGroupTemplateKeys.length == 0
          ? templates.keys
          : templates.keys.filter((key) =>
              namedFilterGroupTemplateKeys.includes(key)
            );

      for (const filter of filterGroup.filters) {
        describe(`Filter '${filter.name}' (field '${filter.field}')`, () => {
          it('Has one or more values', () => {
            expect(filter.values.length).toBeGreaterThan(0);
          });
          const filterFieldValues = filter.values;
          const filterFieldPath = filter.field.split('.');

          if (filterFieldPath[0] == 'properties') {
            const propertyName = filterFieldPath[1];

            /*
             * Reduce the FilterGroup's templates down to an array of valid select options.
             *
             * This big statement needs some explanation:
             * 1. map templateKey -> template
             *    This maps each templateKey to its respective template
             * 2. map template -> field with same propertyName as in filter
             *    Now we have an array of ItemTemplateFields that each have
             *    the same name but are in different templates if this
             *    FilterGroup uses more than one template.
             * 3. filter template fields -> keep only those that have type == 'select'
             *    This test is really only interested in 'select' fields, as e.g.
             *    'text' fields can have arbitrary values anyway.
             * 4. map template fields -> options
             *    Assuming that we still have some template fields that have passed
             *    the filter, lets gather up any option keys they have. The result is
             *    an array of string-arrays.
             * 5. reduce to array of string-arrays into a single array of strings.
             *    The final array of field select-options is now ready for the subset
             *    comparison below.
             */
            const allFieldSelectOptions = actualTemplateKeys
              .map((templateKey) => templates.get(templateKey))
              .map((template: ItemTemplate) =>
                template.fields.find((tf) => tf.name == propertyName)
              )
              .filter(
                (templateField?: ItemTemplateField) =>
                  templateField && templateField.type == 'select'
              )
              .map((templateField?: ItemTemplateField) =>
                Object.keys(templateField?.options ?? {})
              )
              .reduce((array: string[], selectOptions: string[]) => {
                return [...array, ...selectOptions];
              }, []);

            const pluralOrNot = actualTemplateKeys.length > 1 ? 's' : '';
            it(
              `Filter values [${filterFieldValues}] are a subset of all ` +
                `select options (property '${propertyName}' in ` +
                `template${pluralOrNot} '${actualTemplateKeys}').`,
              () => {
                /*
                 * Check if the filter's values are a subset of all possible
                 * select-options that exist in this field in any named template.
                 */
                const isSubset = filterFieldValues.every((val) =>
                  val.startsWith('!')
                    ? allFieldSelectOptions.includes(val.slice(1))
                    : allFieldSelectOptions.includes(val)
                );
                expect(isSubset).toBeTruthy();
              }
            );
          }
        });
      }
    });
  }
});
