import type { ItemTemplate } from '@/types/item-template-schema';

export const landmark: ItemTemplate = {
  plural: 'Landmarks',
  pouch_identifier: 'LANDMARK',
  enforce_initial_position: true,
  default_tracking_type: 'MANUAL',
  allow_creation_via_ui: true,
  type: 'line',
  default_marker_icon: 'landmark',
  always_show_markers: true,
  fields: [
    {
      name: 'name',
      title: 'Name',
      info: 'Name',
      field_group: 'Important',
      type: 'text',
    },
    {
      name: 'category',
      title: 'Landmark Category',
      info: 'Landmark Category',
      type: 'select',
      options: {
        oil_field: 'Oil Field / Wind Farm',
        sand_bank: 'Sand Bank',
        island: 'Island Centre',
        beach: 'Beach',
        city: 'City/Port',
      },
    },
    {
      name: 'comment',
      title: 'Comments',
      info: 'Comments',
      type: 'text',
    },
  ],
};
