import type { ItemTemplate } from '@/types/item-template-schema';

export const droppoint: ItemTemplate = {
  plural: 'Droppoint',
  pouch_identifier: 'DROPPOINT',
  enforce_initial_position: true,
  default_tracking_type: 'IRIDIUMGO',
  allow_creation_via_ui: false,
  type: 'point',
  default_marker_icon: 'droppoint',
  always_show_markers: false,
  fields: [
    {
      name: 'name',
      title: 'title',
      info: 'title',
      field_group: 'Important',
      type: 'text',
    },
    {
      name: 'created_by_vehicle',
      title: 'Created by vehicle',
      info: 'Created by vehicle',
      type: 'text',
    },
    {
      name: 'comment',
      title: 'comment',
      info: 'comment',
      type: 'text',
    },
  ],
};
