import type { CalcTemplate } from '@/types/calc-template-schema';

export const alarmPhoneTemplate: CalcTemplate = {
  name: 'AP',
  version: '0.1',
  defaultStatus: 'ready_for_documentation',
  templateInfoSheetname: 'Template',
  dataSheetName: 'Data',
  typeOfItemToParse: 'case',
  casesRowStart: 3,
  letterColumnEnd: 'Z',
  deprecated: false,
  CalcFields: [
    {
      cellName: ['AP NR'],
      columnLetter: ['B'],
      caseTemplateFieldEquivalentName: 'name',
      requiredForCreation: true,
    },
    {
      cellName: ['BOAT TYPE'],
      columnLetter: ['E'],
      caseTemplateFieldEquivalentName: 'boat_type',
      defaultValue: 'unknown',
    },
    {
      cellName: ['BOAT COLOR'],
      columnLetter: ['F'],
      caseTemplateFieldEquivalentName: 'boat_color',
      defaultValue: 'unknown',
    },
    {
      cellName: ['THURAYA'],
      columnLetter: ['C'],
      caseTemplateFieldEquivalentName: 'phonenumber',
    },
    {
      cellName: ['Place . DEP'],
      columnLetter: ['I'],
      caseTemplateFieldEquivalentName: 'place_of_departure',
      fallbackCells: [
        {
          fallbackCellName: ['Country. DEP'],
          column: ['H'],
        },
      ],
    },
    {
      cellName: [],
      columnLetter: ['L'],
      caseTemplateFieldEquivalentName: 'sar_region',
      defaultValue: 'unknown',
    },
    {
      cellName: ['Date. DEP', 'time. DEP'],
      columnLetter: ['J', 'K'],
      caseTemplateFieldEquivalentName: 'occurred_at',
      fallbackCells: [
        {
          fallbackCellName: ['DATE (rescued, arrived case closed)'],
          column: ['W'],
        },
      ],
      requiredForCreation: true,
    },
    {
      cellName: ['Date. DEP', 'time. DEP'],
      columnLetter: ['J', 'K'],
      caseTemplateFieldEquivalentName: 'time_of_departure',
    },
    {
      cellName: ['POB Number'],
      columnLetter: ['G'],
      caseTemplateFieldEquivalentName: 'pob_total',
    },
    {
      cellName: ['missing ppl', 'DEAD'],
      columnLetter: ['O', 'P'],
      caseTemplateFieldEquivalentName: 'people_dead',
    },
    {
      cellName: ['Source'],
      columnLetter: ['Z'],
      caseTemplateFieldEquivalentName: 'url',
      regex: /\bhttps?:\/\/\S+\b/g,
    },
    {
      cellName: ['outcome'],
      columnLetter: ['R'],
      caseTemplateFieldEquivalentName: 'outcome',
      defaultValue: 'unknown',
    },
  ],
  CalcPositions: [
    {
      format: 'REGEX',
      positionCellName: '1st position',
      positionCellColumn: 'M',
      regex:
        /([-+]?\d{1,2}\.\d+)[N]?,\s*([-+]?\d{1,3}\.\d+)[E]?\s*@\s*(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2})/,
    },
    {
      format: 'REGEX',
      positionCellName: 'Last position',
      positionCellColumn: 'S',
      regex:
        /([-+]?\d{1,2}\.\d+)[N]?,\s*([-+]?\d{1,3}\.\d+)[E]?\s*@\s*(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2})/,
    },
  ],
};
