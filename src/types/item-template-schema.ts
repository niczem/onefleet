import type { DbItemProperty, TrackingTypeOptions } from './db-item';
import type { TagSettings } from './tags';

export interface ItemTemplate {
  /** remove? */
  _id?: string;

  /** What to call plurals of this template in the UI */
  plural: string;

  /** Which prefix for the itemId. Has become a lot less important since we use `ulid` and `couchdb-find`. */
  pouch_identifier: string;

  /** whether to enforce that an initial position must be entered when creating a new item with this template. */
  enforce_initial_position: boolean;

  /** whether new items of this template should start with a given tracking type pre-selected. */
  default_tracking_type: TrackingTypeOptions;

  /** whether this template may be created via the UI or if it can only be added via background services. */
  allow_creation_via_ui: boolean;

  /** remove this? Can be taken from positions/timestamps: multiple positions with equal timestamps are an area, else a track. */
  type: 'line' | 'area' | 'point';

  /** the default marker for this type of item */
  default_marker_icon: string;

  /** always show markers for this template, even if their positions are outside the selected date range */
  always_show_markers: boolean;

  /** All the actual field definitions! The ordering here is reflected in the UI, grouped by `field_group`s. */
  fields: Array<ItemTemplateField>;
}

export interface ItemTemplateField {
  /** This is the internal name. Please use lowercase and no spaces. */
  name: string;

  /** The Title is displayed in the UI as field label */
  title: string;

  /** The type defines which UI widgets are used to display and edit this field. */
  type: ItemTemplateFieldType;

  /** A mapping of internal value to UI title for each option */
  options?: Record<string, string>;

  /** A specification of the possible options for a tag */
  tag_settings?: TagSettings;

  /** This fields default or starting value */
  default_value?: DbItemProperty;

  /** The minimum possible value in case of number field type */
  minimum_value?: number;

  /** The maximum possible value in case of number field type */
  maximum_value?: number;

  /** Whether the field should be (temporarily?) hidden from the UI. */
  hidden?: boolean;

  /** The tooltip text that gives users a bit more information on what to put into this field.*/
  info?: string;

  /** Which icon to display next to the field (not yet implemented) */
  field_icon?: string;

  /** Fields can be visually grouped in the UI for semantic reasons. */
  field_group?: string;

  /** For fields with type: number, this defines the step on each increase (default: 1) */
  step?: number;

  /** Whether this information is required for this item to be created (default: false) */
  required_for_creation?: boolean;

  /** Whether this information is required for this item to be exported (default: false) */
  required_for_export?: boolean;
}

/**
 * Field types define which UI widgets are used to display and edit each field.
 * They also imply which data type to use to store the field's data. Use string if unsure.
 */
export type ItemTemplateFieldType =
  | 'number'
  | 'inputtable'
  | 'text'
  | 'textarea'
  | 'color'
  | 'select'
  | 'togglebuttons'
  | 'boolean'
  | 'tag'
  | 'icon'
  | 'datetime'
  | 'datetimemulti'
  | 'checkbox';
