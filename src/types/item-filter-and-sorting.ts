import type { DbItem } from './db-item';

/**
 * Defines a group of Filters for filtering items in the LeftNavigation sidebar and on the map.
 */
export interface ItemFilterGroup {
  /** This title will appear at the top of the LeftNavigation as well as a "Database Item" in the right-side LayerControl. */
  title: string;

  /** Whether this ItemFilterGroup is even selectable in the sidebar. */
  selectable_in_sidebar: boolean;

  /** Whether this ItemFilterGroup is even selectable on the map. */
  selectable_on_map: boolean;

  /** Only one ItemFilterGroup can be initially selected in the sidebar (default: false) */
  initially_selected_in_sidebar?: boolean;

  /** Most ItemFilterGroups should be initially selected (visible) on the map (default: true) */
  initially_selected_on_map?: boolean;

  /** The array of ItemFilters that defines this ItemFilterGroup. */
  filters: Array<ItemFilter>;

  sortings: Array<any>;
}

/**
 * A single filter that helps define an ItemFilterGroup.
 * In order to be shown in the LeftNavigation, a DbItem needs to match all active filters.
 */
export interface ItemFilterSpecification {
  /** The name that is shown in the "Filter:" dropdown in the LeftNavigation sidebar. */
  name: string;

  /** Which field of the DbItem this filter checks. Separate sub-fields by a dot (.) */
  field: string;

  /** An array of allowed values for this filter to allow a given DbItem */
  values: Array<string>;

  /** Whether this ItemFilter should initially be active when the app first loads. (default: false) */
  initially_active?: boolean;

  /** ItemFilters that are always_active cannot be toggled off. They define the ItemFilterGroup. (default: false) */
  always_active?: boolean;
}

/**
 * When in active use, store this additional information in a filter.
 */
export interface ItemFilter extends ItemFilterSpecification {
  /** Whether this ItemFilter is currently toggled "on" in the UI. */
  active?: boolean;
}

/**
 * After filtering, the DbItems will be grouped by the ItemFilterGroup they were matched by.
 * This interface is used to keep track of those already-filtered DbItems.
 */
export interface FilteredItemSection {
  title: string;
  base_items: Array<DbItem>;
  hidden_items: number;
  filter_group_settings?: ItemFilterGroup;
}

/**
 * Sort option for items in LeftNavigation.
 */
export interface ItemSorting {
  /**
   * Human readable description.
   */
  label: string;
  /**
   * Sorting Order. True = Ascending, False = Descending
   */
  isSortedAscending: boolean;
  /**
   * Property by which item is sorted.
   */
  sortByProperty: keyof DbItem;
}
