export interface TagSettings {
  /**
   * Whether each tag should have a key:value string or just the "key" part. (Default: false)
   */
  keyOnly?: boolean;
  /**
   * May be empty
   */
  fixedKeyOptions: Record<string, string>;
  /**
   * Whether users may type additional tags / keys (default: false)
   */
  allowOpenOptions: boolean;
  /**
   * Whether form validation should complain if keys are not unique. (Default: true)
   */
  forceUniqueKeys?: boolean;
  /**
   * Label for the key to enter
   */
  labelKey: string;
  /**
   * Label for the value to enter
   */
  labelValue?: string;
}
