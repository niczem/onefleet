export interface MapConfig {
  tracks: MapTracksConfig;
  layers: MapLayersConfig;
  view?: MapViewConfig;
}

export interface MapTracksConfig {
  type: 'number_of_days' | 'date_range'; // days before now or date range
  oldest_date_iso: string;
  newest_date_iso: string | null;
  length_limit: number; // maximum default track length per item in number of positions
  show_past_days: number; // maximum default track length per item in days before now
}

export interface MapLayersConfig {
  background_tiles: string;
  openseamap: boolean;
  showcaptions: boolean;
  positiontimestamps: boolean;
}

export interface MapViewConfig {
  zoom: number;
  center: [number, number];
}

// storage.set('mapzoom', (this.map as any)._zoom);
