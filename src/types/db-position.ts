/**
 * Pure lat/lon coordinates and nothing else.
 *
 * Used a lot in the `mapWrapper`.
 */
export interface MapCoordinates {
  /* index signature */
  [key: string]: any;

  /**
   * latitude (Breitengrad)
   *
   * In geography, latitude is a coordinate that specifies the north–south position of a point
   * on the surface of the Earth or another celestial body. Latitude is given as an angle that
   * ranges from –90° at the south pole to 90° at the north pole, with 0° at the Equator.
   * --Wikipedia
   */
  lat: number;

  /**
   * longitude (Längengrad)
   *
   * Longitude is a geographic coordinate that specifies the east–west position of a point on
   * the surface of the Earth, or another celestial body. It is an angular measurement, usually
   * expressed in degrees.
   * --Wikipedia
   */
  lon: number;
}

/**
 * A possibly new position that has never been written to the database yet,
 * and therefore has neither an `_id` field nor references to the item that this position is (will be) for.
 * See `DbPositionPartial` for the missing id fields and `DbPosition` for positions that
 * have been previously stored and read from the database.
 */
export interface NewPosition extends MapCoordinates {
  /** The altitude over sea level(?) at which a given aircraft is flying. */
  altitude?: number;

  /**
   * The direction in which the pointy end of a ship points.
   * This field is currently also being used for storing the Course over Ground, CoG :-/
   */
  heading?: number;

  // /**
  //  * The Course over Ground, CoG, is the direction in which a ship is actually travelling
  //  * and may be different from its heading due to winds and/or water currents.
  //  * This field is not being used yet, and the location service only knows the field `heading`. :-/
  //  */
  // course?: undefined;

  /** invalid positions should never be deleted by the frontend app, only maked as "soft_deleted" */
  soft_deleted?: boolean;

  /**
   * The datasource that supplied the data for this position. This field must not be user-editable!
   *
   * Examples include:
   * - fleetmon
   * - marinetraffic
   * - iridium
   * - onefleet
   */
  source: string;

  /** Speed over Ground, SoG */
  speed?: number;

  /** ISO format. May only be changed before _id field is created. See type `DbPositionPartial` below. */
  timestamp: string;

  /** The standard deviation of the coordinates in meters, i.e., "a blurry radius". */
  uncertainty_radius?: number;

  /** additional notes on a single position. Use wisely. */
  comments?: string;
}
/**
 * This constant should list all fields of the compiletime-only NewPosition interface.
 * TODO: Ensure that this constant is always in sync with NewPosition during compile time.
 */
export const STORABLE_POSITION_FIELDS = [
  'lat',
  'lon',
  'altitude',
  'heading',
  'soft_deleted',
  'source',
  'speed',
  'timestamp',
  'uncertainty_radius',
  'comments',
];

/**
 * In-memory extension of `NewPosition` for display only.
 * These contents should never be written to the database but instead
 * be computed on-the-fly from the fields of `NewPosition` when needed.
 */
export interface DisplayPosition extends NewPosition {
  /** DisplayPositions MIGHT have an _id field, but don't rely on it! */
  readonly _id?: string;

  /** contains the absolute time in human-readable format */
  time: string;

  /** contains the relative time in human-readable format */
  age: string;

  /** contains the position coordinates in human-readable DMS format */
  dms: string;

  /** contains the altitude with " ft" added */
  altitude_feet: string;

  /** signals whether the current contents of the lat/lon fields are valid */
  broken: boolean;
}

/**
 * A position from (or destined for) the database. The fields `_id`, `item_id` and `timestamp`
 * can only be added while creating new objects of this type, and should never be altered later.
 * This is because IDs must never change for objects that are already in the database.
 * As the timestamp becomes part of the position's ID field for more efficient searching,
 * it too needs to be read-only in this type.
 */
export interface DbPositionPartial extends NewPosition {
  /** item_identifier + '_' + timestamp (isotime) + '_' random id (ulid) */
  readonly _id: string;

  /** must never change. Rather copy & soft-delete a position. */
  readonly item_id: string;

  /** ISO format. Must never change. Rather copy & soft-delete a position. */
  readonly timestamp: string;

  /** The time at which this position was created, in ISO format. */
  time_created?: string;

  /** The time at which this position was last modified, in ISO format. */
  time_modified?: string;

  /** CouchDB revision for comparing changes. May need to be removed before storing a new revision. */
  _rev?: string;
}

/**
 * Positions retrieved from the database always also have a `_rev` field, which sometimes requires special
 * handling for common database operations, and hence should be reflected in the type DbPosition.
 */
export type DbPosition = DbPositionPartial;

export type DbPositionsPerItem = Record<string, Array<DbPosition>>;
