import { createApp } from 'vue';

import ElementPlus from 'element-plus';
import 'element-plus/theme-chalk/index.css';
import * as ElementPlusIconsVue from '@element-plus/icons-vue';

import lang from 'element-plus/es/locale/lang/en';

import '@fortawesome/fontawesome-free/js/fontawesome';
import '@fortawesome/fontawesome-free/js/solid';

import { DbWrapper } from '@/utils/dbWrapper';
import type { MapWrapper } from '@/utils/mapWrapper';
import mapWrapper from '@/utils/mapWrapper';

import 'element-plus/theme-chalk/el-overlay.css';
import 'element-plus/theme-chalk/el-message.css';
import 'element-plus/theme-chalk/el-message-box.css';
import 'element-plus/theme-chalk/dark/css-vars.css';

import { createPinia } from 'pinia';
import piniaPluginPersistedState from 'pinia-plugin-persistedstate';

import App from './App.vue';
import './style.css';

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $db: DbWrapper;
    $map: MapWrapper;
  }
}

const app = createApp(App);

app.use(ElementPlus, {
  locale: lang, // configure language of ElementPlus
});

app.use({
  install: (app) => {
    const db = new DbWrapper();

    app.config.globalProperties.$db = db;
    app.config.globalProperties.$map = mapWrapper;

    app.provide('map', mapWrapper);
    app.provide('db', db);
  },
});

const pinia = createPinia();

pinia.use(piniaPluginPersistedState);
app.use(pinia);

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component);
}

app.config.globalProperties.$db = new DbWrapper();
app.config.globalProperties.$map = mapWrapper;

app.mount('#app');
