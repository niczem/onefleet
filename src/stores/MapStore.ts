import { defineStore } from 'pinia';
import storage from '@/utils/storageWrapper';

import type { MapTracksConfig } from '@/types/config';

type MapState = {
  map_tracks_config: MapTracksConfig;
  itemLayerVisibility: { title?: string; isShowing: boolean };
};

/**
 * Holds data and provides function for map-related controls
 */
export const useMapStore = defineStore<string, MapState, any, any>('map', {
  state: () => ({
    map_tracks_config: storage.get_map_tracks_config(),
    itemLayerVisibility: {
      title: undefined,
      isShowing: true,
    },
  }),
  getters: {},
  actions: {
    setItemLayerVisibility(val: { title: undefined; isShowing: true }): void {
      this.itemLayerVisibility = val;
    },
    updateStorage(): void {
      this.map_tracks_config = storage.get_map_tracks_config();
    },
  },
});
