import { defineStore } from 'pinia';

import type { NewPosition } from '@/types/db-position';

import type { Templates } from '../constants/templates';

type ShowState = {
  /**
   * Whether ExportItem should be displayed
   */
  exportItemId?: string;
  /**
   * Data for EditItem
   */
  createItem: {
    /**
     * Whether EditItem should be displayed
     */
    show: boolean;
    /**
     * The template of the item to be created
     */
    templateKey?: keyof typeof Templates.templates;
    /**
     * Given Positions for itemId
     */
    positions?: NewPosition[];
  };
  /**
   * Data for EditItem
   */
  showItem: {
    /**
     * Whether EditItem should be displayed and the itemId if so
     */
    id?: string;
    /**
     * Given Positions for itemId
     */
    positions?: NewPosition[];
  };
  /**
   * Whether LoadingScreen should be displayed
   */
  showLoading: boolean;
  /**
   * Data for LoginScreen
   */
  loginScreen: {
    /**
     * Whether LoginScreen should be displayed
     */
    show: boolean;
    /**
     * Error that might have led to LoginScreen display
     */
    errorMessage?: string;
  };
  /**
   * Whether LogSidebar should be displayed
   */
  showLogs: boolean;

  showSettings: boolean;
  showSheetPasswordCheck: boolean;
  /**
   * Whether to show MapArea, ListView or CalcImportTool
   */
  view: 'MAP' | 'LIST' | 'EXCEL';
};

/**
 * Holds data and provides function necessary for controlling which component to show and with which data to prefill
 */
export const useShowStore = defineStore<string, ShowState, any, any>('show', {
  state: () => ({
    exportItemId: undefined,
    createItem: {
      show: false,
      templateKey: undefined,
      positions: [],
    },
    showItem: {
      id: undefined,
      positions: undefined,
    },
    showLoading: true,
    loginScreen: {
      show: true,
      error: undefined,
    },
    showLogs: false,
    showSettings: false,
    showSheetPasswordCheck: false,
    view: 'MAP',
  }),
  getters: {},
  actions: {
    setExportItemId(val: string): void {
      this.exportItemId = val;
    },
    setCreateItem(val: boolean): void {
      this.createItem = val;
    },
    setShowItem(val: boolean): void {
      this.showItem = val;
    },
    setShowLoading(val: boolean): void {
      this.showLoading = val;
    },
    setLogin(val: boolean): void {
      this.loginScreen = val;
    },
    setShowLogs(val: boolean): void {
      this.showLogs = val;
    },
    setShowSettings(val: boolean): void {
      this.showSettings = val;
    },
    setShowSheetPasswordCheck(val: boolean): void {
      this.showSheetPasswordCheck = val;
    },
    setView(val: string): void {
      this.view = val;
    },
  },
});
