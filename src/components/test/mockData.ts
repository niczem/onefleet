import type { DbPosition } from '@/types/db-position';
import type { DbItem } from '@/types/db-item';

/**
 * A class to store mock Data
 */

export default class mockData {
  public static positions: DbPosition[] = [
    {
      _id: '1',
      item_id: 'a',
      timestamp: '2022-04-15T11:24:35.431Z',
      source: 'onefleet',
      lat: 234234,
      lon: 234234,
    },
    {
      _id: '2',
      item_id: 'b',
      timestamp: '2022-03-15T11:24:35.431Z',
      source: 'fleetmon',
      lat: 234234,
      lon: 234234,
    },
  ];
  public static base_item: DbItem = {
    _id: '12345',
    time_created: new Date().toISOString(),
    time_modified: new Date().toISOString(),
    template: 'case',
    properties: { name: 'name' },
    map_settings: { marker_color: 'blue', marker_icon: 'case' },
    prefix_created: 'v_test',
    prefix_modified: 'v_test',
  };
}
