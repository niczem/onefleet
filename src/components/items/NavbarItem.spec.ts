import { useShowStore } from '@/stores/ShowStore';
import { shallowMount } from '@vue/test-utils';
import { createPinia, setActivePinia } from 'pinia';
import mockData from '../test/mockData';
import NavbarItem from './NavbarItem.vue';

function createWrapperAndMocks() {
  const mockColorSince = vi.fn();
  setActivePinia(createPinia());
  const wrapper = shallowMount(NavbarItem, {
    propsData: {
      timeNow: new Date(),
      baseItem: mockData.base_item,
      positions: mockData.positions,
    },
    global: {
      mocks: { $map: { colorSince: mockColorSince } },
    },
  });

  const showStore = useShowStore();
  return {
    wrapper,
    showStore,
  };
}

describe('computed Properties', () => {
  it('calculates item name correctly', () => {
    const { wrapper } = createWrapperAndMocks();
    expect(wrapper.vm['itemName']).toMatch('name');
    wrapper.vm.$props.baseItem.properties.name = '';
    expect(wrapper.vm['itemName']).toMatch('');
  });

  it('it returns latest position if position', () => {
    const { wrapper } = createWrapperAndMocks();
    expect(wrapper.vm['latestPosition']).toMatchObject(mockData.positions[0]);
  });

  it('returns correct color', () => {
    const { wrapper } = createWrapperAndMocks();

    expect(wrapper.vm['itemColor']).toMatch('blue');
  });
});

describe('methods', () => {
  it('correctly changes the showItem value in showStore', () => {
    const { wrapper, showStore } = createWrapperAndMocks();
    wrapper.vm['clickItem']();
    expect(showStore.showItem).toMatchObject({
      id: mockData.base_item._id,
      positions: undefined,
    });
  });
});
