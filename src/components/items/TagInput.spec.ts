import { shallowMount } from '@vue/test-utils';
import TagInput from './TagInput.vue';
import type { TagSettings } from '@/types/tags';

const baseTags: string = 'LY:40;AF:50;SS:10';

const baseTagSettings: TagSettings = {
  fixedKeyOptions: {
    SS: 'Stateless',
    AF: 'Afghanistan',
    LY: 'Libya',
    DZ: 'Algeria',
  },
  allowOpenOptions: false,
  keyOnly: false,
  forceUniqueKeys: true,
  labelKey: 'Select country',
  labelValue: 'This is the label',
};

function createWrapper(tags: string, tagSettings: TagSettings) {
  const wrapper = shallowMount(TagInput, {
    props: {
      field: tags as string,
      propTagSettings: tagSettings,
    },
  });

  return wrapper;
}

describe('TagInput', () => {
  const wrapper = createWrapper(baseTags, baseTagSettings);
  it('renders', () => {
    expect(wrapper).toBeTruthy();
  });

  it('Returns a correct array from the tag sent by parent', () => {
    expect(wrapper.vm['dynamicTags']).toMatchObject([
      'LY:40',
      'AF:50',
      'SS:10',
    ]);
  });
});
