<script lang="ts">
import type { DbPosition, NewPosition } from '@/types/db-position';
import type { PropType } from 'vue';
import { defineComponent } from 'vue';

import DatetimePicker from '../DatetimePicker.vue';
import PositionInput from './PositionInput.vue';

import { formatTimestamp } from '@/utils/datetimeUtils';
import { asDMS } from '@/utils/map/positionUtils';

export default defineComponent({
  name: 'SinglePosition',
  components: {
    DatetimePicker,
    PositionInput,
  },
  model: {
    prop: 'position',
    event: 'update:position',
  },

  props: {
    position: {
      required: true,
      type: Object as PropType<DbPosition | NewPosition>,
    },
    originalPosition: {
      required: true,
      type: Object as PropType<DbPosition | NewPosition>,
    },
    editMode: {
      type: Boolean as PropType<boolean>,
      default: true,
    },
  },

  emits: ['update:position', 'finished_editing'],

  data() {
    return {
      internal_position: this.copiedPositionProp(),
      timestamp_date: null as Date | null,
    };
  },

  computed: {
    /**
     * Decide whether the position fields should be editable or not
     */
    editable(): boolean {
      return (
        this.editMode &&
        !(this.internal_position?.soft_deleted ?? false) &&
        this.internal_position?.source === 'onefleet'
      );
    },
    deletedCSS(): string {
      return this.internal_position?.soft_deleted ? 'soft-deleted' : '';
    },
  },

  watch: {
    timestamp_date(newValue: Date | null) {
      // Iff the internal_position is a new position, then update its timestamp
      if (this.internal_position && !this.internal_position['_id']) {
        // positions without an _id field are considered new positions
        let internal_new_position = this.internal_position as NewPosition;
        if (newValue) {
          internal_new_position.timestamp = newValue.toISOString();
          this.emitChangeEvent();
        }
      }
    },
  },

  mounted(): void {},

  created(): void {
    this.timestamp_date = this.copiedOriginalTimestampDate();
  },

  methods: {
    formatCoordinates(): string {
      return asDMS(this.internal_position);
    },
    changedCoordinatesOnly(position: NewPosition | DbPosition | null): void {
      if (position && this.internal_position) {
        this.internal_position.lat = position.lat;
        this.internal_position.lon = position.lon;
        this.emitChangeEvent();
      }
    },
    toggleSoftDeleted(): void {
      if (this.internal_position) {
        const isSoftDeleted = this.internal_position.soft_deleted ?? false;
        this.internal_position.soft_deleted = !isSoftDeleted;
        this.emitChangeEvent();
      }
    },
    undoAllChanges(): void {
      this.internal_position = this.copiedPositionProp();
      this.timestamp_date = this.copiedOriginalTimestampDate();
      this.emitChangeEvent();
    },
    /** To be called on each change to a position */
    emitChangeEvent(): void {
      this.$emit('update:position', { ...this.internal_position });
    },
    /** To be called when finished editing and the displaypositions can be updated */
    emitFinishedEditing(): void {
      this.$emit('finished_editing');
    },
    copiedPositionProp(): DbPosition | NewPosition {
      return JSON.parse(JSON.stringify(this.position));
    },
    copiedOriginalTimestampDate(): Date | null {
      return this.internal_position
        ? new Date(this.internal_position.timestamp)
        : null;
    },
    updatePositionTimestamp(event: Date | string | null | undefined): void {
      if (event) {
        this.timestamp_date = new Date(event);
      } else {
        this.timestamp_date = null;
      }
    },
    changedCSS(field_names: string[]): string {
      for (const field_name of field_names) {
        if (
          this.internal_position &&
          this.originalPosition &&
          this.internal_position[field_name] !=
            this.originalPosition[field_name]
        ) {
          return 'changed-row';
        }
      }
      return '';
    },
    changedFieldTooltip(field_names: string[]): string {
      let tooltip_text = '';
      for (const field_name of field_names) {
        tooltip_text += `Original '${field_name}': ${
          (this.originalPosition ?? {})[field_name]
        } \n`;
        tooltip_text += `Current '${field_name}': ${
          (this.internal_position ?? {})[field_name]
        } \n\n`;
      }
      return tooltip_text;
    },
    /**
     * This imported function formatTimestamp() needs to be named here once for the template to find it:
     */
    formatTimestamp,
  },
});
</script>

<template>
  <div class="positionbox">
    <!-- Row: {{ props.$index }} -->
    <!-- CL: {{ showLog(props) }} -->
    <!-- ID: {{ internal_position._id }} <br /> -->

    <span v-if="internal_position.source != 'onefleet'">
      <p>
        ℹ️ This position was created via automatic tracking and therefore
        <br />
        cannot be changed manually. But you can mark it as incorrect.<br />
      </p>
    </span>

    <table :class="deletedCSS">
      <tr :class="changedCSS(['lat', 'lon'])">
        <td :title="changedFieldTooltip(['lat', 'lon'])">Coordinates:</td>
        <td v-if="editable">
          <PositionInput
            name="position_input"
            print-feedback
            class="coordinates-input"
            :position="internal_position"
            @update_coords="changedCoordinatesOnly($event)"
          />
        </td>
        <td v-else>
          {{ formatCoordinates() }}
        </td>
      </tr>
      <tr :class="changedCSS(['timestamp'])">
        <td :title="changedFieldTooltip(['timestamp'])">Time:</td>
        <td v-if="editable">
          <DatetimePicker
            :datetime-local="timestamp_date.toISOString()"
            type="datetime"
            placeholder="Enter time of sighting (UTC)"
            format="YYYY-MM-DD HH:mm"
            size="large"
            :disabled="Boolean(internal_position['_id'])"
            title="Only NEW positions can have their timestamp changed."
            @update_datetime="updatePositionTimestamp($event)"
          />
        </td>
        <td v-else>
          {{ formatTimestamp(internal_position.timestamp) }}
        </td>
      </tr>
      <tr :class="changedCSS(['heading'])">
        <td :title="changedFieldTooltip(['heading'])">
          Course over Ground (deg):
        </td>
        <td v-if="editable">
          <input
            v-model="internal_position.heading"
            name="tracking_heading"
            type="text"
            @input="emitChangeEvent"
          />
        </td>
        <td v-else>
          {{ internal_position.heading }}
        </td>
      </tr>
      <tr :class="changedCSS(['speed'])">
        <td :title="changedFieldTooltip(['speed'])">Speed over Ground (kn):</td>
        <td v-if="editable">
          <input
            v-model="internal_position.speed"
            name="tracking_speed"
            type="text"
            @input="emitChangeEvent"
          />
        </td>
        <td v-else>
          {{ internal_position.speed }}
        </td>
      </tr>
      <tr :class="changedCSS(['altitude'])">
        <td :title="changedFieldTooltip(['altitude'])">Altitude:</td>
        <td v-if="editable">
          <input
            v-model="internal_position.altitude"
            name="tracking_altitude"
            type="text"
            @input="emitChangeEvent"
          />
        </td>
        <td v-else>
          {{ internal_position.altitude }}
        </td>
      </tr>
      <tr :class="changedCSS(['source'])">
        <td :title="changedFieldTooltip(['source'])">Datasource:</td>
        <td>
          {{ internal_position.source }}
        </td>
      </tr>
      <tr :class="changedCSS(['soft_deleted'])">
        <td :title="changedFieldTooltip(['soft_deleted'])">
          Incorrect / "Deleted":
        </td>
        <td>
          {{ internal_position.soft_deleted ?? false }}
        </td>
      </tr>
    </table>

    <el-button-group>
      <el-button
        title="Mark this position as incorrect"
        plain
        type="danger"
        @click="toggleSoftDeleted()"
        ><el-icon
          ><DeleteFilled v-if="internal_position.soft_deleted ?? false" />
          <Delete v-else />
        </el-icon>
      </el-button>
      <el-button
        title="Undo changes to this position"
        plain
        type="primary"
        :disabled="!Boolean(internal_position['_id'])"
        @click="undoAllChanges()"
        ><el-icon><RefreshLeft /></el-icon>
      </el-button>
      <el-button
        title="Finish editing and update table display"
        plain
        type="success"
        @click="emitFinishedEditing()"
        ><el-icon><Finished /></el-icon>
      </el-button>
    </el-button-group>
  </div>
</template>

<style scoped>
.positionbox > label,
.positionbox > div > label {
  margin-left: 5px;
  min-width: 100px;
  display: inline-block;
  margin-bottom: 0px;
  padding-bottom: 0px;
}

.coordinates-input {
  font-size: 15px;
  height: 200%;
  padding: 0px;
}

.el-button-group .el-button {
  width: 120px;
}
</style>
