import { mount } from '@vue/test-utils';
import NumberInput from './NumberInput.vue';

const factory = (
  valuesProps = {
    label: 'Unknown' as string,
    propMaximumValue: 1000 as number,
  },
  valuesData = {},
  valueMocks = {}
) => {
  return mount(NumberInput, {
    data() {
      return {
        ...valuesData,
      };
    },
    props: valuesProps,
    global: {
      mocks: {
        ...valueMocks,
      },
    },
  });
};

let wrapper;
beforeEach(() => {
  wrapper = factory();
});
afterEach(() => {
  wrapper.unmount();
});

describe('NumberInput Component', () => {
  it('renders', () => {
    expect(wrapper).toBeTruthy();
  });
  it('resets to -1 when checkbox is activated', () => {
    wrapper.setData({
      value: 100,
    });
    wrapper.vm['resetValue']();
    expect(wrapper.vm['value']).toEqual(-1);
  });
  it('resets to 0 when checkbox is deactivated', () => {
    wrapper.setData({
      value: -1,
    });
    wrapper.vm['resetValue']();
    expect(wrapper.vm['value']).toEqual(0);
  });
  // TODO it("checkbox is checked when value is at 'unknown' value", () => {})
  // TODO it('checkbox is unchecked when value is within number bounds', () => {})
});
