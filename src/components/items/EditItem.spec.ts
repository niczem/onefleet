import { shallowMount } from '@vue/test-utils';
import type { NewPosition } from 'services/location/types';
import EditItem from './EditItem.vue';
import { createTestingPinia } from '@pinia/testing';

function createWrapperAndMocks(props = {}, data = {}) {
  const mockMessage = {
    error: vi.fn((message) => message),
    success: vi.fn((message) => message),
  };
  const mockCreateItem = vi.fn();
  const mockCreatePositions = vi.fn();
  const wrapper = shallowMount(EditItem, {
    data() {
      return {
        ...data,
      };
    },
    props: props,
    global: {
      plugins: [createTestingPinia()],
      mocks: {
        ...{
          $message: mockMessage,
          $db: {
            createItem: mockCreateItem,
            createPositions: mockCreatePositions,
          },
        },
      },
    },
  });
  return {
    wrapper,
    mockMessage,
    mockCreateItem,
    mockCreatePositions,
  };
}
describe('CreateItem Component', () => {
  it('renders', () => {
    expect(createWrapperAndMocks().wrapper).toBeTruthy();
  });
});
describe('prefillProperties Method', () => {
  it('returns first item of fields array and ignores second item.', () => {
    const template: any = {
      fields: [
        { name: 'df', title: 'dsf', type: 'text' },
        { name: 'uz', title: 'ikdsf', type: 'number', default_value: 232 },
      ],
    };
    expect(
      createWrapperAndMocks().wrapper.vm['prefillProperties'](template).uz
    ).toEqual(232);
  });
});

describe('editItem Method', () => {
  it('returns false boolean if name is too short', () => {
    const { wrapper, mockMessage } = createWrapperAndMocks();
    wrapper.vm.form_data = {
      properties: {
        name: 'ab',
      },
    };
    wrapper.vm['saveItem']();

    expect(mockMessage.error).toHaveBeenLastCalledWith(
      expect.objectContaining({
        message: 'Please enter a valid name',
      })
    );
  });

  it('returns false boolean if zero positions are given and the template enforces an initial position', () => {
    const { wrapper, mockMessage } = createWrapperAndMocks(
      {},
      {
        showStore: {
          createItem: {
            templateKey: 'sighting',
          },
        },
      }
    );
    wrapper.vm.form_data = {
      properties: {
        name: 'test',
      },
    };
    wrapper.vm.template = {
      enforce_initial_position: true,
    };
    wrapper.vm['saveItem']();
    expect(mockMessage.error).toHaveBeenLastCalledWith(
      expect.objectContaining({
        message:
          'The template "sighting" requires at least one position, according to its configuration.',
      })
    );
  });

  it('shows error and success messages when calling $db', async () => {
    const position = {
      altitude: 1,
      heading: 1,
      speed: 1,
      lat: 1,
      lon: 1,
      source: 'onefleet',
      timestamp: '2021-03-29T12:25:37.678Z',
    } as NewPosition;

    const data = {
      form_data: {
        template: 'case',
        properties: {
          name: '1234',
          pob_total: '98',
        },
        time_created: new Date().toISOString(),
      },
      positions: [position],
    };
    const { wrapper, mockCreateItem, mockMessage } = createWrapperAndMocks(
      {},
      data
    );

    //db returns error
    mockCreateItem.mockImplementationOnce(() => 'error');
    await wrapper.vm['saveItem']();
    expect(mockMessage.error).toHaveBeenLastCalledWith(
      expect.objectContaining({
        message: 'An unknown error occured while creating the item.',
      })
    );
  });
});
