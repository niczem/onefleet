# FAQ

This page contains frequently asked questions on different topics.

- [Release versioning FAQ](#release-versioning-faq)

## Release versioning FAQ

**1. I think we should have at least one new database every year, correct?**

As I understand it, it is not really important how often we change the database prefix (which is synonymous with creating a new database), but rather we have to make sure that the public Api does not change within that database. I think the main part of the public api is our template system. This technically and organizationally defined:

- technically defined in the files that are in `src/constants/templates/*` and in the file `src/types/item-template-schema.ts`
- organizationally in the Excel file that describe the template system. The excel file should also get a version and that version should be inside the repository, I think.

Semantic Versioning defined by semver.org says accordingly:

> As a solution to this problem, we propose a simple set of rules and requirements that dictate how version numbers are assigned and incremented. These rules are based on but not necessarily limited to pre-existing widespread common practices in use in both closed and open-source software. For this system to work, you first need to declare a public API. This may consist of documentation or be enforced by the code itself. Regardless, it is important that this API be clear and precise. Once you identify your public API, you communicate changes to it with specific increments to your version number. Consider a version format of X.Y.Z (Major.Minor.Patch). Bug fixes not affecting the API increment the patch version, backwards compatible API additions/changes increment the minor version, and backwards incompatible API changes increment the major version.

**2. What I was wondering: when we fix a bug and have a new deployment, is this already a new app version?**

If we fix a bug and merge to the develop branch nothing happens. The moment we merge from the develop branch to the main branch we have to increase the version number. If the public api does not change (e.g. the excel is not updated and the templates are not changed) only the patch increases (the third number). If we update the templates, we have to update the excel and then we have to either update the minor version(second number) or the major version (first number). I think we normally update the minor version, and if we have the feeling the app has changed a lot we can update the major version (but that can be discussed). It does not really matter if we update the major or minor version, it only matters that we do not change the public api when we increase the patch version.

**3. And how are we counting our app versions? 1, 2, 3... only or also something like 1.0, 1.1., 1.2, 2.0, ...?**

We follow semantic versioning as defined on https://semver.org/. This means there are three numbers, major, minor, patch. each time we merge to the main branch we have to update one of the three numbers. Normally we just update the patch number, but if we change the template system we have to update either the minor or major version.

**4. I have not yet fully understood why we would save our major database version update for ~ once a year (based on our feeling)?**

> Doesn't it suffice to update the version number whenever there is a technical requirement? What would speak against increasing our major version fast?
> I think people do not really deploy our app and the DB by themselves, we deal with it all by ourselves, right? So there would be no issue that a version upgrade would lead to incompatibilities.

That is also possible, I don't really know what would be best. As far as I understood Mareike, there is always work to communicate to the users when there are new features. So, if we kind of try make bigger changes with updating the major version once in a while and smaller changes with updating the minor version more often, we get an idea how much change we have to communicate to the user. But yeah, I don't know what would be best. When do you think we should update the major version?

**5. I think people do not really deploy our app and the DB by themselves, we deal with it all by ourselves, right? So there would be no issue that a version upgrade would lead to incompatibilities.**

As long as the Public API, i. e. the template system that affects the db schema and the Excel file that contains the current template system status, is not changed that is true I think. But as I understand, if any of the two changes, we would have to create a new database (make a new prefix), and write a migration script to migrate the old data to the new database. I think even though we deal with it ourselves we should be very clear so that SARchive can use the onefleet data.
