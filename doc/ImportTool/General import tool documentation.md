# Import Tool Documentation

## Overview

This document provides guidance on how to import data using a specific import tool. It covers the general information required to access and use the tool effectively.

### Accessing the Tool

To access the import tool, click on your user name, then click on "import tool".

### Import Requirements

Once you have accessed the tool, you need to provide two pieces of information to enable parsing:

1. **Last Row Number**: Indicate the number of the last row in your file containing a case to be parsed.
2. **File Upload**: Currently, the tool supports only `.xlsx` files. If your file is in a different format (e.g., `.ods`, `.xls`, or `.csv`), convert it using OnlyOffice or LibreOffice.

### File Specifications

#### Sheet Names

- The first sheet should be named **"Data"** and contain the cases to be parsed.
- The second sheet should be named **"Template"**. The cell A1 in this sheet should contain text specifying the template for parsing. Refer to the documentation for each template for detailed instructions.

### Confirmation and Parsing

After filling in the required information, click 'Confirm'. A table will appear, detailing the parsing results for each row in the file. The columns in this table provide information about the parsed data, but there are specific aspects to note:

1. **Row Number**: The left-most column corresponds to the row number in the original file.
2. **Case Naming**: Users are responsible for naming cases. Ensure each case in your file has a name, following any existing naming conventions (e.g., AP cases in onefleet).
3. **Timestamp Requirement**: Each row must have a valid timestamp in the 'occured_at' field. This timestamp is critical for grouping cases in the SARchive. Rows without a valid timestamp will not be parsed.
4. **Positions Parsed**: This column indicates the number of positions parsed in each row but does not specify the positions.

### General Information

- **Column Name (NoC)**: A human-readable identifier, not used in the tool's parsing process.
- **Column Letter(s) (CL)**: The key identifier used for data parsing.
- **Onefleet Property (OP)**: The label under which data appears in the tool's output columns after parsing.
- **Fallbacks**: Used when no valid data is found in the primary CL.

### Types of Fields

- **Select**: Fields with pre-defined strings in the Onefleet database. For accurate parsing, entries must strictly adhere to these predefined options.
- **Number**: Only parses whole numbers present in the cell. Approximations or ranges (like "36/37") are not parsed. When a number field spans two columns, the numbers from both columns are summed.
- **Datetime**: Typically composed of two columns: one for the date (formatted as `DD/MM/YYYY`) and the other for the time (formatted as `HH@MM`). If the time column is missing or cannot parse a correct time, midnight is automatically chosen, but this only applies if a valid date is present in the first column. All datetime fields need to be in UTC.
- **Text**: Freeform fields. Any text in the column will be parsed as it is. Users should ensure that the text adheres to OF entry guidelines (available on the CMRCC cloud), especially for fields requiring specific formats, like `place_of_departure`.
- **Inputtable**: These fields are used for URLs. When entering multiple URLs, separate them using the triple character sequence `|||`.
- **Regex**: Regex types include examples for correctly formatted strings. Typically this is only used for positions

### Post-Parsing Actions

#### Adjustments

If the parsed information is incorrect (e.g., due to improper cell formatting), you can reset the tool and re-import the file after making necessary corrections.

#### Saving

If you are satisfied with the parsed data:

- Click the "save all cases" button.
- You will then be redirected to the map interface.
- Imported cases are typically marked as "Ready for documentation".
- Use the filter for these cases and sort by the most recent to find the cases you have just imported.