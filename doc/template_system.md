Last updated: January 2022

# Templatesystem

All objects shown in the map are defined as "items" and stored in the items database. Each item has a template (like "vehicle", "case" or "landmark") which are defined in the `src/constants/templates/`.

## Field Types

Field types are defined in the Typescript Interface `ItemTemplateField` in `src/types/item-template-schema.ts`.

## Tags

Tags are [not implemented](https://gitlab.com/civilmrcc/onefleet/-/merge_requests/71). Please see issue [80](https://gitlab.com/civilmrcc/onefleet/-/issues/80).

# Workflow to change template

The workflow to change a template is described in the following flowchart:

![workflow_change_template_fields](template-fields/workflow_change_template_fields.png)

## Database prefix

When changes are made to the templates, a version has to be released according to [release_versioning](./release_versioning.md).
