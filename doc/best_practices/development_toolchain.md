# Development Workflows, Tools & Environments

This document exists to give new developers a running start into the tools used by existing developers and how the development process can be made easier using helpful tools for each job. When you find a useful workflow, tool or environment setup that helps you in your development process, please consider adding it here. Existing developers might also like to learn about new tools that make engineering more efficient or simply more fun!

## Editor and Vue-Devtools

Recommended editor is [Visual Studio Code](https://code.visualstudio.com/) with the extension [Vetur](https://marketplace.visualstudio.com/items?itemName=octref.vetur) to support `.vue` files. The [Vue-devtools](https://github.com/vuejs/vue-devtools) plugin for Firefox or Chrome/Chromium is also highly recommended.

## Code Editor's Corner

This section is all about exchanging hints & findings about editor tools or integrated development environments (IDEs) that have been found useful.
Here are some editors commonly used in this project:

- Vim, Emacs, Nano & other text mode editors - allow quick edits even through a remote ssh connection
- [VSCode](https://code.visualstudio.com/) - one of the most widely used editors as it is highly extendable and fast to learn.
- Sublime - fast and loved by many
- Pycharm Community Edition / Webstorm - powerful IDE but closed source

#### VSCode: Suggested Plugins

- [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) for supporting `.vue` files
- [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) Code formatter
- [Eslint] (<https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint>) Code linting

#### VSCode: Optional Plugins

- [GitLab Workflow](https://marketplace.visualstudio.com/items?itemName=fatihacet.gitlab-workflow) Integrate GitLab into VSCode
- [Git Graph](https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph) View a Git Graph of your repository, and perform Git actions from the graph.
- [Document This](https://marketplace.visualstudio.com/items?itemName=joelday.docthis) Automatically generates detailed JSDoc comments in TypeScript and JavaScript files.
- [Mermaid Syntax Highlighting](https://marketplace.visualstudio.com/items?itemName=bpruitt-goddard.mermaid-markdown-syntax-highlighting) syntax highlighting & completion for Marmaid diagrams
- [Mermaid Preview](https://marketplace.visualstudio.com/items?itemName=vstirbu.vscode-mermaid-preview) Previews Mermaid diagrams
- [Peacock](https://marketplace.visualstudio.com/items?itemName=johnpapa.vscode-peacock) Change editor color for different repositories and/or git-worktrees.
- [Docker](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker) Makes it easy to create, manage, and debug containerized applications.
- [Vite] (<https://marketplace.visualstudio.com/items?itemName=antfu.vite>) Starts the dev server/Debug from the IDE
- [Vitest] (<https://marketplace.visualstudio.com/items?itemName=ZixuanChen.vitest-explorer>) Runs tests inside the IDE/Watch mode to unit test during development
- [PostCSS] (<https://marketplace.visualstudio.com/items?itemName=csstools.postcss>) Suoport for PostCSS

## Setting up your development environment beyond "npm install"

tbc

#### Development Web Browser: Suggested Plugins

- The [Vue-devtools](https://github.com/vuejs/vue-devtools) plugin for Firefox or Chrome/Chromium is highly recommended.

## Typical development workflow

tbc

## Debugging JavaScript files with Visual Studio Code and Chrome

Enable remote debugging in Chrome. You can do that by starting Chrome with `chrome.exe --remote-debugging-port=9222`. If you are using Windows you can simply [edit the Chrome shortcut](https://stackoverflow.com/a/56457835/2306587) to start Chrome with remote debugging enabled.
In linux you can do this using `chromium --remote-debugging-port=9222` in your terminal but you can also edit the .desktop file/shortcut and add `--remote-debugging-port=9222` in the `Arguments` field.

Start the container as documented in the README and open the `Run and Debug` menu of vscode. Choose the `Attach to Chromium` preconfigured command. You can find this configuration in the following file `.editor-settings/.vscode/launch.json` file if it's not already been copied to your local `.vscode/launch.json` file.

If the debugger successfully attached to chrome you should see some console output in the tab "DEBUG CONSOLE".

Now you can add break points to Typescript files and even inside of Vue components <script>content</script>. One quirk is that when placing a breakpoint inside of a Vue file, the breakpoint will actually be bound in the transpiled .js file and once the browser hits the desired breakpoint, this js file will open in read-only mode inside of VSCode.
