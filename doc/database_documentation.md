Last updated: January 2022
# Summary

Onefleet uses a PouchDB and CouchDB. PouchDB is a in-browser database and uses the IndexedDB which is part of all modern browsers. Onefleet loads data directly from this local database.

The CouchDB is a server-side database to which the local PouchDB is synchronised in the background. When onefleet is opened for the first time a local PouchDB is initiated and the data from the remote CouchDB synced to the local PouchDB.
