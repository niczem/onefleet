# ADR-005: Introducing CouchDB Mango Queries

## Context

In light of our performance issues, Neighborhoodie did a review of the App and one of the improvement suggestion was that instead of getting all documents and then filtering, [MangoQueries](https://pouchdb.com/guides/mango-queries.html) can be employed to directly query the requested subset.

**Pros**

- Making use of a better option that is available in the CouchDB ecosystem
- Better efficiency and therefore also improved performance.
- Neighborhoodie confirmed that MangoQueries are future proof as they will be working on a new PouchDB Engine which would speed up MangoQueries even more.

**Cons**

None

## Decision

- Implemented Mango Query that searches for items that match an array of filters. This is used for the initial load of items, instead of previously where all items are loaded, causing a longer waiting time for the user.
- When the section filters change, the items of the currently selected section (e.g. Cases) are reloaded using Mango Query
- When item update is registered only reload that item and not all items, this is also done using Mango Query
- When position update is registered only reload that position and not all positions, this is also done using Mango Query

## Status

Accepted and implemented.

## Consequences

Plugin [pouchdb-find](https://pouchdb.com/guides/mango-queries.html#installation) has to be installed.
The fields that shall be queried over need to be [indexed](https://pouchdb.com/guides/mango-queries.html#more-than-one-field).
