# ADR-008: Introducing Vite

## Context

As the size of the project grows, so does the starting and hot reloading time of the server, it can take dozens of seconds to first start the server and several seconds to see a change in code reflected in the browser.
This is because there are dozens of different bits of JS code that needs to be bundled by webpack to be served in the browser and run the application.
With the advent of ES6 and support for ES modules in modern browsers however, it's now possible for browsers to take over part of the job of bundlers in a dev environnement by letting them request our source code when they need it. Vite is a dev tool that takes advantage of ES modules to significantly speed up load times in a dev environnement. It also takes advantage of other developments in the JS sphere such as ESBuild, a bundler written in Go instead of JS to significantly speed up the dev environnement.

To learn in details how Vite is faster than Vue-Cli, read more at -> <https://vitejs.dev/guide/why.html>

**Pros**

- Serves all code in dev as ES modules to be dynamically requested by the browser when needed on the current screen.
- Pre-bundles all modules into a single one to reduce roundabout time between the browser and the dev server.
- Uses ESbuild (written in a lower-level language (Go) to quickly bundle modules in the dev env).
- Uses Rollup for production bundling which features a simpler API than Webpack, especially for tree-shaking.

**Cons**

- Implies refactoring existings tests to work with Vitest.
- Switches our production bundler to Rollup instead of Webpack which may mean some rework on backend integration.

## Decision

Per a suggestion of BenBen (@codepoet) in issue #487, it was decided to switch to Vite for our dev tooling to speed up our dev environnement.

**Vitest**
After an internal discussion between between @rcfcc  @simonsthings and @TransFemZboomer, it was also decided to switch the unit testing to Vitest. Sharing the same configuration as Vite is a huge plus and comes with seamless integration. This however, means that the current tests need to be refactored as maintaining two pipelines for the low number of existing Jest tests is not justifiable.

**Rollup and unplugin**
Switching to Vite also means moving from Webpack to Rollup.
In theory, we could have a smaller bundle size thanks to Rollup's tree-shaking features (also exists in webpack but is supposedly harder to implement). However as of now, the prod build in Rollup is slightly bigger.

Switching to Rollup also means adopting its plugin system to configure our bundle.
We've adopted unplugin on top of the rollup system because it allows us access to other pre-written plugins in both the Rollup and Webpack ecosystem to optimize our bundle.
This is already the case now with unplugin-vue-components which allows us to only import the vue components we actually use in the project, one example where this is useful would be our UI library: Element Plus.

**Future changes**

- We should look into optimizing our bundle size by leveraging Rollup's tree-shaking and plugin features, this will be particularly important when we split mapWrapper, this will also avoid a lot of boilerplate code in the import section
- Remove VoerroTagsInput made with Vue2 and switch to Element Plus's input tags feature
- Look into the checkbox-like artifacts that now appear at the edge of the 12 and 24NM zones.

## Status

Done
