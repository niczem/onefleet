FROM node:16-alpine

COPY . /onefleet

WORKDIR /onefleet
RUN npm ci --prefer-offline
# Always exit 0 if npm run build returns 254 otherwise return original return code workaround for https://gitlab.com/civilmrcc/onefleet/-/issues/529
RUN sh -c "npm run build"; if [ $? -ne 254 ]; then exit $?; fi; exit 0;
CMD ["npm", "run", "dev"]
