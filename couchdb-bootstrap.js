#!/usr/bin/env node
/* eslint-env node */
const couchdbBoostrap = require('couchdb-bootstrap');

/**
 * Couchdb-bootstrap script
 *
 * This is used to setup and configure couchdb initially
 *
 * @see https://github.com/jo/couchdb-bootstrap
 */

const prefix = process.env.COUCHDB_PREFIX;
if (!prefix) {
  console.log('No COUCHDB_PREFIX given. Exiting.');
  process.exit(1);
}

const config = {
  // set custom config option for CouchDB
  // https://docs.couchdb.org/en/stable/config-ref.html
  _config: {
    couchdb: {
      single_node: true,
      database_dir: '/opt/couchdb/data',
      default_security: 'admin_only',
    },
    chttpd: {
      bind_address: '::',
      enable_cors: true,
      require_valid_user: true,
    },
    chttpd_auth: {
      allow_persistent_cookies: true,
      require_valid_user: true,
      users_db_public: false,
    },
    cors: {
      credentials: true,
      origins: '*',
      methods: ['GET', 'PUT', 'POST', 'HEAD', 'DELETE'],
      headers: [
        'accept',
        'authorization',
        'content-type',
        'origin',
        'referer',
        'x-csrf-token',
      ],
    },
    log: {
      writer: 'stderr',
      level: 'info',
    },
  },

  _users: {
    _design: {},

    // These are templates for individual user-permissions in a future update
    alice: {
      _id: 'org.couchdb.user:alice@example.com',
      type: 'user',
      roles: [],
      name: 'alice@example.com',
      password: 'secure',
    },
    bob: {
      _id: 'org.couchdb.user:bob@example.com',
      type: 'user',
      roles: [],
      name: 'bob@example.com',
      password: 'secure',
    },
  },
};

config[`${prefix}items`] = {
  // define indices for Mango queries
  // http://docs.couchdb.org/en/2.1.1/api/database/find.html
  _design: {
    // this should be done via couchdb-create-index once https://github.com/jo/couchdb-bootstrap/pull/64 is merged
    propertiesjsonindex: {
      _id: '_design/propertiesjsonindex',
      language: 'query',
      views: {
        'item-properties-index': {
          map: {
            fields: {
              item_id: 'asc',
              source: 'asc',
              timestamp: 'asc',
            },
            partial_filter_selector: {},
          },
          reduce: '_count',
          options: {
            def: {
              fields: ['properties', 'template', '_id'],
            },
          },
        },
      },
    },
  },

  _security: {
    members: {
      roles: [],
      names: ['alice@example.com'],
    },
    admins: {
      roles: [],
      names: ['alice@example.com'],
    },
  },

  // Initial seed, could potentially be behind a flag
  SHIP_SEEDEDSW501HH2PKK8T0N9D1GWBT2CQKJ4M: {
    _id: 'SHIP_SEEDEDSW501HH2PKK8T0N9D1GWBT2CQKJ4M',
    identifier: 'SW5',
    template: 'ship',
    properties: {
      name: 'Sea-Watch 5',
      active: 'true',
      category: 'civilfleet',
    },
    map_settings: {
      marker_color: '#17AFFA',
      marker_icon: 'ship',
      get_historical_data_since: 900,
      tracking_type: 'AIS',
      tracking_id: '211879870',
      tracking_token: '',
    },
    time_created: '2023-12-06T20:20:02.000Z',
    time_modified: '2023-12-06T20:20:02.000Z',
    soft_deleted: false,
  },
  SHIP_SEEDEDEVERGIVENPRMB26ZQ03K: {
    _id: 'SHIP_SEEDEDEVERGIVENPRMB26ZQ03K',
    template: 'ship',
    properties: {
      name: 'EVERGIVEN',
      category: 'commercial',
      active: 'true',
    },
    map_settings: {
      marker_color: '#7ccd47',
      marker_icon: 'ship',
      get_historical_data_since: -1,
      tracking_type: 'MANUAL',
      tracking_id: '353136000',
      tracking_token: '',
    },
    time_created: '2020-06-20T21:20:02.431Z',
    time_modified: '2020-08-08T20:27:49.067Z',
    soft_deleted: false,
  },
};

config[`${prefix}positions`] = {
  _design: {
    // this should be done via couchdb-create-index once https://github.com/jo/couchdb-bootstrap/pull/64 is merged
    itemsjsonindex: {
      _id: '_design/itemsjsonindex',
      language: 'query',
      views: {
        'item-json-index': {
          map: {
            fields: {
              item_id: 'asc',
              source: 'asc',
              timestamp: 'asc',
            },
            partial_filter_selector: {},
          },
          reduce: '_count',
          options: {
            def: {
              fields: ['item_id', 'source', 'timestamp'],
            },
          },
        },
      },
    },
    lastCasePositionView: {
      _id: '_design/allTimeStamps',
      views: {
        by_timestamp: {
          map: function (doc) {
            if (doc.timestamp) {
              emit(doc.timestamp, null);
            }
          },
        },
      },
    },
  },

  _security: {
    members: {
      roles: [],
      names: ['alice@example.com'],
    },
    admins: {
      roles: [],
      names: ['alice@example.com'],
    },
  },

  // Initial seed, could potentially be behind a flag
  'SHIP_SEEDEDSW3PPFS7BYHM3M2PRMB2_2020-06-10T12:11:18.000Z_12345XG': {
    _id: 'SHIP_SEEDEDSW3PPFS7BYHM3M2PRMB2_2020-06-10T12:11:18.000Z_12345XG',
    timestamp: '2020-06-10T12:11:18.000Z',
    item_id: 'SHIP_SEEDEDSW3PPFS7BYHM3M2PRMB2',
    time_created: '2020-06-10T12:11:18.000Z',
    time_modified: '2020-06-10T12:11:18.000Z',
    source: 'onefleet',
    soft_deleted: false,

    altitude: undefined,
    lat: 33.450787,
    lon: 12.798547,
    heading: 68,
    speed: 0,
  },
  'SHIP_SEEDEDEVERGIVENPRMB26ZQ03K_2021-03-23T20:14:47.928Z_01G9ZHGZKRVHFA2C2N7T05ABR3':
    {
      _id: 'SHIP_SEEDEDEVERGIVENPRMB26ZQ03K_2021-03-23T20:14:47.928Z_01G9ZHGZKRVHFA2C2N7T05ABR3',
      timestamp: '2021-03-23T20:14:47.928Z',
      item_id: 'SHIP_SEEDEDEVERGIVENPRMB26ZQ03K',
      time_created: '2022-08-08T20:14:47.928Z',
      time_modified: '2022-08-08T20:15:04.400Z',
      source: 'onefleet',
      soft_deleted: false,

      altitude: undefined,
      lat: 31.014271987494087,
      lon: 32.31179575435818,
      heading: 300,
      speed: 0,
    },
};

config[`${prefix}logs`] = {
  _design: {},

  _security: {
    members: {
      roles: [],
      names: ['alice@example.com'],
    },
    admins: {
      roles: [],
      names: ['alice@example.com'],
    },
  },
};

const url = `http://${process.env.COUCHDB_USER || 'admin'}:${
  process.env.COUCHDB_PASSWORD || 'admin'
}@${process.env.COUCHDB_HOST || 'localhost'}:${
  process.env.COUCHDB_PORT || 5984
}`;

couchdbBoostrap(url, config, {}, (error, response) => {
  if (error) {
    process.exitCode = 1;

    return console.error(error);
  }

  console.log(JSON.stringify(response, null, '  '));
});
