#
# download map tiles from osm like map services
# parameters for the server must be zoom, x, y
# set the directory DIR, the parallel downloads (4-25?) and the zoom levels
# tile number increases exponentionally with the zoom level (12 is a lot)
# start/end hold the coordinates in lat lon of the area of interest
#

import aiohttp
import asyncio
import math
import os

DIR = "."
PARALLEL_DOWNLOADS = 9

STARTZOOM = 1
ENDZOOM = 13 # exclusive

# download only if file does not exist
NO_OVERWRITE = True

# set here the latest x,y values from the output to continue
# an interrupted download
CONTINUE_X = False
CONTINUE_Y = False

URL = "https://basemaps.cartocdn.com/rastertiles/voyager/{zoom}/{x}/{y}.png"
# dark
#URL = "https://cartodb-basemaps-a.global.ssl.fastly.net/dark_all/{zoom}/{x}/{y}.png"
# base-eco, almost no labels
#URL = "https://cartocdn_a.global.ssl.fastly.net/base-eco/{zoom}/{x}/{y}.png"


# more or less the region of interest
start = {"lat": 49, "lon": -12}
end = {"lat": 29, "lon": 41}

def deg2num(lat_deg, lon_deg, zoom):
  # get osm x,y coordinates from lat, lon
  lat_rad = math.radians(lat_deg)
  n = 1 << zoom
  xtile = int((lon_deg + 180.0) / 360.0 * n)
  ytile = int((1.0 - math.asinh(math.tan(lat_rad)) / math.pi) / 2.0 * n)
  return xtile, ytile


def getxy(start, end, zoom):
  # helper func
  lefttop = deg2num(start["lat"], start["lon"], zoom)
  rightbottom = deg2num(end["lat"], end["lon"], zoom)
  return (lefttop, rightbottom)


async def task(zoom, x, y, session, target_file):

  async with session.get(URL.format(zoom=zoom, x=x, y=y)) as response:

    if (response.status != 200):
      print("Status:", response.status)
      print("Content-type:", response.headers['content-type'])

    png = await response.read()
    with open(target_file, "wb") as ff:
      ff.write(png)


async def download(startzoom, endzoom, dryrun, total=0):

  count = 0

  session = aiohttp.ClientSession()

  for zoom in range(startzoom, endzoom):
    try:
      os.mkdir(f"{DIR}/{str(zoom)}")
    except:
      pass

    lefttop, rightbutton = getxy(start, end, zoom)


    for x in range(CONTINUE_X or lefttop[0], rightbutton[0] + 1):

      try:
        os.mkdir(f"{DIR}/{str(zoom)}/{x}")
      except:
        pass

      tasks = []

      for y in range(CONTINUE_Y or lefttop[1], rightbutton[1] + 1):

        target_file = f"{DIR}/{str(zoom)}/{x}/{y}.png"

        count += 1
        if dryrun:
          continue

        else:
          if NO_OVERWRITE and os.path.isfile(target_file):
            continue

          tasks.append(task(zoom, x, y, session, target_file))

          if len(tasks) > PARALLEL_DOWNLOADS:
            print(f"{count}/{total} - x: {x}, y: {y}")
            await asyncio.gather(*tasks)
            await session.close()
            session = aiohttp.ClientSession()
            tasks = []

      await asyncio.gather(*tasks)
      await session.close()
      session = aiohttp.ClientSession()
      tasks = []

  print(count)
  await session.close()
  return count


async def main():
  startzoom = STARTZOOM
  endzoom = ENDZOOM
  total = await download(startzoom, endzoom, True)
  await download(startzoom, endzoom, False, total)

asyncio.run(main())
