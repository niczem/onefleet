/**
 * Interface to start other template converters.
 * Each template converter should have its own converter that implements this interface.
 */
export interface TemplateConverter {
  /**
   * Convert the input file to the output version of the templates that this class represents
   * @param inputItems The array of actual item objects to convert.
   * @returns
   */
  migrateItemsData(inputItems: CouchDBDocument[]): CouchDBDocument[];

  /**
   * Check the input file to the output version of the templates that this class represents
   * @param inputItems The array of actual item objects to check.
   * @returns anything, as it will be ignored outside of specific converters
   */
  validateItemsData(inputItems: CouchDBDocument[]): any;
}

export interface CouchDBDocument {
  /** Documents read from CouchDB should always contain an _id field. */
  _id: string;

  /** While not used here, we should still reserve the field _rev because all CouchDB documents contain it. */
  _rev?: string;
}
