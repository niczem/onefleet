/**
 * A command-line node script to convert old template items into new template items.
 */

import * as commander from 'commander';
import * as fs from 'fs';
import { LatestConverter } from './converters/latest';
import type { CouchDBDocument, TemplateConverter } from './types';

/**
 * Read the input file, parse as JSON, call the converter function,
 * and write back to converted json file.
 *
 * Input and output versions depend on the git tag of the upcoming release.
 */
function convert(
  inputFilename: string,
  outputFilename: string,
  checkOnly: boolean
): void {
  const rawdata = fs.readFileSync(inputFilename, { encoding: 'utf8' });
  const inputJson: CouchDBDocument[] = JSON.parse(rawdata).docs;
  const designDocs = inputJson.filter((doc) => doc._id.substring(0, 1) === '_');
  const inputDocs = inputJson.filter((doc) => doc._id.substring(0, 1) !== '_');

  const converter: TemplateConverter = new LatestConverter();
  if (checkOnly) {
    converter.validateItemsData(inputDocs);
    console.info('No output file written due to check-only mode. Bye.');
    process.exit(0);
  } else {
    const outputDocs: CouchDBDocument[] = converter.migrateItemsData(inputDocs);
    const data = { docs: [...outputDocs, ...designDocs] };
    fs.writeFileSync(outputFilename, JSON.stringify(data, null, 2));
  }
}

commander.program
  .option(
    '-i, --input <filename>',
    'input file as ready for couchdb push',
    'input_items.json'
  )
  .option(
    '-o, --output <filename>',
    'output file in json format',
    'output_items.json'
  )
  .option(
    '--check_only',
    'whther to skip conversion and only check the inputs',
    false
  );
commander.program.parse(process.argv);

console.log(
  `Converting file '${commander.program.opts().input}'`,
  `into '${commander.program.opts().output}'...`
);

convert(
  commander.program.opts().input,
  commander.program.opts().output,
  commander.program.opts().check_only
);

console.log('Done.');
