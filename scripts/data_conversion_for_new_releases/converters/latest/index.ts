import type { DbItem as DbItemLatest } from './types/db-item';
import type { TemplateConverter } from '../../types';
import type {
  CheckingResult,
  ResultSummary,
  DisallowedResult,
  MatchResult,
} from './types';
import templates from './templates';

import { convertBaseAttributes } from './convert/base_attributes';
import { convertProperties } from './convert/properties';
import { convertMapSettings } from './convert/map_settings';

import { matchBaseAttributes } from './match/base_attributes';
import { matchProperties } from './match/properties';
import { matchMapSettings } from './match/map_settings';

import { summariseResult } from './summary/summarize_result';
import { printSummary } from './summary/print_summary';

/**
 * Convert database items that were created or edited with a
 * pre-latest template to the latest template in the database.
 *
 * For example, if we have a previous deploy of onefleet template version
 * 0.3.x on the live server, and we are preparing to deploy version 0.4.x,
 * any existing items in the database that do not conform to the new templates
 * must be converted to conform to them. That's where this converter comes in!
 *
 * Converter version should be merged before tagging the final release tag for
 * a new release, but after testing the conversion with the a release candidate.
 * Then the git tag can be used to reconstruct old converters for old template
 * versions if one ever needs to re-run an old version of this converter.
 */
export class LatestConverter implements TemplateConverter {
  /**
   * Convert the input file to the output version of the templates that this class represents
   * @param inputItems The array of actual item objects to convert.
   * @returns
   */
  public migrateItemsData(inputItems: any[]): DbItemLatest[] {
    console.info('Total number of items: ', inputItems.length);

    const convertedItems = inputItems.map((item) => this.convertToLatest(item));
    this.validateItemsData(convertedItems);
    return convertedItems;
  }

  /**
   * Check the input file to the output version of the templates that this class represents
   * @param inputItems The array of actual item objects to check.
   * @returns
   */
  public validateItemsData(
    inputItems: any[]
  ): [CheckingResult[], ResultSummary] {
    const checkingResults = inputItems.map((item) => this.matchesLatest(item));
    const checkingSummary = checkingResults.reduce(summariseResult, null);
    printSummary(checkingSummary);

    return [checkingResults, checkingSummary];
  }

  /**
   * Do the actual conversion using the .map() function of typescript arrays.
   */
  private convertToLatest(input_item: any): DbItemLatest {
    let item = convertBaseAttributes(input_item);
    item = convertMapSettings(item);
    item = convertProperties(item);
    return item;
  }

  private matchesLatest(
    convertedItem: DbItemLatest
  ): DisallowedResult | MatchResult {
    // guard statement if this item does not have a valid template:
    if (!templates.get(convertedItem.template)) {
      return {
        item: convertedItem,
        disallowedTemplate: `${convertedItem.template}`,
      };
    }

    return {
      item: convertedItem,
      baseAttributeValidity: matchBaseAttributes(convertedItem),
      mapSettingsValidity: matchMapSettings(convertedItem),
      propertiesValidity: matchProperties(convertedItem),
    };
  }
}
