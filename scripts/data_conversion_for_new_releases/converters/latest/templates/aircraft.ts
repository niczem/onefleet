import type { ItemTemplate } from '../types/item-template-schema';

export const aircraft: ItemTemplate = {
  plural: 'Aircraft',
  pouch_identifier: 'AIRCRAFT',
  enforce_initial_position: false,
  default_tracking_type: 'IRIDIUMGO',
  allow_creation_via_ui: true,
  type: 'line',
  default_marker_icon: 'aircraft',
  always_show_markers: false,
  fields: [
    {
      name: 'name',
      title: 'Aircraft Name',
      info: 'Name of the Aircraft',
      field_group: 'Important',
      type: 'text',
    },
    {
      name: 'category',
      title: 'Affiliation Category',
      info: 'Category of the Aircraft',
      field_group: 'Important',
      type: 'select',
      options: {
        civilfleet: 'Civilfleet',
        import: 'Import',
        commercial: 'Commercial',
        military: 'Military',
        unknown: 'Still Unknown',
        other: 'Other',
      },
      default_value: 'unknown',
    },
    {
      name: 'active',
      title: 'Active',
      type: 'select',
      info: 'Status',
      options: {
        true: 'true',
        false: 'false',
      },
      default_value: 'true',
    },
  ],
};
