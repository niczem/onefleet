import type { DbItem as DbItemV03, DbItemProperty } from '../types/db-item';
import templateHelper from '../templateHelper';
import type { ItemTemplateField } from '../types/item-template-schema';
import type { FieldMatchOutcome, PropertyMatchResult } from '../types';

export function matchProperties(convertedItem: DbItemV03): PropertyMatchResult {
  return {
    ...matchPropertyNames(convertedItem),
    ...matchPropertyTypes(convertedItem),
  };
}

/**
 * Check existence of propertyNames
 * @returns Part of a PropertyMatchResult, but let's not call it a `Partial<PropertyMatchResult>`
 *   because that will confuse typing in matchProperties() and we know what we are doing.
 */
function matchPropertyNames(convertedItem: DbItemV03): any {
  // prepare propertyNames:
  const res: Partial<PropertyMatchResult> = {
    propertyNames: {
      missing: [],
      recognised: [],
      superfluous: [],
    },
  };

  // recognised or superfluous propertyNames:
  for (const existing_property_key of Object.keys(convertedItem.properties)) {
    if (
      templateHelper.necessaryProperties[convertedItem.template].includes(
        existing_property_key
      ) ||
      templateHelper.optionalProperties[convertedItem.template].includes(
        existing_property_key
      )
    ) {
      res.propertyNames.recognised.push(existing_property_key);
    } else {
      res.propertyNames.superfluous.push(existing_property_key);
    }
  }
  // missing propertyNames:
  for (const expectedPropertyKey of [
    ...templateHelper.necessaryProperties[convertedItem.template],
    ...templateHelper.optionalProperties[convertedItem.template],
  ]) {
    if (!Object.keys(convertedItem.properties).includes(expectedPropertyKey)) {
      res.propertyNames.missing.push(expectedPropertyKey);
    }
  }
  return { propertyNames: res.propertyNames };
}

/**
 * Check propertyTypes
 * @returns Part of a PropertyMatchResult, but let's not call it a `Partial<PropertyMatchResult>`
 *   because that will confuse typing in matchProperties() and we know what we are doing.
 */
function matchPropertyTypes(convertedItem: DbItemV03): any {
  const res: Partial<PropertyMatchResult> = {
    propertyTypes: {},
    propertyTypeMatches: {},
    propertyTypeMatchExamples: {}, // name, match, examples
  };

  for (const [propName, propValue] of Object.entries(
    convertedItem.properties
  )) {
    const field =
      templateHelper.templateFieldSpecifications[convertedItem.template][
        propName
      ];
    if (field) {
      // field might be undefined if property is superfluous
      res.propertyTypes[field.name] = field.type;
      const fieldMatchOutcome: FieldMatchOutcome = matchesFieldType(
        propValue,
        field
      )
        ? 'matches_type'
        : 'no_match';

      res.propertyTypeMatches[field.name] = fieldMatchOutcome;

      const mismatch_examples: Record<FieldMatchOutcome, DbItemProperty[]> = res
        .propertyTypeMatchExamples[field.name] ?? {
        matches_type: [],
        no_match: [],
      };
      mismatch_examples[fieldMatchOutcome].push(propValue);
      res.propertyTypeMatchExamples[field.name] = mismatch_examples;
    }
  }
  return {
    propertyTypes: res.propertyTypes,
    propertyTypeMatches: res.propertyTypeMatches,
    propertyTypeMatchExamples: res.propertyTypeMatchExamples,
  };
}

function matchesFieldType(
  propValue: DbItemProperty,
  field: ItemTemplateField
): boolean {
  switch (field.type) {
    case 'text':
      return typeof propValue === 'string' && !propValue.includes('\n');

    case 'textarea':
    case 'inputtable':
      return typeof propValue === 'string';

    case 'number':
      return typeof propValue === 'number';

    case 'datetime':
      if (propValue == null) {
        return true;
      }
      try {
        const parsedValue = new Date(`${propValue}`).toISOString();
        return parsedValue === propValue;
      } catch (e) {
        return false;
      }

    case 'datetimemulti':
      if (propValue == null) {
        return true;
      }
      try {
        let all_iso = true;
        for (const singleDate of String(propValue ?? '').split(';')) {
          // throws error if not parseable:
          const parsedValue = new Date(singleDate).toISOString();
          // even if parseable, the result may be misleading. We want ISO!
          all_iso = all_iso && parsedValue === singleDate;
        }
        return all_iso;
      } catch (e) {
        // we have no match if values not parseable
        return false;
      }

    case 'select':
      return Object.keys(field.options ?? {}).includes(`${propValue}`);
    default:
      throw new Error(
        `No match for the field type ${field.type} has been implemented yet! Please implement!`
      );
  }
}
