import templates from './templates';
import type { ItemTemplateField } from './types/item-template-schema';

class TemplateHelper {
  /** Necessary properties per template, having `required_for_creation == true` */
  necessaryProperties: Record<string, string[]> = {};

  /** Optional properties per template, having `required_for_creation != true` */
  optionalProperties: Record<string, string[]> = {};

  /** All template fields per template, indexed by field name */
  templateFieldSpecifications: Record<
    string,
    Record<string, ItemTemplateField>
  > = {};

  constructor() {
    // find necessary and optional Properties for each template:
    for (const [key, template] of Object.entries(templates.getAll())) {
      // find necessary Property names:
      this.necessaryProperties[key] = template.fields
        .filter((field) => field.required_for_creation ?? false)
        .map((field: ItemTemplateField) => field.name);

      // find optional Property names:
      this.optionalProperties[key] = template.fields
        .filter((field) => !(field.required_for_creation ?? false))
        .map((field: ItemTemplateField) => field.name);

      // make template fields addressable by name:
      this.templateFieldSpecifications[key] = {};
      for (const field of template.fields) {
        this.templateFieldSpecifications[key][field.name] = field;
      }
    }
  }
}

export default new TemplateHelper();
