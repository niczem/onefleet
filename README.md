# OneFleet - A Real-time Geographic Information System for Efficient Rescue Coordination

**Table of Contents:**

- [Technical Overview](#technical-overview)
- [Installation Guide](#installation-guide)
- [Contributing Guide](#contributing-guide)
- [Credits](#credits)

## Technical Overview

OneFleet is a real-time geographic information system designed to track vehicles and streamline rescue missions. It uses a combination of [PouchDB](https://pouchdb.com/) and [CouchDB](https://couchdb.apache.org/) to provide offline-first capabilities. Each client synchronizes data from the central CouchDB to their in-browser PouchDB. Live data is accessible, while historical data is archived separately.

<div class="center">

[![](https://mermaid.ink/img/pako:eNqtkrFOwzAQhl_l5AlEO9BuEWKgGRhADF2zXO0LserYkX0pQlXenUuaIkORGMCDbf3333dn-Y5KB0OqUNphSqXF14ht5QFgEuDRJg7RanQlMu4wERyn8LgqSfaM1idoPn1gxFipzHTlA0OHkSHUwOKESF24nj3DtP9Q5265hE3odVM-ZA3NyrmJrAVnD5QXP4Fn_0i7h42z5Pk2x52U_E2xF9iLp9oRMVgPuxjeEsXfsKsL7Oo_sOsL7Ppv2KegkW3wW4oHqynDf4ucy9ygfFnuFqlF675og1qolqLoRqZpSq0UN9RSpQq5Goz7safRhz2H7bvXquDY00L1nXwbzcOnihpdEpWMlZF4nsdzPIYPQVTdXg?type=png)](https://mermaid.live/edit#pako:eNqtkrFOwzAQhl_l5AlEO9BuEWKgGRhADF2zXO0LserYkX0pQlXenUuaIkORGMCDbf3333dn-Y5KB0OqUNphSqXF14ht5QFgEuDRJg7RanQlMu4wERyn8LgqSfaM1idoPn1gxFipzHTlA0OHkSHUwOKESF24nj3DtP9Q5265hE3odVM-ZA3NyrmJrAVnD5QXP4Fn_0i7h42z5Pk2x52U_E2xF9iLp9oRMVgPuxjeEsXfsKsL7Oo_sOsL7Ppv2KegkW3wW4oHqynDf4ucy9ygfFnuFqlF675og1qolqLoRqZpSq0UN9RSpQq5Goz7safRhz2H7bvXquDY00L1nXwbzcOnihpdEpWMlZF4nsdzPIYPQVTdXg)

</div>

**Key Components:**

- **Historical Database:** Contains historical data (not part of this repository).
- **CouchDB:** Stores live data and synchronizes with clients.
- **Clients (Client1, Client2, Client3):** Run OneFleet in the browser.
- **Location Service:** Provides AIS and mail services for real-time location data.

## Technical Development

Our development plan includes the following steps:

- Utilizing VueJS as a UI framework for easy prototyping.
- Developing offline-first functionality for use on ships and airplanes.
- Synchronizing data via CouchDB and PouchDB.
- Incorporating Leaflet for map visualization.
- Allowing operative organizations to add data during missions.
- Providing accessibility for journalists and research/legal teams.
- Enhancing documentation for human rights violations and fostering cooperation among organizations.
- For a detailed list of requested functions and features, please refer to our backlog.

## About this Repository

This repository contains all the services required to track and display vehicles and positions. It includes the main application for displaying vehicles and cases on a map, the database for storing locations, vehicles, and positions, and the location service for collecting ship locations from external services. The database must be running when you start the app, while the location service operates independently.

## Installation Guide

## Quickstart with docker-compose

```bash
git clone https://gitlab.com/civilmrcc/onefleet.git
cd onefleet
cp .env.template .env
docker-compose up app
```

## 1. Clone the repository

```bash
git clone https://gitlab.com/civilmrcc/onefleet.git
```

## 2. Copy .env.template to .env

```bash
cd OneFleet
cp .env.template .env
```

## 3. Install the software and dependencies

### Setup Local Instance

To get started, clone the repository and install the necessary dependencies:

```bash
npm install
```

### Local Development Server with Docker-Compose

Requirements: [docker-compose](https://docs.docker.com/compose/install/)

```bash
docker-compose --profile dev up
```

This command starts containers for the frontend (served by the `vite` command), the location service, the all_ships service, and the database. Afterward, you can access the client at [http://localhost:5173/](http://localhost:5173/) with login credentials `alice@example.com`/`secure` or `admin`/`admin`. The database web interface is available at [http://localhost:5984/\_utils](http://localhost:5984/_utils) using the same credentials.

To serve the built project locally, use the following command:

```bash
docker-compose -f docker-compose.profiles.yml --profile preview up
```

You can then access the built project at [http://localhost:4173/](http://localhost:4173/) using the same credentials again.

### Docker Container Sensitive Environment Values

The docker container for the location service needs to be provided with some sensitive credentials (Api-keys, database login, IMAP login, etc.) to operate normally. These credentials can be entered in the file `.env`. If this file does not yet exist, it can be copied from `.env.template`. This should happen automatically as a post-install step when running `npm install`.

### Manage Docker Containers

[Portainer](https://www.portainer.io/) is a simple UI tool to manage Docker containers and images. It has the same functionality as the Docker CLI tool but is much more convenient to use. You can find the instructions on how to set up Portainer Community Edition in their documentation [here](https://docs.portainer.io/start/install-ce/server/docker).

## Contributing Guide

We would love for you to contribute to OneFleet and help make it even better than it is today and #SafePassage and lives! As a contributor, here are the guidelines we would like you to follow:

- [How to contribute](./CONTRIBUTING.md)
- [Installation Guide](#installation-guide)
- [Code of Conduct](./CODE_OF_CONDUCT.md)
- [Issue Reporting Guidelines](#issue-reporting-guidelines)
- [Pull Request Guidelines](#pull-request-guidelines)
- [Code Review Guidelines](#code-review-guidelines)
- [Documentation Guidelines](#documentation-guidelines)

### Issue Reporting Guidelines

The issue list is reserved exclusively for bug reports and feature requests, not for usage questions. Please use the discord channel for that. Please report all issues in the [issue page](https://gitlab.com/civilmrcc/onefleet/issues). There make sure that the bug or feature was not already reported, or please link duplicated cards together. Write detailed information because it is very helpful to understand an issue. For example:

- How to reproduce the issue, step-by-step.
- The expected behavior.
- What is actually happening (or what is wrong)?
- The operating system.
- Screenshots always help a lot.

### Merge Request Guidelines

The main branch is just a snapshot of the latest stable release. All development should be done in dedicated branches. Do not submit (Merge Requests) MRs against the main branch. Checkout a topic branch from the develop branch and merge on gitlab. Overview of Branches:

- main (release branch)
- develop (latest development branch)

**Naming of topic branch**
`[ISSUENUMBER]-[FEATURENAME]`
example feature branch:
`19-left_navigation_update_on_change`
Explanation: If the issue number is added in the beginning follow by a hyphen it automatically appears in the corresponding gitlab issue. This makes it easier to track branches.

- assign yourself
- select as reviewers only the people that definitely need to approve the MR
  - for example one reviewer that checks for the usability and another reviewer for the technical aspects, however usually it is enough to just have one dev reviewer look at both and only require a separate UX-reviewer for major UX changes (e.g. [!460](https://gitlab.com/civilmrcc/onefleet/-/merge_requests/460))
  - please do not assign a number of devs in order to inform them, you can just tag them in a comment in the MR instead
  - other people are welcome to additionally review the MR too
- set the labels on your MR
- connect your MR to the respective issues
- If your MR is a draft / WIP, write this in the beginning of the MR's name: Draft: name-of-MR
- If your MR is ready for review, write this in the beginning of the MR's name: [ready for review]name-of-MR
- an issue needs two approvals to be merged
- others are allowed to merge any MR if approved twice
  &rarr; if you want to be the one merging the MR (e.g. because you want to read the feedback before merging or other reasons), set the label: "do not merge"

### Testing Guidelines

Writing tests is strongly encouraged. The

Vitest UI makes it possible to see the results of the tests in a browser window. It runs Vitest in watch mode and further enables rerunning specific tests at a click of a button. In order to do that, navigate to the root folder and execute `npm test:unit:frontend:ui` or `npm test:unit:location:ui` respectively.

### Code Review Guidelines

For every MR one is assigned as a reviewer, one needs to do a proper review and potentially approve (or close) it. When reviewing a merge request it is asked to mark the comments priority. We chose to use emojis to represent the following:
:apple: : "must be fixed"
:orange_book:: "personal taste, but should probably be fixed"
:green_apple: : "style questions, white spaces, etc"
Gitlab text replacements for these emojis:
:apple: `:apple:`
:orange_book: `:orange_book:`
:green_apple: `:green_apple:`

### Documentation Guidelines

#### Documenting Architecture Decisions

Architecture decisions are documented in `doc/decisions` according to [this](https://cognitect.com/blog/2011/11/15/documenting-architecture-decisions) blogpost. Please read more about ADRs [here](https://github.com/joelparkerhenderson/architecture-decision-record). Please make sure the discussion leading to the ADR is documented well and a link to the issue is provided.

## Credits

Special thanks to everyone who already contributed to the project!

Funded by
![Prototype Fund](https://prototypefund.de/wp-content/uploads/2020/05/prototypefund-social-2020.png)

```

```
