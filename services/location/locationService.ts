import type { NewPosition } from './types';

import MailService from './mailService';
import DBInstance from './DBInstances';

//see datasources/index.ts
import * as datasources from './datasources';

const config_path: string = './config';
const config = await import(`${config_path}`);

export default class LocationService {
  dbConfig: any;
  itemDB: any;
  locationsDB: any;

  DBInstance: DBInstance; //db instance manages the DB setup logic and save logic on its own
  mailService: MailService; //mailservice will read emails and update positions accordingly
  constructor() {
    this.DBInstance = DBInstance.getInstance();
    this.mailService = MailService.getInstance();
    //test config
    if (
      !config.dbConfig.dbUser ||
      !config.dbConfig.dbPassword ||
      !config.dbConfig.dbUrl ||
      !config.dbConfig.dbPrefix
    ) {
      throw `${new Date().toISOString()}: config is missing some values`;
    }
  }

  /** fetches all current locations for all vehicles */
  private async fetchVehicleLocations() {
    try {
      await this.DBInstance.initDBs();
      const items = await this.DBInstance.getItems('');
      items
        .filter(
          (item) =>
            !item.doc._id.startsWith('_') &&
            (item.doc.map_settings?.tracking_type ??
              item.doc.properties.tracking_type) != 'MANUAL' &&
            item.doc.soft_deleted == (false || null || undefined)
        )
        .forEach(async (item) => {
          const position = await this.fetchVehicleLocation(item);
          await this.DBInstance.insertLocation(item.doc._id, position);
        });
    } catch (error) {
      console.error(`${new Date().toISOString()}: `, error);
    }
  }

  public async fetchVehicleLocationsInterval(interval_in_minutes) {
    //run on startup at function
    await this.fetchVehicleLocations();
    setInterval(async () => {
      await this.fetchVehicleLocations();
    }, interval_in_minutes * 1000 * 60);
  }

  /** fetches location for a single Vehicle */
  private async fetchVehicleLocation(item): Promise<NewPosition | null> {
    try {
      const tracking_type =
        item.doc.map_settings?.tracking_type ??
        item.doc.properties.tracking_type;
      const sources = this.getDatasourceCandidates(tracking_type);
      return await this.queryDatasources(item, config.locationConfig, sources);
    } catch (error) {
      console.log(
        `${new Date().toISOString()}: Uncaught error in "fetchVehicleLocation": `,
        error
      );
      return null;
    }
  }

  /**
   * For a given tracking type, return the datasources that can return data for this tracking type.
   * @param tracking_type A string such as "AIS", "ADSB", or similar.
   * @returns An array of datasources that each have a getPosition() function.
   */
  private getDatasourceCandidates(tracking_type: string): any[] {
    // filter all datasources down to those that start with the given tracking type
    const candidate_keys = Object.keys(datasources).filter((ds_key) =>
      ds_key.startsWith(tracking_type)
    );
    const sources: string[] = [];
    for (const ds_key of candidate_keys) {
      sources.push(datasources[ds_key]);
    }

    //order candidate datasources by priority
    sources.sort((a: any, b: any) => (a.priority > b.priority ? 1 : -1));
    return sources;
  }

  /**
   * Calls each given datasource's query function for finding the current position of the given item
   * @param filtered_sorted_datasources The array of datasources to try until we have a good position
   * @returns A valid up-to-date position, if found. Or null otherwise.
   */
  private async queryDatasources(
    item: { doc: any },
    config: any,
    filtered_sorted_datasources: any[]
  ): Promise<NewPosition | null> {
    const suboptimal_positions: NewPosition[] = [];

    // iterate through the given datasources and break the loop if a good position is found
    for (const datasource of filtered_sorted_datasources) {
      try {
        const position = await datasource.getPosition(item, config);
        // break loop if position is fine
        if (this.DBInstance.positionIsFine(position)) {
          // if the position is recent enough, just return it directly
          return position;
        } else if (position) {
          suboptimal_positions.push(position);
        }
      } catch (e: any) {
        console.log(
          `${new Date().toISOString()}: Datasource '${
            datasource.source_title
          }':`,
          e.message ?? e,
          `(ItemID: ${item?.doc?._id}, Name: ${item?.doc?.properties?.name})`
        );
      }
    }

    // if no positions were found, just return null
    if (suboptimal_positions.length == 0) {
      return null;
    }
    // sort suboptimal positions by timestamp: youngest first.
    suboptimal_positions.sort(
      (a, b) =>
        new Date(b.timestamp).getTime() - new Date(a.timestamp).getTime()
    );
    // as no perfect position was found, just return the newest imperfect position
    return suboptimal_positions[0];
  }

  //sets it all in motion
  public async run(): Promise<void> {
    await this.DBInstance.initDBs();
    this.mailService.initMail();

    if (config.locationConfig.timerInterval) {
      this.fetchVehicleLocationsInterval(config.locationConfig.timerInterval);
    } else if (config.locationConfig.locationsOnce) {
      this.fetchVehicleLocations();
    } else if (config.locationConfig.deleteOlderThan) {
    } else {
      console.error(
        `${new Date().toISOString()}: No run mode was selected. Please select a run mode:`
      );
      process.exit(2);
    }
  }
}
