import type { DbPosition, NewPosition } from './types';
import pouchDB from 'pouchdb';
import ulid from 'ulid';
import Nano from 'nano';

import * as dotenv from 'dotenv';
dotenv.config();

const config_path: string = './config';
const config = await import(`${config_path}`);

export default class DBInstance {
  dbConfig: any;
  itemDB: any; // the DB connection for items
  locationsDB: any; // the DB connection for locations / positions
  private static instance: DBInstance;

  public static getInstance(): DBInstance {
    if (!DBInstance.instance) {
      DBInstance.instance = new DBInstance();
    }
    return DBInstance.instance;
  }

  private getConfig() {
    return process.env.DEVELOPMENT
      ? {}
      : {
          auth: {
            username: config.dbConfig.dbUser,
            password: config.dbConfig.dbPassword,
          },
        };
  }
  public initDBs() {
    return new Promise(async (resolve, reject) => {
      this.dbConfig = this.getConfig();

      //this.dbConfig.skip_setup = true;
      const username = config.dbConfig.dbUser;
      const userpass = config.dbConfig.dbPassword;

      const nano = Nano({
        url: `${config.dbConfig.dbUrl}`,
        requestDefaults: {
          jar: true,
        },
      });
      // authenticate
      await nano.auth(username, userpass);

      this.itemDB = new pouchDB(
        `${config.dbConfig.dbUrl}/${config.dbConfig.dbPrefix}items`,
        this.dbConfig
      );
      this.locationsDB = new pouchDB(
        `${config.dbConfig.dbUrl}/${config.dbConfig.dbPrefix}positions`,
        this.dbConfig
      );

      resolve(true);
    });
  }

  public getItems(id) {
    return new Promise<Array<any>>((resolve, reject) => {
      const items = this.itemDB;
      items
        .allDocs({
          include_docs: true,
          attachments: true,
          startkey: id,
        })
        .then(function (result) {
          if (result.error) reject(result.error);
          else resolve(result.rows);
          // handle result
        })
        .catch(function (err) {
          reject(err);
        });
    });
  }
  public insertItem(obj, cb) {
    const itemDB = this.itemDB;
    itemDB
      .put(obj)
      .then(function (response) {
        cb(null, response);
      })
      .catch(function (err) {
        cb(err);
      });
  }

  /**
   * Inserts a position into the database that was freshly received from one of the datasources.
   * @param item_id The id of the item that this new position refers to.
   * @param position The new position object
   */
  async insertLocation(
    item_id: string,
    position: NewPosition | null
  ): Promise<void> {
    if (!position) {
      console.log(`Error: No position to insert for ${item_id}`);
      return;
    }
    try {
      const iso_timestamp = new Date(position.timestamp).toISOString();
      const entry: DbPosition = {
        ...position,
        _id: `${item_id}_${iso_timestamp}_${ulid.ulid()}`.toUpperCase(),
        item_id: item_id,
        source: position.source || 'unknown_datasource',
        timestamp: iso_timestamp,
      };

      this.locationsDB
        .put(entry)
        .then(function (response) {
          console.log('location created');
          console.log(entry);
        })
        .catch(function (err) {
          console.log(entry);
          console.log(err);
        });
    } catch (error) {
      console.log('Error inserting position for ' + item_id, error);
    }
  }

  /**
   * Rules that define when a received position is so good that we don't need to look at any further datasources.
   * @param position The position to check
   * @returns Whether the position is fine or not.
   */
  public positionIsFine(position: NewPosition): boolean {
    const perfectPositionAge = 30 * 60 * 1000; // 15 minutes old
    return (
      new Date().getTime() - new Date(position.timestamp).getTime() <
      perfectPositionAge
    );
  }
}
