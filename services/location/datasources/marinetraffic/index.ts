import axios from 'axios';

export const source_title = 'marinetraffic';
export const priority = 10;
export async function getPosition(item, nconfig) {
  const doc = item.doc;
  const mmsi_string = doc.map_settings?.tracking_id ?? doc.properties.MMSI;
  const mmsi = parseInt(mmsi_string, 10);
  if (!nconfig.marine_traffic_exportvessel_api_key) {
    throw `${new Date().toISOString()}: No api key for marineraffic provided`;
  }

  // API Request format version 5, but here's the online documentation for API version 3 at least:
  // https://web.archive.org/web/20230603144050/https://www.marinetraffic.com/en/ais-api-services/documentation/#info29
  // the parameter "protocol:json" gives us a "Simplified JSON" array of arrays:
  const url = `https://services.marinetraffic.com/api/exportvessel/v:5/${nconfig.marine_traffic_exportvessel_api_key}/timespan:720/mmsi:${mmsi}/protocol:json`;
  const response = await axios.get(url).catch((error) => {
    const msg = error.message ? error.message : JSON.stringify(error);
    throw `${new Date().toISOString()}: Error fetching position from marinetraffic: ${msg} mmsi: ${mmsi} name: ${
      doc.properties.name
    }`;
  });

  // throw an error if marinetraffic returned any errors, such as e.g. running out of credit:
  if (response.data.errors) {
    throw `${new Date().toISOString()}: Marinetraffic is complaining: ${JSON.stringify(
      response.data.errors
    )}`;
  }

  // datasource-specific format for latest position:
  // API response format (Version 3 instead of 5, but looks similar):
  // https://www.marinetraffic.com/en/ais-api-services/documentation#response29
  // https://www.marinetraffic.com/en/ais-api-services/documentation#usage29
  // Simplified JSON sample response [simple]
  const latest_mt_position = response.data[0];
  if (latest_mt_position) {
    return {
      // mmsi: latest_mt_position[0],
      lat: latest_mt_position[1],
      lon: latest_mt_position[2],
      speed: latest_mt_position[3],
      heading: latest_mt_position[4],
      course: latest_mt_position[5],
      // status: latest_mt_position[6],
      timestamp: latest_mt_position[7],
      source: 'marinetraffic',
    };
  } else {
    throw `${new Date().toISOString()}: No position in marinetraffic request`;
  }
}
