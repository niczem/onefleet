import * as AIS_marinetraffic from './index';

describe('datasource: marinetraffic', () => {
  describe('source_title', () => {
    it('should have the correct source title', () => {
      expect(AIS_marinetraffic.source_title).toBe('marinetraffic');
    });
  });

  // describe('source_title', () => {
  //   it('should have the correct source title', () => {
  //     expect(false).toBe('marinetraffic'); // fail until implemented
  //   });
  // });
  // describe('priority', () => {
  //   it('should have the agreed-upon priority', () => {
  //     expect(false).toBe(10); // fail until implemented
  //   });
  // });
  // describe('.getPosition()', () => {
  //   it('should return a position if the datasource is reachable', () => {
  //     expect(false).toBe(true); // fail until implemented
  //   });
  //   it('should throw a descriptive error message if the datasource is not reachable', () => {
  //     expect(false).toBe(true); // fail until implemented
  //   });
  //   it('should throw a descriptive error message if the datasource returns data in an unexpected format', () => {
  //     expect(false).toBe(true); // fail until implemented
  //   });
  // });
});
