import axios from 'axios';

export const source_title = 'ais-scraper';
export const priority = 5;
export async function getPosition(item, nconfig) {
  const doc = item.doc;
  const mmsi_string = doc.map_settings?.tracking_id ?? doc.properties.MMSI;
  const mmsi = parseInt(mmsi_string, 10);

  const url = `${nconfig.aisUrl}/getLastPosition/${mmsi}`;
  const response = await axios.get(url).catch((error) => {
    const msg = error.message ? error.message : JSON.stringify(error);
    throw `${new Date().toISOString()} error fetching position from ais-scraper: ${msg} mmsi: ${mmsi} name: ${
      doc.properties.name
    }`;
  });

  if (response.data?.data?.longitude) {
    // datasource-specific format for latest position:
    const latest_scraper_position = response.data.data;
    return {
      // mmsi: mmsi,
      lat: latest_scraper_position.latitude,
      lon: latest_scraper_position.longitude,
      course: latest_scraper_position.course,
      timestamp: latest_scraper_position.timestamp,
      source: 'ais-scraper',
    };
  } else {
    throw `${new Date().toISOString()}: no data in ais request for mmsi: ${mmsi} -> name: ${
      doc.properties.name
    }`;
  }
}
