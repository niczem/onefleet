import * as AIS_fleetmon from './index';
import axios from 'axios';

vi.mock('axios', () => {
  return {
    default: {
      get: vi.fn(),
    },
  };
});

describe('datasource: fleetmon', () => {
  describe('source_title', () => {
    it('should have the correct source title', () => {
      expect(AIS_fleetmon.source_title).toBe('fleetmon');
    });
  });

  describe('priority', () => {
    it('should have the agreed-upon priority', () => {
      expect(AIS_fleetmon.priority).toBe(20);
    });
  });

  describe('.getPosition()', () => {
    const mockAxios = vitest.mocked(axios.get);

    /** Mock axios library and return axios-specific responses */
    function mockmultiResponses(mocked_fleetmon_responses) {
      mockAxios.mockImplementation((url) => {
        const not_found = {
          status: 404,
          statusText: 'Not Found (mocked)',
          data: null,
        };
        return mocked_fleetmon_responses[url] ?? not_found;
      });
    }
    /** Mock axios library and always return the same response */
    function mockSingleResponse(mock_response) {
      mockAxios.mockImplementation(() => {
        return mock_response;
      });
    }

    async function callGetterWithCatch(input_item, input_nconfig) {
      let thrownError;
      try {
        await AIS_fleetmon.getPosition(input_item, input_nconfig);
      } catch (error) {
        thrownError = error;
      }
      return thrownError;
    }

    afterEach(() => {
      vi.clearAllMocks();
    });
    afterAll(() => {
      vi.resetAllMocks();
    });

    it('should return a position via tracking_id if the datasource is reachable', async () => {
      // Setup:
      mockmultiResponses({
        'https://apiv2.fleetmon.com/vesselsearch/?mmsi_number=555555&max_pos_age_days=2':
          {
            status: 200,
            statusText: 'OK',
            data: { vessels: [{ vessel_id: 4444444, name: 'SW3' }] },
          },
        'https://apiv2.fleetmon.com/vessel/4444444/position?request_limit_info=true':
          {
            status: 200,
            statusText: 'OK',
            data: {
              latitude: 43.3,
              longitude: 5.8,
              received: '2022-06-20T11:49:26+00:0',
              request_limit_info: { max_requests: 1, left_requests: 1 },
            },
          },
      });
      const input_item = {
        doc: {
          properties: { name: 'SW3' },
          map_settings: { tracking_id: '555555' },
        },
      };
      const input_nconfig = { fleetmon_api_key: 'abcdefabcdefabcdef' };
      const expected_returned_data = {
        lat: 43.3,
        lon: 5.8,
        timestamp: '2022-06-20T11:49:26+00:0',
        source: 'fleetmon',
      };
      // Call the datasource's getPosition() function
      const actual_returned_data = await AIS_fleetmon.getPosition(
        input_item,
        input_nconfig
      );
      // Assertion:
      expect(actual_returned_data).toStrictEqual(expected_returned_data);
    });

    it('should return a position via cached tracking_token if the datasource is reachable', async () => {
      // Setup:
      mockmultiResponses({
        'https://apiv2.fleetmon.com/vesselsearch/?mmsi_number=555555': {
          status: 200,
          statusText: 'OK',
          data: { vessels: [{ vessel_id: 4444444, name: 'SW3' }] },
        },
        'https://apiv2.fleetmon.com/vessel/4444444/position?request_limit_info=true':
          {
            status: 200,
            statusText: 'OK',
            data: {
              latitude: 43.3,
              longitude: 5.8,
              received: '2022-06-20T11:49:26+00:0',
              request_limit_info: { max_requests: 1, left_requests: 1 },
            },
          },
      });
      const input_item = {
        doc: {
          properties: { name: 'SW3' },
          map_settings: { tracking_id: 'bla', tracking_token: '4444444' },
        },
      };
      const input_nconfig = { fleetmon_api_key: 'abcdefabcdefabcdef' };
      const expected_returned_data = {
        lat: 43.3,
        lon: 5.8,
        timestamp: '2022-06-20T11:49:26+00:0',
        source: 'fleetmon',
      };
      // Call the datasource's getPosition() function
      const actual_returned_data = await AIS_fleetmon.getPosition(
        input_item,
        input_nconfig
      );
      // Assertion:
      expect(actual_returned_data).toStrictEqual(expected_returned_data);
    });

    it('should throw a descriptive error message if the datasource is not reachable', async () => {
      // Setup:
      const input_item = {
        doc: {
          map_settings: { tracking_id: '555555' },
          properties: { name: 'SW3' },
        },
      };
      const input_nconfig = { fleetmon_api_key: 'abcdefabcdefabcdef' };
      mockSingleResponse({
        status: 404,
        statusText: 'Not Found (mocked)',
      });

      // Call:
      const thrownError = await callGetterWithCatch(input_item, input_nconfig);
      // Assertion:
      const regex =
        /Bad HTTP status while searching for fleetmon vessel\. status: (\d+), statusText: (.+)/;
      expect(regex.test(thrownError.message)).toBeTruthy();
    });

    // TODO:

    it('should throw a descriptive error message if no api key was provided', async () => {
      // Setup:
      const input_item = { doc: { map_settings: { tracking_id: '555555' } } };
      const input_nconfig = { fleetmon_api_key: undefined };
      mockSingleResponse({});

      // Call:
      const thrownError = await callGetterWithCatch(input_item, input_nconfig);

      // Assertion:
      expect(thrownError.message).toMatch(/No api key for fleetmon provided/);
    });

    it('should throw a descriptive error message if the datasource does not accept the given API key', async () => {
      // Setup:
      const input_item = {
        doc: {
          map_settings: { tracking_id: '555555' },
          properties: { name: 'SW3' },
        },
      };
      const input_nconfig = { fleetmon_api_key: 'wrong_api_key' };
      mockSingleResponse({
        status: 401,
        statusText: 'UNAUTHORIZED',
      });

      // Call:
      const thrownError = await callGetterWithCatch(input_item, input_nconfig);
      // Assertion:
      const regex =
        /Bad HTTP status while searching for fleetmon vessel\. status: (\d+), statusText: (.+)/;
      expect(regex.test(thrownError.message)).toBeTruthy();
    });

    it('should throw a descriptive error message if the tracking_id contains no digits', async () => {
      // Setup:
      const input_item = {
        doc: {
          map_settings: { tracking_id: 'alphabet' },
          properties: { name: 'SW3' },
        },
      };
      const input_nconfig = { fleetmon_api_key: 'abcdefabcdefabcdef' };
      mockSingleResponse({
        status: 422,
        statusText: 'UNPROCESSABLE ENTITY',
      });

      // Call:
      const thrownError = await callGetterWithCatch(input_item, input_nconfig);
      // Assertion:
      const regex =
        /Bad HTTP status while searching for fleetmon vessel\. status: (\d+), statusText: (.+)/;
      expect(regex.test(thrownError.message)).toBeTruthy();
    });

    it('should throw a descriptive error message if the datasource does not know the tracking_id', async () => {
      // Setup:
      const input_item = {
        doc: {
          map_settings: { tracking_id: '2222222' },
          properties: { name: 'SW3' },
        },
      };
      const input_nconfig = { fleetmon_api_key: 'abcdefabcdefabcdef' };
      mockSingleResponse({
        status: 200,
        statusText: 'OK',
        data: { vessels: [] },
      });

      // Call:
      const thrownError = await callGetterWithCatch(input_item, input_nconfig);
      expect(axios.get).toHaveBeenCalled();
      // Assertion:
      const regex =
        /The given MMSI \(([0-9]+)\) returned no ships on fleetmon\. Is the MMSI correct\? The ship is named SW3/;
      expect(regex.test(thrownError.message)).toBeTruthy();
    });

    it('should throw a descriptive error message if the datasource does not know the tracking_token', async () => {
      // Setup:
      const input_item = {
        doc: {
          map_settings: { tracking_token: 'lalalala' },
          properties: { name: 'SW3' },
        },
      };
      const input_nconfig = { fleetmon_api_key: 'abcdefabcdefabcdef' };
      mockSingleResponse({
        status: 404,
        statusText: 'NOT FOUND',
      });

      // Call:
      const thrownError = await callGetterWithCatch(input_item, input_nconfig);

      // Assertion:
      expect(thrownError.message).toMatch(/status: 404, statusText: NOT FOUND/);
    });

    // it('should throw a descriptive error message if the datasource does not have any data for the tracking_id',async () => {
    //   expect(false).toBe(true); // fail until implemented
    // });

    // it('should throw a descriptive error message if the datasource returns data in an unexpected format',async () => {
    //   expect(false).toBe(true); // fail until implemented
    // });
  });
});
