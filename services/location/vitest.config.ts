/// <reference types="vitest" />
import { defineConfig } from 'vite';

export default defineConfig({
  test: {
    include: ['datasources/**/*.spec.ts'],
    globals: true,
    clearMocks: true,
    coverage: {
      provider: 'istanbul', // or 'c8'
    },
  },
});
