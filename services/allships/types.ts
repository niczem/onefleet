/**
 * Interface to start other datasources.
 * Each datasource should have its own converter that implements this interface.
 */
export interface GeoJSONConverter {
  convert(
    input_filename: string,
    output_filename: string,
    input_format_version: string
  ): void;
}
