/**
 * A command-line node script to convert datasource requests into geojson files.
 *
 * Example usage:
 *   npm install
 *   npm run build
 *   curl https://apiv2.fleetmon.com//regional_ais/tracking/?apikey=<API_KEY> --header 'Accept: application/json' > ./fleetmon.json
 *   node main.js -i ./fleetmon.json -o ./all_ships.geojson
 */

import * as commander from 'commander';
import FleetmonGeoJSONConverter from './fleetmon_converter.js';

/**
 * List all possible datasources
 */
export enum ConverterType {
  fleetmon = 'fleetmon',
  marinetraffic = 'marinetraffic',
}

commander.program
  .option(
    '-i, --input <filename>',
    'input file as received from query',
    'fleetmon.json'
  )
  .option(
    '-o, --output <filename>',
    'output file in geojson format',
    'all_ships.geojson'
  )
  .option('--converter_type <type>', 'converter type', ConverterType.fleetmon)
  .option('--input_version <version>', 'version of input file format', 'latest')
  .version('0.0.1');
commander.program.parse(process.argv);

console.log(
  `Converting file '${commander.program.opts().input}' into '${
    commander.program.opts().output
  }'`,
  `using ${commander.program.opts().converter_type} converter version "${
    commander.program.opts().input_version
  }"...`
);
switch (commander.program.opts().converter_type) {
  case ConverterType.fleetmon:
    const converter = new FleetmonGeoJSONConverter();
    converter.convert(
      commander.program.opts().input,
      commander.program.opts().output,
      commander.program.opts().input_version
    );
    break;
  case ConverterType.marinetraffic:
    throw new Error('Marinetraffic converter has not yet been implemented.');
  default:
    throw new Error('Unknown converter type');
}
console.log('Done.');
