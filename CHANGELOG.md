# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.9] - 2024-06-18

### Fixed
- Fixed a bug where the final row was off by one in the import tool
- Fixed a bug where empty timestamps in excel files would result in epoch time being parsed instead of nothing.
  
### Changed
- UI/UX improvements in the import tool 
  - coloring for different levels of parsing (green for full, yellow for partial with mandatory fields present and red for mandatory fields missing)
  - Showing users how many cases were found and of those, how many were parsed
- Security fixes in several dev packages.

## [0.3.8] - 2024-03-25

### Fixed
- Changed some settings to not run out of requests on Fleetmon AIS position endpoint

### Changed
- CreateItem and ShowItem merged into one component 
- New default map tiles

## [0.3.7] - 2024-02-19

### Fixed
- Refactor of the public AIS service

### Changed
- lastCasePosition view has been removed and replaced by an index on the timestamp, significantly reducing load times
- ListView has been partially refactored, significantly reducing load times and bringing some UI improvements
- The map toolbar has been reworked, reducing the unecessary icons and with better differentiation of the different ways to input positions on the map.
- Changed the color and size of the different SAR Zones
- Added TUN NOTAM to the list of layers
- Added import tool documentation to the repo as a copy of the documentation on the CMRCC cloud server

##  [0.3.6] - 2024-01-16

### Changed

- Utilized Tailwind CSS whenever possible and removed most scoped CSS classes.
- Adjusted positioning using flex instead of absolute positioning.
- Implemented a new login page design.
- Introduced tabs in the left navigation bar, with a logic where unclicking "show..." creates a small icon in the tab.
- Conducted cleanup on various components, addressing issues related to filters, style, and positioning.
- Added a drag element to the right-center of the left navigation bar, along with a mouse pointer indicator.

##  [0.3.5] - 2024-01-13

### Changed

- Made the SinglePosition component slightly bigger for smaller screens readibility
- Code of conduct updated

### Added

- Implemented an import tool to parse excel files and creates cases out of the info in them
- Added military no-fly zones for the tunisian air space as an optional layer

### Fixed

- Requests to the openstreetmap server for tiles now made with https rather than http
- Fixed the content of the position tooltip not being updated when a new position is added to an item
- Fixed typos in readme
- AIS Service: Security upgrade for pupeteer package from 18.2.0 to 19.7.3
- Allships Service: Removed progress bar in logs and now fails early if something fails
- Default launch.json configuration now correctly binds for debugging purposes in VScodium

## [0.3.4] - 2023-09-19

### Changed
Replication of the remote database now follows the set date range in the settings (by default the last 3 days) to reduce replication time and thus the loading time of Onefleet.

### Added
Added a couchdb view containing the last known position of each item to always display some known position even if that position is before or after the range specified in settings.


## [0.3.3] - 2023-07-31

### Added

- Added instructions to the README on how to use the new docker compose 
- Added South-Sudan to list of nationalities for pob_per_nationality with the SS key

### Changed

- Updated the README for formatting
- Changed stateless key in pob_per_nationality field to XX 
- Upgraded vite from 3.2.4 to 3.2.7
- Updated yaml and @antfu/utils for security patches
- Updated browserlist database

### Fixed

-  Fixed a bug where the date of a position but not the hour would be formatted to the broweer's timezone instead of UTC.
- Fixed some bugs in the docker compose profile that would prevent the app from starting if it was started once before because port was already allocated

## [0.3.2] - 2023-04-18

### Added

- Filters for the different SAR zones
- Added a button to LeftNavigation to add/remove layers of the map
- Tests to make sure filters always match up with the existing templates
- Moved from mitt to pinia to handle events and to be used as a data store
- Made a "latest" converter folder to keep old versions of it as we update it


### Changed

- Split services artefacts in separate files for a release 
- Refactor of DbWrapper (added typing, removing deprecated functions and callbacks)
- Refactor of MapWrapper into different files
- Made time_created and time_modified fields serializable- 
- removed template-specific toUpperCase function from ShowItem
- Changed mouse cursor to pointer in LeftNav tab selector
### Fixed

- Fixed the icon for DrawnMarkerPopup not being included in the assets on build and thus not appearing
- No need to log back in after a refresh
- Made NumberInput always return numbers instead of "-1"
- boat_color now uses the default color as defined per ItemTemplateField boat_color 
- No more default [0,0] position in CreateItem
- Fixed "unknown database name" error message for logs db
- Fixed the fact that it was possible to enter a separator character in TagInput
- Miscelaneous typing
- Miscelaneous typo fixes


## [0.3.1] - 2023-01-30

### Fixed

- SinglePosition now expects an ISO-formatted date strings instead of a Date object 


## [0.3.0] - 2023-02-14

### Added

- Template Versioning: Add fields `prefix_created` and `prefix_modified` 
  - Add version to item db entry

- Wrote item converter script(s) between template schemata

- Added field Actor conducting outcome 

- Added open/closed field to Sightings template
  - added a filter that automatically hides the closed sightings, like for cases

-  Added status "Ready for Documentation" 
  - Added filter group to only show cases with status "ready for documentation"

- Feature: Follow-Up
  - Renamed drowned into dead 
  - Added additional boat types (iron, sailing boat, fishing vessel)
  - Added possibility to store files via cloud
  - Added indicator: “Follow up needed” y/n 
  - Renamed dropdown menues: "-" to "currently unknown" 
  - Added feature to document nationalities of POB via tag function
  - Added occured_at field to case 
  - Added possibility to document nationalities of POB via tags

- Added feature: possibility to switch between filtergroups via dropdown in LeftNavigation 

- Added a git pre-commit link hook via husky & lint-staged to improve the dev experience 

### Changed

- Introduced Vite/Vitest
  - New linting and prettier rules before vite merge request 
  - Included leaflet via npm only

- Refactored existing tests for Vitest
  - Coverage: Switch back to Istanbul 

- Refactored allships service into multiple files 
  - split up fleetmon.ts into main.ts and others 

- Refactored: first intermediate attempt at splitting up LocD before further refactoring 
  - main.ts is split into four separate files: DBInstances.ts, locationService.ts, mailService.ts and runLocationServices.ts 

- Rephrased: "speed" under positions to "speed in knots"

- Rephrased: "Details" under authorities to "Details about communication"

- Made "alerted_at" a datetime field 

- Cleaned up Template Fields
  - In the OF database schema, there were a couple of fields, which need to be cleaned up, so that they can be used in the SARchive to get meaningful analytics
  - e.g. The " Notes" field (Boat) has a leading space in the name. That should be fixed as well

- Show absolute times for Positions in ShowItem/CreateItem by default (user request)

- Split the huge LayerControl into multiple buttons on the map 
  - one button for maptiles and info (e.g. SAR zones)
  - one button for items
  - one button for weather layers

- Switched to Element Plus tags input and removed Voerro Tags input

- Refactor: swap `<script>` and `<template>` sections in all vue files 

- Refactor: first intermediate attempt at splitting up LocD before further refactoring 
  - This aims to improve visibility of code changes

- Chore: disallow lint stage to fail in pipeline 

- Chore: add husky to npm prepare script 

- Chore: replace v0_2_ prefix with v0_3 (or v_test in case of test data) 

- Documentation: update review guidelines 
  - select as reviewers only the people that **definitely need to approve the MR**
  - other people are welcome to additionally review the MR too

### Deprecated

### Removed

- Removed the "Droppoints" from the category name "Sightings&Droppoints"

- Removed mermaid diagram edit link from README (documentation)

### Fixed

- Bug: datetime fields have stopped working (found): DateTimePicker in ShowItem no longer stores selected times)

- Bug: upgraded json package to 2.2.3 

- Style: fix eslint issues 

- Fixed symlinking in bootstrap:vscode in package.json 

- Fixed: do not show "really loose all data?" dialog when creating items 

- Fixed: only offer some filtergroups on map, not all 
  - Fixed the bug: `initially_selected_on_map` has no effect

- Fixed: case template field names & fallback widgets (needed for the item converter script)
  - some field names contained spaces, e.g.  boat_notes
  - some field names contained uppercase character, e.g. Outcome or Notes
  - some field types were changed from text to datetime since our last release, but the contained data does not fit that new type yet.
  - rename template vehicle to ship
  - add `prefix_converted` to DbItem
  - fix: types of attributes in DbItem type
  - actually use pouch_identifier again & fix in templates
  - refactor: transform ad-hoc into jsdoc comments

- Fixed: bring back safeguard of `.env` variables 

### Security

- Security upgrade luxon from 2.5.0 to 2.5.2 (Snyk)

## [0.2.4] - 2022-10-10

### Added

- feat: Refactor Datetime/Timezone Usages 
  - Store all timestamps in UTC (Backend)
  - Ability to see and input timestamp in any timezone (UI)
  - Ability to display timestamps in the timezone they were saved at
  - Give visual hint to user that UTC timezone is used: added datetime display in TopNavigation Bar

- Make Case creation without position and time possible 
  - Remove default Position of 0/0 for all items and make saving without position possible
  - Make Case creation possible without a certain time specification/position

- feat: add positions inside items template for a quick workflow.

- feat: added possibility to delete cases from map 
  - note that cases are not deleted, but only moved to trash so they can be restored again in cases of mistake 

### Changed

- Rename SoG and CoG 
  - Changed Speed over Ground to Speed over Ground (deg)30. and Course over Ground to Course over Ground (deg)

- Rephrased 'Details' under authorities 
  - rephrased "details" under authorities to "details about communication" to make it clearer as wished from issue 473

- Clarify button text in confirm-cancel dialogue 
  - This little change clarifies the text that you see when hitting the "Cancel" button from "Ok" -> "Close and loose changes"; "Cancel" -> "Keep editing"

- Adapted OF frontend to the new default of version 3 in vue

- Finally added the AllShips background service to the OneFleet codebase and integrated it with automated service deployment

- Update project setup-documentation to include adding _users db

- Improved bootstrap & build by introducting couchdb-bootstrap and removing old bootstrap-code

- Replaced source by . in install-db script for shell compatibility

### Removed

- Added /docker/couchdb/etc/local.d/docker.ini to the .gitignore, so files generated by docker on starting the couchDB do not need to be removed manually all the time

- Resolve "Remove deprecated functions in DbWrapper" 

### Fixes

- Fix bug which did not display positions of lat=0 / lon=0 

- clearify dataflow in sidebar: The dataflow of items and positions in the sidebar is a bit unclear and ambiguous. This lead to bugs and inconsistencies.
  - Bug: positions were not visible on the map if a new position was added

- Fix bug: locationservice is not compatible with current stable version of commander package
  - also adds a fail-fast in that it now complains when it cannot understand any of its given parameters.

- Fix: ensure radix on parseInt and enforce it via eslint 
  - Define all parseInt() correctly because it is good practice

- Fix: timestamps for positions were being shown in UTC-1 time when browser was in summer mode

- Fix bug: Marker does not deselect item when forgetting to enter time first and users thought they cannot add a new position. 
  - In the Marker pop-up, the workflow was to first select a time & date for a new position and then select the item for the new position. If one forgets to enter the time/date first, the date selector glows red and we are correctly told that we forgot to enter a time. But then we cannot re-select the item we previously tried to add the position to, as it appears to already be selected in the "Existing..." button dropdown. This prevents us from selecting the item again, now that the time has been entered, and storing the position. Now the item doe not remain selected if an error occurs and the workflow, to select the item first and then the time & date also works. 

- Fix bug: Before when one datasource failed to complete, the location service canceled the whole promise of getting data, instead of waiting to see if one of the other datasources returns any data. 

- Fix: Artifact URL in gitlab-ci was wrong

- Fix: bug that landmarks were not showing up anymore if their position was created a long time ago (out of track range). 
  - This was done by adding an always_show_markers attribute to the ItemTemplate type, and providing a boolean setting for this for each template. Only the landmark template had this setting set to true, while all other templates had this new setting set to false.

- Fix bug: track only showed the last day, no matter what tracking length is set
  - changed the default track settings to show only 3 days but up to 9999 positions per item within that time

- Fix: Npm install and tests were broken

- Fix: CSS in date picker when adding a position in the template 



## [0.2.3] - 2022-08-05

### Added

- feat: Make global track range visible in LeftNav (showing users in LeftNav which track range is visible)

- feature: Add position format recognition to add marker (In order to place a marker easier, a simple input field for the coordinates is implemented.
Also the format recogniser is implemented, which is already in use when creating a case.)


### Changed

- Refactor Datetime/Timezone Usages:
  - Showing different timezones more clearly
  - added the current datetime in UTC in the TopNav:

- refactor(event): clarify use of itemUpdated event
  - Release versioning: New allowed values with patch release possible
  corrected some typos

- add secondary color in showItem

- Quick aesthetic change: sar zone and 12/24nm zone opacity and colors

- Resolve "Change Folder Structure of Release Artifact generated in CI"

- test: Turn off coverage threshold which causes pipeline to fail when not reached
  - agreed to take coverage threshold out for now. Nevertheless, people should still write tests when making and note the change in coverage. We shall reintroduce the coverage in 2-3 months when

- chore: corrected pipeline stage name for lint:prettier and lint:eslint 

- Update .gitlab-ci.yml - removed public deploy state. 

- Resolve "Remove nodemailer and sqlite dependencies from location service" 

### Deprecated

- Resolve "Replace local PouchDB with local CouchDB"
  - Remove PouchDB and add documentation how to start the local CouchDB.

### Removed

### Fixed

- Fix: filtered replication
  - Filtered replication was only fetching the past 24 hours of automatically created positions. However the past 30 days should be synced.

- apply item-specific track updates without needing a page reload (multiple MRs)
  - Instead of reloading the page, an event is sent when global track settings (1) or item-specific track settings (2) are updated.
  - fixes the long-standing bug that changes in track length needed a total reload of the app to be applied. This should now allow on-the-go changes to track lengths to be shown on the map.

- fix: set default value for field category in templates aircraft and vehicle
  - Before there was a bug that newly created items of these categories could not be seen because the category has not been set, therefore the Mango query cannot find the item. The solution would be to set default value for template categories.

- fix(icon handler): svg formatting
  - bug closing the marker popup randomly

- Resolve Bug: Timestamp of vehicles in Left Navigation not updated
  - before: When a new position is created for cases, the timestamp in Left Navigation is correctly updated. For ships it is not. However, the position is loaded from the db correctly and the map is updatad correctly.

- 430-date-range-selected-in-background-bug
  - fix: only show alert when 'date_range' is selected in settings
  - fix: correct computeItemSpecificTracksConfig function to properly take item-specific settings into account
  - fix: make clear that when date-range is selected, newly added positions are just not shown while they have still been added to the system Closes #430 (closed)

- Fix aircraft icon problem
  - there was a problem with the aircraft icon when no speed was given

- Fix: introduce boolean confirm that distinguishes whether confirm dialog is to be shown 
  - Fixes the bug that the 'Are you sure' dialog is shown for 'Save' Button whereas it should only be shown for the 'Cancel' Button. 

- Fix: The position timestamps on the Map are always shown, even if they are switched off in the settings

### Security

- Upgrade dependencies where patch and minor updates are available and remove unused dependencies

- chore: Remove unused `moment` dependency which also constitutes a security risk 


## [0.2.2] - 2022-04-07

### Added

- Introduce Mango Queries (CouchDB Feature). Use for reloading updated items and positions.

### Changed

- Documentation:
  - Update field request workflow.
  - Update release process.
- CI
  - Use node 16.14.0.
  - Change folder structure of Release Artifact.
- List View layout improved.
- Move 'Number and Checkbox' into component 'NumperInput'.
- Map:
  - Update opacity and colors of SAR Zones.
  - Add labels to world port index layer.
  - Improve icon handling.
- Add secondary color in ShowItem.
- Introduce filtered replication for items and positions. First load past 24 hours, then past month.
- Introduce test coverage threshold.
- Case Templates: add allowed values `Unclear` in `Outcome` and `SAR 3` in `SAR Region`.

### Deprecated

- Remove local PouchDB for development setup. Add instructions for local CouchDB.

### Removed

- Remove unused folders from repository.

### Fixed

- location service: all services except mailer work currently.
  - Load all items, not only IDs starting with `VEHICLES`.
  - Fix fleetmon handling.
  - Fix bug because of design documents (Mango Query feature).
- Template Field documentation:
  - remove columns `implemented in` and `relevant`.
  - update Template_Fields_v2.0.xlsx to reflect current implementation.
- Fix: marker popup cannot be closed.
- Update Readme to reflect current setup.
- Fix Aircraft icon problem.
- Fix: On first start and if offline, positions and items are only shown on reload.
- Fix unit tests.
- Fix: date range selected in background.

### Security

- Upgrade dependencies where patch and minor updates are available and remove unused dependencies

## [0.2.1] - 2022-02-16

### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security
